from copy import deepcopy

"""
def replsfx(sfx_b, sfx_a, try_sub=list()):
    possible_words = list()
    if word[-len(sfx_b):] == sfx_b:
        w_a = word[:-len(sfx_b)]
        for sub in try_sub + [('', '')]:
            if w_a[-len(sub[0]):] == sub[0]:
                new_word = w_a[:-len(sub[0])] + sub[1] + sfx_a
                possible_words.append(new_word)
    return possible_words


def addpfx(pfx_a, try_sub=list()):
    possible_words = list()
    w_a = word
    for sub in try_sub + [('', '')]:
        if w_a[:len(sub[0])] == sub[0]:
            new_word = pfx_a + sub[1] + w_a[len(sub[0]):]
            possible_words.append(new_word)
    return possible_words


def addsfx(sfx_a, try_sub=list()):
    possible_words = list()
    w_a = word
    for sub in try_sub + [('', '')]:
        if w_a[:-len(sub[0])] == sub[0]:
            new_word = sfx_a + sub[1] + w_a[-len(sub[0]):]
            possible_words.append(new_word)
    return possible_words


def delpfx(pfx_b, try_sub=list()):
    possible_words = list()
    if word[:len(pfx_b)] == pfx_b:
        w_a = word[len(pfx_b):]
        for sub in try_sub + [('', '')]:
            if w_a[:len(sub[0])] == sub[0]:
                new_word = w_a[len(sub[0]):] + sub[1]
                possible_words.append(new_word)
    return possible_words


def delsfx(sfx_b, try_sub=list()):
    possible_words = list()
    if word[-len(sfx_b):] == sfx_b:
        w_a = word[:-len(sfx_b)]
        for sub in try_sub + [('', '')]:
            if w_a[-len(sub[0]):] == sub[0]:
                new_word = w_a[:-len(sub[0])] + sub[1]
                possible_words.append(new_word)
    return possible_words
"""


class DerivationADV:
    def __init__(self, word, pos):
        self.word = word
        self.pos = pos
        self.possible_derivatives = list()

    def generate(self):

        def replsfx(word, kwargs):
            try_sub = [] if len(kwargs) == 2 else kwargs[-1]
            sfx_b, sfx_a = kwargs[0], kwargs[1]
            possible_words = list()
            if len(word) > len(sfx_b) and word[-len(sfx_b):] == sfx_b:
                w_a = word[:-len(sfx_b)]
                possible_words.append(w_a + sfx_a)
                for sub in try_sub:
                    if len(w_a) >= len(sub[0]) and w_a[-len(sub[0]):] == sub[0]:
                        new_word = w_a[:-len(sub[0])] + sub[1] + sfx_a
                        possible_words.append(new_word)
            return possible_words

        def addpfx(word, kwargs):
            try_sub = [] if len(kwargs) == 1 else kwargs[-1]
            pfx_a = kwargs[0]
            possible_words = list()
            w_a = word
            possible_words.append(pfx_a + w_a)
            for sub in try_sub:
                if w_a[:len(sub[0])] == sub[0]:
                    new_word = pfx_a + sub[1] + w_a[len(sub[0]):]
                    possible_words.append(new_word)
            return possible_words

        def addsfx(word, kwargs):
            try_sub = [] if len(kwargs) == 1 else kwargs[-1]
            sfx_a = kwargs[0]
            possible_words = list()
            w_a = word
            possible_words.append(w_a + sfx_a)
            for sub in try_sub:
                if w_a[-len(sub[0]):] == sub[0]:
                    new_word = w_a[:-len(sub[0])] + sub[1] + sfx_a
                    possible_words.append(new_word)
            return possible_words

        def delpfx(word, kwargs):
            try_sub = [] if len(kwargs) == 1 else kwargs[-1]
            pfx_b = kwargs[0]
            possible_words = list()
            if word[:len(pfx_b)] == pfx_b:
                w_a = word[len(pfx_b):]
                possible_words.append(w_a)
                for sub in try_sub:
                    if w_a[:len(sub[0])] == sub[0]:
                        new_word = w_a[len(sub[0]):] + sub[1]
                        possible_words.append(new_word)
            return possible_words

        def delsfx(word, kwargs):
            try_sub = [] if len(kwargs) == 1 else kwargs[-1]
            sfx_b = kwargs[0]
            possible_words = list()
            if word[-len(sfx_b):] == sfx_b:
                w_a = word[:-len(sfx_b)]
                possible_words.append(w_a)
                for sub in try_sub:
                    if w_a[-len(sub[0]):] == sub[0]:
                        new_word = w_a[:-len(sub[0])] + sub[1]
                        possible_words.append(new_word)
            return possible_words

        rules = [
            # ('быстрый', 'быстро')
            [[(replsfx, 'ый', 'о')], ('adj', 'adv')],

            # ('всяческий', 'всячески')
            [[(replsfx, 'ий', 'и')], ('adj', 'adv')],

            # ('дружеский', 'по-дружески')
            [[(replsfx, 'ий', 'и'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('певучий', 'певуче')
            [[(replsfx, 'ий', 'е')], ('adj', 'adv')],

            # ('зябкий', 'зябко')
            [[(replsfx, 'ий', 'о')], ('adj', 'adv')],

            # ('вечер', 'вечером')
            [[(addsfx, 'ом')], ('noun', 'adv')],

            # ('видимый', 'по-видимому')
            [[(replsfx, 'ый', 'ому'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('вдалеке', 'невдалеке')
            [[(addpfx, 'не')], ('adv', 'adv')],

            # ('трудно', 'трудненько')
            [[(replsfx, 'о', 'енько')], ('adv', 'adv')],

            # ('прежний', 'по-прежнему')
            [[(replsfx, 'ий', 'ему'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('верх', 'вверх')
            [[(addpfx, 'в')], ('noun', 'adv')],

            # ('деловой', 'по-деловому')
            [[(replsfx, 'ой', 'ому'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('рано', 'ранёхонько')
            [[(replsfx, 'о', 'ёхонько')], ('adv', 'adv')],

            # ('верх', 'наверх')
            [[(addpfx, 'на')], ('noun', 'adv')],

            # ('прямой', 'впрямую')
            [[(replsfx, 'ой', 'ую'), (addpfx, 'в')], ('adj', 'adv')],

            # ('весна', 'весной')
            [[(replsfx, 'а', 'ой')], ('noun', 'adv')],

            # ('пять', 'пятью')
            [[(addsfx, 'ю')], ('num', 'adv')],

            # ('рано', 'ранёшенько')
            [[(replsfx, 'о', 'ёшенько')], ('adv', 'adv')],

            # ('светло', 'засветло')
            [[(addpfx, 'за')], ('adv', 'adv')],

            # ('белый', 'добела')
            [[(replsfx, 'ый', 'а'), (addpfx, 'до')], ('adj', 'adv')],

            # ('долго', 'подолгу')
            [[(replsfx, 'о', 'у'), (addpfx, 'по')], ('adv', 'adv')],

            # ('тихо', 'тихонько')
            [[(replsfx, 'о', 'онько'), (addpfx, 'по')], ('adv', 'adv')],

            # ('прочь', 'напрочь')
            [[(addpfx, 'на')], ('adv', 'adv')],

            # ('полный', 'сполна')
            [[(replsfx, 'ый', 'а'), (addpfx, 'с')], ('adj', 'adv')],

            # ('половина', 'вполовину')
            [[(replsfx, 'а', 'у'), (addpfx, 'в')], ('noun', 'adv')],

            # ('малый', 'вмале')
            [[(replsfx, 'ый', 'е'), (addpfx, 'в')], ('adj', 'adv')],

            # ('грубый', 'нагрубо')
            [[(replsfx, 'ый', 'о'), (addpfx, 'на')], ('adj', 'adv')],

            # ('озорной', 'озорно')
            [[(replsfx, 'ой', 'о')], ('adj', 'adv')],

            # ('ныне', 'поныне')
            [[(addpfx, 'по')], ('adv', 'adv')],

            # ('встреча', 'навстречу')
            [[(replsfx, 'а', 'у'), (addpfx, 'на')], ('noun', 'adv')],

            # ('четверо', 'вчетвером')
            [[(replsfx, 'о', 'ом'), (addpfx, 'в')], ('num', 'adv')],

            # ('мелькать', 'мельком')
            [[(replsfx, 'ать', 'ом')], ('verb', 'adv')],

            # ('веселый', 'навеселе')
            [[(replsfx, 'ый', 'е'), (addpfx, 'на')], ('adj', 'adv')],

            # ('пятеро', 'впятеро')
            [[(addpfx, 'в')], ('num', 'adv')],

            # ('торчать', 'торчмя')
            [[(replsfx, 'ать', 'мя')], ('verb', 'adv')],

            # ('напрасный', 'понапрасну')
            [[(replsfx, 'ый', 'у'), (addpfx, 'по')], ('adj', 'adv')],

            # ('редкий', 'изредка')
            [[(replsfx, 'ий', 'а'), (addpfx, 'из')], ('adj', 'adv')],

            # ('крепкий', 'накрепко')
            [[(replsfx, 'ий', 'о'), (addpfx, 'на')], ('adj', 'adv')],

            # ('средина', 'посредине')
            [[(replsfx, 'а', 'е'), (addpfx, 'по')], ('noun', 'adv')],

            # ('ныне', 'доныне')
            [[(addpfx, 'до')], ('adv', 'adv')],

            # ('десятый', 'в-десятых')
            [[(replsfx, 'ый', 'ых'), (addpfx, 'в-')], ('num', 'adv')],

            # ('мертвый', 'замертво')
            [[(replsfx, 'ый', 'о'), (addpfx, 'за')], ('adj', 'adv')],

            # ('бок', 'обок')
            [[(addpfx, 'о')], ('noun', 'adv')],

            # ('бок', 'сбоку')
            [[(addsfx, 'у'), (addpfx, 'с')], ('noun', 'adv')],

            #  ('два', 'дважды')
            [[(addsfx, 'жды')], ('num', 'adv')],

            # ('белый', 'белым')
            [[(replsfx, 'ый', 'ым')], ('adj', 'adv')],

            # ('рано', 'рановато')
            [[(replsfx, 'о', 'овато')], ('adv', 'adv')],

            #  ('хорошенько', 'хорошенечко')
            [[(replsfx, 'о', 'енько')], ('adv', 'adv')],

            # ('лёгкий', 'слегка')
            [[(replsfx, 'ий', 'а'), (addpfx, 'с')], ('adj', 'adv')],

            # ('простой', 'попросту')
            [[(replsfx, 'ой', 'у'), (addpfx, 'по')], ('adj', 'adv')],

            # ('косой', 'накосо')
            [[(replsfx, 'ой', 'о'), (addpfx, 'на')], ('adj', 'adv')],

            # ('низ', 'донизу')
            [[(addsfx, 'у'), (addpfx, 'до')], ('noun', 'adv')],

            # ('весна', 'весною')
            [[(replsfx, 'а', 'ою')], ('noun', 'adv')],

            # ('сидеть', 'сидмя')
            [[(replsfx, 'еть', 'мя')], ('verb', 'adv')],

            # ('расхватать', 'нарасхват')
            [[(delsfx, 'ать'), (addpfx, 'на')], ('verb', 'adv')],

            # ('вразвалку', 'вразвалочку')
            [[(replsfx, 'у', 'ку', [('к', 'оч')]), (addpfx, 'в')], ('adv', 'adv')],

            # ('серый', 'иссера')
            [[(replsfx, 'ый', 'а'), (addpfx, 'ис')], ('adj', 'adv')],

            # ('живой', 'вживе')
            [[(replsfx, 'ой', 'е'), (addpfx, 'в')], ('adj', 'adv')],

            # ('простой', 'спроста')
            [[(replsfx, 'ой', 'а'), (addpfx, 'с')], ('adj', 'adv')],

            # ('верх', 'кверху')
            [[(addsfx, 'у'), (addpfx, 'к')], ('noun', 'adv')],

            # ('догонять', 'вдогон')
            [[(delsfx, 'ять'), (addpfx, 'в')], ('verb', 'adv')],

            # ('перебивать', 'вперебой') ##### ???
            [[(replsfx, 'а', 'у'), (addpfx, 'в')], ('verb', 'adv')],

            # ('скользить', 'скользом')
            [[(replsfx, 'ить', 'ом')], ('verb', 'adv')],

            # ('коротко', 'коротенько')
            [[(replsfx, 'о', 'енько', [('к', '')])], ('adv', 'adv')],

            # ('ныне', 'отныне')
            [[(addpfx, 'от')], ('adv', 'adv')],

            # ('плотный', 'вплотную')
            [[(replsfx, 'ый', 'ую'), (addpfx, 'в')], ('adj', 'adv')],

            # ('малый', 'смалу')
            [[(replsfx, 'ый', 'у'), (addpfx, 'с')], ('adj', 'adv')],

            # ('новый', 'сызнова')
            [[(replsfx, 'ый', 'а'), (addpfx, 'сыз')], ('adj', 'adv')],

            # ('краткий', 'вкратце')
            [[(replsfx, 'ий', 'е', [('к', 'ц')]), (addpfx, 'в')], ('adj', 'adv')],

            # ('короткий', 'накоротке')
            [[(replsfx, 'ий', 'е'), (addpfx, 'на')], ('adj', 'adv')],

            # ('косой', 'искоса')
            [[(replsfx, 'ой', 'а'), (addpfx, 'ис')], ('adj', 'adv')],

            # ('рядом', 'рядком')
            [[(replsfx, 'ом', 'ком')], ('adj', 'adv')],

            # ('займ', 'взаймы')
            [[(addsfx, 'ы'), (addpfx, 'в')], ('noun', 'adv')],

            # ('перед', 'спереди')
            [[(addsfx, 'и'), (addpfx, 'с')], ('noun', 'adv')],

            # ('ночь', 'ночью')
            [[(addsfx, 'ю')], ('noun', 'adv')],

            # ('муж', 'замуж')
            [[(addpfx, 'за')], ('noun', 'adv')],

            # ('верх', 'вверху')
            [[(addsfx, 'у'), (addpfx, 'в')], ('noun', 'adv')],

            # ('утро', 'утром')
            [[(replsfx, 'о', 'ом')], ('noun', 'adv')],

            # ('воля', 'волей')
            [[(replsfx, 'я', 'ей')], ('noun', 'adv')],

            # ('двое', 'надвое')
            [[(addpfx, 'на')], ('num', 'adv')],

            # ('двое', 'вдвоём')
            [[(replsfx, 'ое', 'оём'), (addpfx, 'в')], ('num', 'adv')],

            # ('желтый', 'изжелта')
            [[(replsfx, 'ый', 'а'), (addpfx, 'из')], ('adj', 'adv')],

            # ('новый', 'вновь')
            [[(delsfx, 'ый', [('в', 'вь')]), (addpfx, 'в')], ('adj', 'adv')],

            # ('тихо', 'тихохонько')
            [[(replsfx, 'о', 'онько')], ('adv', 'adv')],

            # ('тихий', 'втихую')
            [[(replsfx, 'ий', 'ую'), (addpfx, 'в')], ('adv', 'adv')],

            # ('общий', 'сообща')
            [[(replsfx, 'ий', 'а'), (addpfx, 'со')], ('adv', 'adv')],

            # ('седьмой', 'в-седьмых')
            [[(replsfx, 'ой', 'ых'), (addpfx, 'в-')], ('num', 'adv')],

            # ('нагой', 'донага')
            [[(replsfx, 'ой', 'а'), (addpfx, 'до')], ('adj', 'adv')],

            # ('живой', 'заживо')
            [[(replsfx, 'ой', 'о'), (addpfx, 'за')], ('adj', 'adv')],

            # ('прямой', 'напрямую')
            [[(replsfx, 'ой', 'ую'), (addpfx, 'на')], ('adj', 'adv')],

            # ('молодой', 'смолоду')
            [[(replsfx, 'ой', 'у'), (addpfx, 'с')], ('adj', 'adv')],

            # ('близость', 'поблизости')
            [[(delsfx, '', [('ь', 'и')]), (addpfx, 'по')], ('noun', 'adv')],

            # ('верх', 'поверх')
            [[(addpfx, 'по')], ('noun', 'adv')],

            # ('даль', 'вдали')
            [[(addsfx, 'и'), (addpfx, 'в')], ('noun', 'adv')],

            # ('один', 'единожды')
            [[(addsfx, 'ожды', [('один', 'един')])], ('num', 'adv')],

            # ('догонять', 'вдогонку')
            [[(replsfx, 'ять', 'ку'), (addpfx, 'в')], ('verb', 'adv')],

            # ('ползать', 'ползком')
            [[(replsfx, 'ать', 'ком')], ('verb', 'adv')],

            # ('перемежать', 'вперемежку')
            [[(replsfx, 'ать', 'ку'), (addpfx, 'в')], ('verb', 'adv')],

            # ('внутрь', 'вовнутрь')
            [[(addpfx, 'во')], ('adv', 'adv')],

            # ('переваливать', 'вперевалку')
            [[(replsfx, 'ивать', 'ку'), (addpfx, 'в')], ('verb', 'adv')],

            # ('новый', 'по-новой')
            [[(replsfx, 'ый', 'ой'), (addpfx, 'по')], ('adj', 'adv')],

            # ('правый', 'вправо')
            [[(replsfx, 'ый', 'о'), (addpfx, 'в')], ('adj', 'adv')],

            # ('чистый', 'начистую')
            [[(replsfx, 'ый', 'ую'), (addpfx, 'на')], ('adj', 'adv')],

            # ('частый', 'зачастую')
            [[(replsfx, 'ый', 'ую'), (addpfx, 'за')], ('adj', 'adv')],

            # ('старый', 'исстари')
            [[(replsfx, 'ый', 'и'), (addpfx, 'ис')], ('adj', 'adv')],

            # ('чистый', 'подчистую')
            [[(replsfx, 'ый', 'ую'), (addpfx, 'под')], ('adj', 'adv')],

            # ('пеший', 'пешком')
            [[(replsfx, 'ий', 'ком')], ('adj', 'adv')],

            # ('тихий', 'по-тихому')
            [[(replsfx, 'ий', 'ому'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('поздний', 'допоздна')
            [[(replsfx, 'ий', 'а'), (addpfx, 'до')], ('adj', 'adv')],

            # ('синий', 'иссиня')
            [[(replsfx, 'ий', 'я'), (addpfx, 'ис')], ('adj', 'adv')],

            # ('древлий', 'издревле')
            [[(replsfx, 'ий', 'е'), (addpfx, 'из')], ('adj', 'adv')],

            # ('босой', 'босиком')
            [[(replsfx, 'ой', 'иком')], ('adj', 'adv')],

            # ('твой', 'по-твоему')
            [[(replsfx, 'ой', 'оему'), (addpfx, 'по-')], ('pron', 'adv')],

            # ('слепой', 'сослепу')
            [[(replsfx, 'ой', 'у'), (addpfx, 'со')], ('adj', 'adv')],

            # ('косой', 'вкось')
            [[(delsfx, 'ой', [('', 'ь')]), (addpfx, 'в')], ('adj', 'adv')],

            # ('век', 'вовек')
            [[(addpfx, 'во')], ('noun', 'adv')],

            # ('верх', 'поверху')
            [[(addsfx, 'у'), (addpfx, 'по')], ('noun', 'adv')],

            # ('верх', 'наверху')
            [[(addsfx, 'у'), (addpfx, 'на')], ('noun', 'adv')],

            # ('утро', 'поутру')
            [[(replsfx, 'о', 'у'), (addpfx, 'по')], ('noun', 'adv')],

            # ('начало', 'вначале')
            [[(replsfx, 'о', 'е'), (addpfx, 'в')], ('noun', 'adv')],

            # ('стороной', 'сторонкой')
            [[(replsfx, 'ой', 'кой')], ('adv', 'adv')],

            # ('дни', 'днями')
            [[(replsfx, 'и', 'ями')], ('noun', 'adv')],

            # ('висеть', 'на весу')
            [[(replsfx, 'еть', 'у'), (addpfx, 'на ')], ('verb', 'adv')],

            # ('урывать', 'урывками')
            [[(replsfx, 'ать', 'ками')], ('verb', 'adv')],

            # ('ощупать', 'ощупью')
            [[(replsfx, 'ать', 'ю', [('', 'ь')])], ('verb', 'adv')],

            # ('насторожиться', 'настороже')
            [[(replsfx, 'иться', 'е')], ('verb', 'adv')],

            # ('родиться', 'отроду')
            [[(replsfx, 'иться', 'у'), (addpfx, 'от')], ('verb', 'adv')],

            # ('повалить', 'наповал')
            [[(delsfx, 'ить'), (addpfx, 'на')], ('verb', 'adv')],

            # ('выше', 'свыше')
            [[(addpfx, 'с')], ('adv', 'adv')],

            # ('завтра', 'послезавтра')
            [[(addpfx, 'после')], ('adv', 'adv')],

            # ('вчера', 'позавчера')
            [[(addpfx, 'поза')], ('adv', 'adv')],

            # ('дурной', 'сдуру')
            [[(replsfx, 'ой', 'у', [('н', '')]), (addpfx, 'с')], ('adj', 'adv')],

            # ('панибратский', 'запанибрата')
            [[(replsfx, 'ский', 'а'), (addpfx, 'за')], ('adj', 'adv')],

            # ('вытягивать', 'навытяжку')
            [[(replsfx, 'ивать', 'ку', [('г', 'ж')]), (addpfx, 'на')], ('verb', 'adv')],

            # ('приглядывать', 'вприглядку')
            [[(replsfx, 'ывать', 'у'), (addpfx, 'а')], ('verb', 'adv')],

            # ('перечесть', 'наперечет')
            [[(replsfx, 'сть', 'т'), (addpfx, 'на')], ('verb', 'adv')],

            # ('целый', 'целиком')
            [[(replsfx, 'ый', 'иком')], ('adj', 'adv')],

            # ('голый', 'гольём')
            [[(replsfx, 'ый', 'ём', [('', 'ь')])], ('adj', 'adv')],

            # ('особый', 'особняком')
            [[(replsfx, 'ый', 'няком')], ('adj', 'adv')],

            # ('полный', 'полностью')
            [[(replsfx, 'ый', 'остью')], ('adj', 'adv')],

            # ('малый', 'помаленьку')
            [[(replsfx, 'ый', 'еньку'), (addpfx, 'по')], ('adj', 'adv')],

            # ('запертый', 'взаперти')
            [[(replsfx, 'ый', 'и'), (addpfx, 'в')], ('adj', 'adv')],

            # ('первый', 'во-первых')
            [[(replsfx, 'ый', 'ых'), (addpfx, 'во-')], ('num', 'adv')],

            # ('первый', 'впервой')
            [[(replsfx, 'ый', 'ой'), (addpfx, 'в')], ('num', 'adv')],

            # ('первый', 'впервые')
            [[(replsfx, 'ый', 'ые'), (addpfx, 'в')], ('num', 'adv')],

            # ('единый', 'воедино')
            [[(replsfx, 'ый', 'о'), (addpfx, 'во')], ('adj', 'adv')],

            #  ('верный', 'наверное')
            [[(replsfx, 'ый', 'ое'), (addpfx, 'на')], ('adj', 'adv')],

            # ('верный', 'наверняка')
            [[(replsfx, 'ый', 'яка'), (addpfx, 'на')], ('adj', 'adv')],

            # ('малый', 'сызмальства')
            [[(replsfx, 'ый', 'ства', [('', 'ь')]), (addpfx, 'сыз')], ('adj', 'adv')],

            # ('синий', 'синё')
            [[(replsfx, 'ий', 'ё'), (addpfx, 'до')], ('adj', 'adv')],

            # ('пеший', 'пёхом')
            [[(replsfx, 'ий', 'ом', [('еш', 'ёх')])], ('adj', 'adv')],

            # ('давний', 'давным')
            [[(replsfx, 'ий', 'ым')], ('adj', 'adv')],

            # ('тихий', 'потихоньку')
            [[(replsfx, 'ий', 'оньку'), (addpfx, 'по')], ('adj', 'adv')],

            # ('тихий', 'по-тихой')
            [[(replsfx, 'ий', 'ой'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('общий', 'вообще')
            [[(replsfx, 'ий', 'е'), (addpfx, 'во')], ('adj', 'adv')],

            # ('третий', 'в-третьих')
            [[(replsfx, 'ий', 'их', [('', 'ь')]), (addpfx, 'в-')], ('num', 'adv')],

            # ('синий', 'досиня')
            [[(replsfx, 'ий', 'я'), (addpfx, 'до')], ('adj', 'adv')],

            # ('давний', 'сыздавна')
            [[(replsfx, 'ий', 'а'), (addpfx, 'сыз')], ('adj', 'adv')],

            # ('горячий', 'вгорячах')
            [[(replsfx, 'ий', 'ах'), (addpfx, 'в')], ('adj', 'adv')],

            # ('далёкий', 'неподалёку')
            [[(replsfx, 'ий', 'у'), (addpfx, 'непо')], ('adj', 'adv')],

            # ('тихий', 'исподтишка')
            [[(replsfx, 'ий', 'ка', [('х', 'ш')]), (addpfx, 'изпод')], ('adj', 'adv')],

            # ('воровской', 'воровски')
            [[(replsfx, 'ой', 'и')], ('adj', 'adv')],

            # ('живой', 'живьём')
            [[(replsfx, 'ой', 'ём', [('', 'ь')])], ('adj', 'adv')],

            # ('нагой', 'нагишом')
            [[(replsfx, 'ой', 'ишом')], ('adj', 'adv')],

            # ('косой', 'наискось')
            [[(delsfx, 'ой', [('', 'ь')]), (addpfx, 'наис')], ('adj', 'adv')],

            # ('мужской', 'по-мужски')
            [[(replsfx, 'ой', 'и'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('второй', 'во-вторых')
            [[(replsfx, 'ой', 'ых'), (addpfx, 'во-')], ('num', 'adv')],

            # ('прямой', 'напрямик')
            [[(replsfx, 'ой', 'ик'), (addpfx, 'на')], ('adj', 'adv')],

            # ('прямой', 'напрямки')
            [[(replsfx, 'ой', 'ки'), (addpfx, 'на')], ('adj', 'adv')],

            # ('косой', 'наискосок')
            [[(replsfx, 'ой', 'ок'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('косой', 'наискоски')
            [[(replsfx, 'ой', 'ки'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('пешком', 'пешочком')
            [[(replsfx, 'ом', 'ком', [('к', 'оч')])], ('adv', 'adv')],

            # ('явный', 'въяве')
            [[(replsfx, 'ный', 'е')], ('adj', 'adv')],

            # ('довольный', 'вдоволь')
            [[(delsfx, 'ный'), (addpfx, 'в')], ('adj', 'adv')],

            # ('тайный', 'тайком')
            [[(replsfx, 'ный', 'ком')], ('adj', 'adv')],

            # ('босиком', 'босичком')
            [[(replsfx, 'ом', 'ком', [('к', 'ч')])], ('adv', 'adv')],

            # ('рядом', 'рядышком')
            [[(replsfx, 'ом', 'ышком')], ('adv', 'adv')],

            # ('гусь', 'гуськом')
            [[(addsfx, 'ком')], ('noun', 'adv')],

            # ('даль', 'издали')
            [[(addsfx, 'и'), (addpfx, 'из')], ('noun', 'adv')],

            # ('часть', 'отчасти')
            [[(addsfx, 'и'), (addpfx, 'от')], ('noun', 'adv')],

            # ('муж', 'замужем')
            [[(addsfx, 'ем'), (addpfx, 'за')], ('noun', 'adv')],

            # ('день', 'днем')
            [[(addsfx, 'ём', [('ень', 'н')])], ('noun', 'adv')],

            # ('дом', 'дома')
            [[(addpfx, 'а')], ('noun', 'adv')],

            # ('терпёж', 'невтерпёж')
            [[(addpfx, 'нев')], ('noun', 'adv')],

            # ('запас', 'про запас')
            [[(addpfx, 'про ')], ('noun', 'adv')],

            # ('детство', 'сыздетства')
            [[(replsfx, 'о', 'а'), (addpfx, 'сыз')], ('noun', 'adv')],

            # ('утро', 'наутро')
            [[(replsfx, 'о', 'о'), (addpfx, 'на')], ('noun', 'adv')],

            # ('сила', 'силком')
            [[(replsfx, 'а', 'ком')], ('noun', 'adv')],

            # ('сила', 'силом')
            [[(replsfx, 'а', 'ом')], ('noun', 'adv')],

            # ('времена', 'временами')
            [[(replsfx, 'а', 'ами')], ('noun_pl', 'adv')],

            # ('истина', 'воистину')
            [[(replsfx, 'а', 'у'), (addpfx, 'во')], ('noun', 'adv')],

            # ('неволя', 'поневоле')
            [[(replsfx, 'я', 'е'), (addpfx, 'по')], ('noun', 'adv')],

            # ('ничья', 'вничью')
            [[(replsfx, 'я', 'ю'), (addpfx, 'в')], ('adj', 'adv')],

            # ('один', 'однажды')
            [[(addsfx, 'ажды', [('ин', 'н')])], ('num', 'adv')],

            # ('много', 'многажды')
            [[(addsfx, 'ажды')], ('adv', 'adv')],

            # ('стоять', 'стоймя')
            [[(replsfx, 'ять', 'мя')], ('verb', 'adv')],

            # ('стоять', 'стойком')
            [[(replsfx, 'ять', 'йком')], ('verb', 'adv')],

            # ('переменять', 'напеременку')
            [[(replsfx, 'ять', 'ку'), (addpfx, 'на')], ('verb', 'adv')],

            # ('перегонять', 'наперегонки')
            [[(replsfx, 'ять', 'ки'), (addpfx, 'на')], ('verb', 'adv')],

            # ('перегонять', 'вперегонки')
            [[(replsfx, 'ять', 'ки'), (addpfx, 'в')], ('verb', 'adv')],

            # ('рвать', 'срыву')
            [[(replsfx, 'ать', 'у', [('рв', 'рыв')]), (addpfx, 'с')], ('verb', 'adv')],

            # ('голодать', 'впроголодь')
            [[(delsfx, 'ать', [('', 'ь')]), (addpfx, 'впро')], ('verb', 'adv')],

            # ('махать', 'наотмашь')
            [[(delsfx, 'ать', [('х', 'шь')]), (addpfx, 'наот')], ('verb', 'adv')],

            # ('плавать', 'на плаву')
            [[(replsfx, 'ать', 'у'), (addpfx, 'на ')], ('verb', 'adv')],

            # ('бегать', 'в бегах')
            [[(replsfx, 'ать', 'ах'), (addpfx, 'в ')], ('verb', 'adv')],

            # ('умолкать', 'без умолку')
            [[(replsfx, 'ать', 'у'), (addpfx, 'без ')], ('verb', 'adv')],

            # ('кувыркаться', 'кувырком')
            [[(replsfx, 'аться', 'ом')], ('verb', 'adv')],

            # ('просыпаться', 'без просыпу')
            [[(replsfx, 'аться', 'у'), (addpfx, 'без ')], ('verb', 'adv')],

            # ('догадаться', 'невдогад')
            [[(delsfx, 'аться'), (addpfx, 'нев')], ('verb', 'adv')],

            # ('волочь', 'волоком')
            [[(addsfx, 'ом', [('чь', 'к')])], ('verb', 'adv')],

            # ('мочь', 'невмочь')
            [[(addpfx, 'нев')], ('verb', 'adv')],

            # ('мочь', 'невмоготу')
            [[(addsfx, 'оту', [('чь', 'г')]), (addpfx, 'нев')], ('verb', 'adv')],

            # ('родиться', 'сроду')
            [[(replsfx, 'иться', 'у'), (addpfx, 'с')], ('verb', 'adv')],

            # ('дыбиться', 'дыбом')
            [[(replsfx, 'иться', 'ом')], ('verb', 'adv')],

            # ('торопиться', 'второпях')
            [[(replsfx, 'иться', 'ях'), (addpfx, 'в')], ('verb', 'adv')],

            # ('отвалиться', 'до отвала')
            [[(replsfx, 'иться', 'а'), (addpfx, 'до ')], ('verb', 'adv')],

            # ('отвалиться', 'до отвалу')
            [[(replsfx, 'иться', 'у'), (addpfx, 'до ')], ('verb', 'adv')],

            # ('перекосить', 'наперекосяк')
            [[(replsfx, 'ить', 'як'), (addpfx, 'на')], ('verb', 'adv')],

            # ('трусить', 'труском')
            [[(replsfx, 'ить', 'ком')], ('verb', 'adv')],

            # ('ходить', 'ходуном')
            [[(replsfx, 'ить', 'уном')], ('verb', 'adv')],

            # ('скользить', 'вскользь')
            [[(addsfx, 'ить', [('', 'ь')]), (addpfx, 'в')], ('verb', 'adv')],

            # ('набить', 'битком')
            [[(addsfx, 'ком', [('ь', '')]), (delpfx, 'на')], ('verb', 'adv')],

            # ('близко', 'близёхонько')
            # [[(replsfx, 'о', 'ёхонько'), [('к', '')]], ('adv', 'adv')],

            # ('всюду', 'отовсюду')
            [[(addpfx, 'ото')], ('adv', 'adv')],

            # ('вне', 'извне')
            [[(addpfx, 'из')], ('adv', 'adv')],

            # ('мамин', 'по-маминому')
            [[(addsfx, 'ому'), (addpfx, 'по-')], ('adj_', 'adv')],

            # ('наш', 'по-нашему')
            [[(addsfx, 'ему'), (addpfx, 'по-')], ('pron', 'adv')],

            # ('близкий', 'вблизи')
            [[(replsfx, 'ий', 'и', [('к', '')]), (addpfx, 'в')], ('adj', 'adv')],

            # ('легкий', 'полегоньку')
            [[(replsfx, 'ий', 'оньку', [('к', '')]), (addpfx, 'по')], ('adj', 'adv')],

            # ('последствие', 'впоследствии')
            [[(replsfx, 'е', 'и'), (addpfx, 'в')], ('noun', 'adv')],

            # ('двое', 'вдвое')
            [[(addpfx, 'в')], ('num', 'adv')],

            # ('перебивать', 'наперебой') # нихачю (
            # [[(replsfx, 'ой', 'ки'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('упасть', 'доупаду')
            [[(replsfx, 'ть', 'у', [('ст', 'д')]), (addpfx, 'до')], ('verb', 'adv')],

            # ('проснуться', 'впросонках')
            [[(replsfx, 'уться', 'ках', [('сн', 'сон')]), (addpfx, 'в')], ('verb', 'adv')],

            # ('проснуться', 'спросонок')
            [[(replsfx, 'усться', 'ок', [('сн', 'сон')]), (addpfx, 'с')], ('verb', 'adv')],

            # ('проснуться', 'спросонья')
            [[(replsfx, 'усться', 'я', [('сн', 'сонь')]), (addpfx, 'с')], ('verb', 'adv')],

            # ('удаться', 'наудалую')

            # [[(replsfx, '', 'ки'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('нарочно', 'понарошку')
            [[(replsfx, 'но', 'ку', [('ч', 'ш')]), (addpfx, 'по')], ('adv', 'adv')],

            # ('провернуть', 'невпроворот')
            # [[(replsfx, 'ой', 'ки'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('вывернуть', 'навыворот')
            # [[(replsfx, 'ой', 'ки'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('земля', 'наземь')
            [[(delsfx, 'ля', [('', 'ь')]), (addpfx, 'на')], ('noun', 'adv')],

            # ('играть', 'играючи')
            [[(replsfx, 'ать', 'аючи')], ('verb', 'adv')],

            # ('шестой', 'вполшестого')
            [[(replsfx, 'ой', 'ого'), (addpfx, 'впол')], ('num', 'adv')],

            # ('оборот', 'вполоборота')
            [[(addsfx, 'а'), (addpfx, 'впол')], ('noun', 'adv')],







            # ADJECTIVES

            # 611
            [[(addsfx, 'ов')], ('noun', 'adj')],
            [[(addsfx, 'ев')], ('noun', 'adj')],
            # 612
            [[(replsfx, 'а', 'ин')], ('noun', 'adj')],
            [[(replsfx, 'я', 'ин')], ('noun', 'adj')],

            [[(replsfx, 'а', 'ын')], ('noun', 'adj')],

            [[(addsfx, 'ий')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ий')], ('noun', 'adj')],

            # 616

            [[(replsfx, 'я', 'иный')], ('noun', 'adj')],
            [[(addsfx, 'иный')], ('noun', 'adj')],

            [[(replsfx, 'а', 'ный')], ('noun', 'adj')],
            [[(replsfx, 'о', 'ный')], ('noun', 'adj')],
            [[(replsfx, 'я', 'оный')], ('noun', 'adj')],
            [[(addsfx, 'ный')], ('noun', 'adj')],

            [[(replsfx, 'а', 'енный')], ('noun', 'adj')],
            [[(replsfx, 'о', 'енный')], ('noun', 'adj')],
            [[(addsfx, 'енный')], ('noun', 'adj')],

            [[(replsfx, 'а', 'альный')], ('noun', 'adj')],
            [[(addsfx, 'арный')], ('noun', 'adj')],

            [[(replsfx, 'а', 'арный')], ('noun', 'adj')],
            [[(replsfx, 'о', 'енный')], ('noun', 'adj')],
            [[(addsfx, 'альный')], ('noun', 'adj')],

            [[(replsfx, 'а', 'озный')], ('noun', 'adj')],
            [[(replsfx, 'я', 'озный')], ('noun', 'adj')],
            [[(addsfx, 'озный')], ('noun', 'adj')],

            [[(replsfx, 'ия', 'орный')], ('noun', 'adj')],
            [[(addsfx, 'орный')], ('noun', 'adj')],
            [[(addsfx, 'уальный')], ('noun', 'adj')],
            [[(addsfx, 'ивный')], ('noun', 'adj')],

            [[(replsfx, 'ия', 'ичный')], ('noun', 'adj')],
            [[(addsfx, 'ичный')], ('noun', 'adj')],
            [[(addsfx, 'иальный')], ('noun', 'adj')],

            [[(replsfx, 'я', 'онный')], ('noun', 'adj')],
            [[(replsfx, 'я', 'ональный')], ('noun', 'adj')],

            [[(replsfx, 'ние', 'тельный')], ('noun', 'adj')],
            [[(replsfx, 'ение', 'ительный')], ('noun', 'adj')],

            # 626
            [[(addsfx, 'абельный')], ('noun', 'adj')],
            [[(replsfx, 'а', 'абельный')], ('noun', 'adj')],
            [[(replsfx, 'ация', 'абельный')], ('noun', 'adj')],  # добавить -аци-

            # 628
            [[(addsfx, 'овой')], ('noun', 'adj')],
            [[(addsfx, 'овый')], ('noun', 'adj')],
            [[(addsfx, 'евый')], ('noun', 'adj')],
            [[(replsfx, 'а', 'овый')], ('noun', 'adj')],
            [[(replsfx, 'я', 'ёвый')], ('noun', 'adj')],

            # 630
            [[(addsfx, 'ский')], ('noun', 'adj')],
            [[(replsfx, 'е', 'ской')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ский')], ('noun', 'adj')],
            [[(addsfx, 'цкий')], ('noun', 'adj')],

            [[(addsfx, 'еский')], ('noun', 'adj')],
            [[(replsfx, 'а', 'еский')], ('noun', 'adj')],

            [[(addsfx, 'овской')], ('noun', 'adj')],
            [[(addsfx, 'овский')], ('noun', 'adj')],
            [[(replsfx, 'ой', 'овский')], ('noun', 'adj')],
            [[(replsfx, 'ий', 'овский')], ('noun', 'adj')],
            [[(replsfx, 'а', 'овский')], ('noun', 'adj')],
            [[(replsfx, 'ы', 'овский')], ('noun', 'adj')],

            # 632
            [[(replsfx, 'а', 'анский')], ('pnoun', 'adj')],
            [[(replsfx, 'я', 'анский')], ('pnoun', 'adj')],

            [[(replsfx, 'а', 'инский')], ('pnoun', 'adj')],
            [[(replsfx, 'ы', 'инский')], ('pnoun', 'adj')],
            [[(replsfx, 'ый', 'инский')], ('pnoun', 'adj')],
            [[(replsfx, 'и', 'инский')], ('pnoun', 'adj')],
            [[(replsfx, 'е', 'енский')], ('pnoun', 'adj')],
            [[(replsfx, 'о', 'енский')], ('pnoun', 'adj')],
            [[(replsfx, 'ий', 'енский')], ('pnoun', 'adj')],

            [[(replsfx, 'и', 'ийский')], ('pnoun', 'adj')],
            [[(replsfx, 'ы', 'ийский')], ('pnoun', 'adj')],
            [[(replsfx, 'а', 'ийский')], ('pnoun', 'adj')],

            [[(addsfx, 'ический')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ический')], ('pnoun', 'adj')],

            [[(addsfx, 'ческий')], ('noun', 'adj')],
            [[(replsfx, 'ие', 'ческий')], ('noun', 'adj')],

            # 636

            [[(addsfx, 'ианский')], ('pnoun', 'adj')],
            [[(addsfx, 'анский')], ('pnoun', 'adj')],
            [[(addsfx, 'янский')], ('pnoun', 'adj')],

            # 637
            [[(addsfx, 'астый')], ('noun', 'adj')],
            [[(replsfx, 'а', 'астый')], ('noun', 'adj')],
            [[(replsfx, 'о', 'астый')], ('noun', 'adj')],
            [[(replsfx, 'ы', 'астый')], ('noun', 'adj')],
            [[(replsfx, 'и', 'астый')], ('noun', 'adj')],

            # 638

            [[(addsfx, 'атый')], ('noun', 'adj')],
            [[(replsfx, 'а', 'атый')], ('noun', 'adj')],
            [[(replsfx, 'о', 'атый')], ('noun', 'adj')],
            [[(replsfx, 'ы', 'атый')], ('noun', 'adj')],
            [[(replsfx, 'и', 'атый')], ('noun', 'adj')],

            # 639
            [[(addsfx, 'чатый')], ('noun', 'adj')],
            [[(replsfx, 'а', 'чатый')], ('noun', 'adj')],
            [[(replsfx, 'о', 'чатый')], ('noun', 'adj')],
            [[(replsfx, 'и', 'чатый')], ('noun', 'adj')],

            # 640
            [[(addsfx, 'еватый')], ('noun', 'adj')],
            [[(addsfx, 'оватый')], ('noun', 'adj')],
            [[(replsfx, 'а', 'оватый')], ('noun', 'adj')],
            [[(replsfx, 'о', 'оватый')], ('noun', 'adj')],
            [[(replsfx, 'е', 'еватый')], ('noun', 'adj')],

            # 641
            [[(addsfx, 'овитый')], ('noun', 'adj')],
            [[(addsfx, 'евитый')], ('noun', 'adj')],
            [[(replsfx, 'а', 'овитый')], ('noun', 'adj')],

            # 642
            [[(addsfx, 'истый')], ('noun', 'adj')],
            [[(replsfx, 'о', 'истый')], ('noun', 'adj')],
            [[(replsfx, 'а', 'истый')], ('noun', 'adj')],
            [[(replsfx, 'я', 'истый')], ('noun', 'adj')],

            # 643
            [[(addsfx, 'ливый')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ливый')], ('noun', 'adj')],
            [[(replsfx, 'я', 'ливый')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ивый')], ('noun', 'adj')],

            ##    ПРИЛАГАТЕЛЬНЫЕ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

            # 646

            [[(replsfx, 'ать', 'ной')], ('verb', 'adj')],
            [[(replsfx, 'еть', 'ной')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'ный')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'ный')], ('verb', 'adj')],
            [[(replsfx, 'ять', 'ный')], ('verb', 'adj')],
            [[(replsfx, 'еть', 'ный')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'ной')], ('verb', 'adj')],

            [[(replsfx, 'овать', 'ный')], ('verb', 'adj')],
            [[(replsfx, 'ывать', 'ной')], ('verb', 'adj')],
            [[(replsfx, 'ивать', 'ной')], ('verb', 'adj')],
            [[(replsfx, 'ивать', 'ный')], ('verb', 'adj')],

            [[(replsfx, 'лять', 'ной')], ('verb', 'adj')],

            [[(replsfx, 'иться', 'ный')], ('verb', 'adj')],
            [[(replsfx, 'яться', 'ный')], ('verb', 'adj')],

            # 651

            [[(replsfx, 'ать', 'льный')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'льный')], ('verb', 'adj')],

            [[(replsfx, 'аться', 'льный')], ('verb', 'adj')],
            [[(replsfx, 'иться', 'льный')], ('verb', 'adj')],

            # 652

            [[(replsfx, 'ить', 'ительный')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'ательный')], ('verb', 'adj')],
            [[(replsfx, 'еть', 'ительный')], ('verb', 'adj')],

            [[(replsfx, 'аться', 'ительный')], ('verb', 'adj')],

            # 653

            [[(replsfx, 'ировать', 'абельный')], ('verb', 'adj')],

            # 655

            [[(replsfx, 'ать', 'ской')], ('verb', 'adj')],
            [[(replsfx, 'аться', 'ательский')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'еский')], ('verb', 'adj')],

            # 656

            [[(replsfx, 'оть', 'кий')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'кий')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'кий')], ('verb', 'adj')],
            [[(replsfx, 'еть', 'кий')], ('verb', 'adj')],

            # 657

            [[(replsfx, 'ять', 'яемый')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'аемый')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'имый')], ('verb', 'adj')],
            [[(replsfx, 'еть', 'имый')], ('verb', 'adj')],

            [[(replsfx, 'ать', 'уемый')], ('verb', 'adj')],
            [[(replsfx, 'овать', 'уемый')], ('verb', 'adj')],

            [[(replsfx, 'нуть', 'имый')], ('verb', 'adj')],

            # 658

            [[(replsfx, 'ать', 'истый')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'истый')], ('verb', 'adj')],
            [[(replsfx, 'ываться', 'истый')], ('verb', 'adj')],
            [[(replsfx, 'иваться', 'истый')], ('verb', 'adj')],
            [[(replsfx, 'нуться', 'истый')], ('verb', 'adj')],

            # 659

            [[(replsfx, 'ать', 'чатый')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'истый')], ('verb', 'adj')],
            [[(replsfx, 'ываться', 'истый')], ('verb', 'adj')],
            [[(replsfx, 'иваться', 'истый')], ('verb', 'adj')],
            [[(replsfx, 'нуться', 'истый')], ('verb', 'adj')],

            # 660
            [[(replsfx, 'ать', 'учий')], ('verb', 'adj')],
            [[(replsfx, 'еть', 'учий')], ('verb', 'adj')],
            [[(replsfx, 'ять', 'учий')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'ячий')], ('verb', 'adj')],
            [[(replsfx, 'еть', 'ячий')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'ачий')], ('verb', 'adj')],
            [[(replsfx, 'ять', 'ячий')], ('verb', 'adj')],

            # 661
            [[(replsfx, 'ить', 'ливый')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'ливый')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'чивый')], ('verb', 'adj')],
            [[(replsfx, 'еть', 'чивый')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'ивый')], ('verb', 'adj')],

            # 662
            [[(replsfx, 'ть', 'лый')], ('verb', 'adj')],
            [[(replsfx, 'ть', 'лой')], ('verb', 'adj')],

            # 663
            [[(replsfx, 'ить', 'ёный')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'еный')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'аный')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'анный')], ('verb', 'adj')],
            [[(replsfx, 'уть', 'утый')], ('verb', 'adj')],
            [[(replsfx, 'ить', 'итый')], ('verb', 'adj')],
            [[(replsfx, 'оть', 'отый')], ('verb', 'adj')],
            [[(replsfx, 'еть', 'тый')], ('verb', 'adj')],
            [[(replsfx, 'ять', 'ятый')], ('verb', 'adj')],

            # 664

            [[(replsfx, 'аться', 'анный')], ('verb', 'adj')],
            [[(replsfx, 'иться', 'енный')], ('verb', 'adj')],
            [[(replsfx, 'иться', 'ённый')], ('verb', 'adj')],
            [[(replsfx, 'уться', 'ённый')], ('verb', 'adj')],
            [[(replsfx, 'яться', 'янный')], ('verb', 'adj')],

            ## ПРИЛАГАТЕЛЬНЫЕ, МОТИВИРОВАННЫЕ ПРИЛАГАТЕЛЬНЫМИ

            # 666

            [[(replsfx, 'ый', 'оватый')], ('adj', 'adj')],
            [[(replsfx, 'овый', 'оватый')], ('adj', 'adj')],  # включить в предыдущий
            [[(replsfx, 'евый', 'еватый')], ('adj', 'adj')],  # включить в предыдущий

            # 667

            [[(replsfx, 'ый', 'енький')], ('adj', 'adj')],
            [[(replsfx, 'ой', 'енький')], ('adj', 'adj')],
            [[(replsfx, 'ий', 'енький')], ('adj', 'adj')],
            [[(replsfx, 'ий', 'онький')], ('adj', 'adj')],

            # 669

            [[(replsfx, 'ый', 'ёхонький')], ('adj', 'adj')],
            [[(replsfx, 'ый', 'ёшенький')], ('adj', 'adj')],
            [[(replsfx, 'ой', 'ёшенький')], ('adj', 'adj')],

            # 671

            [[(replsfx, 'ой', 'ущий')], ('adj', 'adj')],
            [[(replsfx, 'ой', 'ющий')], ('adj', 'adj')],
            [[(replsfx, 'ый', 'ущий')], ('adj', 'adj')],
            [[(replsfx, 'ый', 'ющий')], ('adj', 'adj')],

            # 673

            [[(replsfx, 'ый', 'истый')], ('adj', 'adj')],
            [[(replsfx, 'ой', 'истый')], ('adj', 'adj')],

            ### ПРЕФИКСАЛЬНЫЕ ПРИЛАГАТЕЛЬНЫЕ

            # 683

            [[(addpfx, 'а')], ('adj', 'adj')],

            # 684

            [[(addpfx, 'анти')], ('adj', 'adj')],

            # 685

            [[(addpfx, 'архи')], ('adj', 'adj')],

            # 686

            [[(addpfx, 'без')], ('adj', 'adj')],
            [[(addpfx, 'бес')], ('adj', 'adj')],

            # 687

            [[(addpfx, 'вне')], ('adj', 'adj')],

            # 688

            [[(addpfx, 'внутри')], ('adj', 'adj')],

            # 689

            [[(addpfx, 'гипер')], ('adj', 'adj')],

            # 690

            [[(addpfx, 'до')], ('adj', 'adj')],

            # 691

            [[(addpfx, 'за')], ('adj', 'adj')],

            # 693

            [[(addpfx, 'интер')], ('adj', 'adj')],

            # 694

            [[(addpfx, 'меж')], ('adj', 'adj')],
            [[(addpfx, 'между')], ('adj', 'adj')],

            # 695

            [[(addpfx, 'над')], ('adj', 'adj')],

            # 696

            [[(addpfx, 'наи')], ('adj', 'adj')],

            # 697

            [[(addpfx, 'не')], ('adj', 'adj')],

            # 698

            [[(addpfx, 'небез')], ('adj', 'adj')],
            [[(addpfx, 'небес')], ('adj', 'adj')],

            # 699

            [[(addpfx, 'около')], ('adj', 'adj')],

            # 700

            [[(addpfx, 'пере')], ('adj', 'adj')],

            # 701

            [[(addpfx, 'по')], ('adj', 'adj')],

            # 703

            [[(addpfx, 'под')], ('adj', 'adj')],

            # 704

            [[(addpfx, 'после')], ('adj', 'adj')],

            # 705

            [[(addpfx, 'пост')], ('adj', 'adj')],

            # 706

            [[(addpfx, 'пре')], ('adj', 'adj')],

            # 707

            [[(addpfx, 'пред')], ('adj', 'adj')],

            # 708

            [[(addpfx, 'при')], ('adj', 'adj')],

            # 709

            [[(addpfx, 'про')], ('adj', 'adj')],

            # 710

            [[(addpfx, 'противо')], ('adj', 'adj')],

            # 711

            [[(addpfx, 'раз')], ('adj', 'adj')],
            [[(addpfx, 'рас')], ('adj', 'adj')],

            # 712

            [[(addpfx, 'сверх')], ('adj', 'adj')],

            # 714

            [[(addpfx, 'суб')], ('adj', 'adj')],

            # 715

            [[(addpfx, 'супер')], ('adj', 'adj')],

            # 716

            [[(addpfx, 'транс')], ('adj', 'adj')],

            # 717

            [[(addpfx, 'ультра')], ('adj', 'adj')],

            # 718

            [[(addpfx, 'экстра')], ('adj', 'adj')],

            ###  ПРЕФИКСАЛЬНО-СУФФИКСАЛЬНЫЕ ПРИЛАГАТЕЛЬНЫЕ

            ## ПРИЛАГАТЕЛЬНЫЕ, МОТИВИРОВАННЫЕ СУЩЕСТВИТЕЛЬНЫМИ

            # 723

            [[(replsfx, 'а', 'ный'), (addpfx, 'без')], ('noun', 'adj')],
            [[(replsfx, 'я', 'ный'), (addpfx, 'без')], ('noun', 'adj')],
            [[(replsfx, 'и', 'ный'), (addpfx, 'без')], ('noun', 'adj')],
            [[(addsfx, 'ный'), (addpfx, 'без')], ('noun', 'adj')],
            [[(addsfx, 'ный'), (addpfx, 'бес')], ('noun', 'adj')],
            [[(addsfx, 'ний'), (addpfx, 'без')], ('noun', 'adj')],
            [[(addsfx, 'ний'), (addpfx, 'бес')], ('noun', 'adj')],

            # 724

            [[(replsfx, 'о', 'ный'), (addpfx, 'вне')], ('noun', 'adj')],
            [[(replsfx, 'о', 'ный'), (addpfx, 'внутри')], ('noun', 'adj')],
            [[(addsfx, 'ный'), (addpfx, 'вне')], ('noun', 'adj')],
            [[(addsfx, 'ный'), (addpfx, 'внутри')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ный'), (addpfx, 'вне')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ный'), (addpfx, 'внутри')], ('noun', 'adj')],

            # 725

            [[(replsfx, 'е', 'ный'), (addpfx, 'за')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ный'), (addpfx, 'за')], ('noun', 'adj')],
            [[(replsfx, 'о', 'ный'), (addpfx, 'за')], ('noun', 'adj')],
            [[(addsfx, 'ный'), (addpfx, 'за')], ('noun', 'adj')],

            # 726
            [[(addsfx, 'ённый'), (addpfx, 'за')], ('noun', 'adj')],
            [[(addsfx, 'енный'), (addpfx, 'за')], ('noun', 'adj')],
            [[(addsfx, 'ованный'), (addpfx, 'за')], ('noun', 'adj')],

            # 727
            [[(addsfx, 'ный'), (addpfx, 'меж')], ('noun', 'adj')],
            [[(addsfx, 'ный'), (addpfx, 'между')], ('noun', 'adj')],
            [[(addsfx, 'ский'), (addpfx, 'меж')], ('noun', 'adj')],
            [[(addsfx, 'ской'), (addpfx, 'меж')], ('noun', 'adj')],

            [[(replsfx, 'а', 'ный'), (addpfx, 'меж')], ('noun', 'adj')],

            # 728
            [[(replsfx, 'а', 'ный'), (addpfx, 'на')], ('noun', 'adj')],
            [[(replsfx, 'о', 'ный'), (addpfx, 'на')], ('noun', 'adj')],
            [[(addsfx, 'ный'), (addpfx, 'на')], ('noun', 'adj')],
            [[(replsfx, 'а', 'енный'), (addpfx, 'на')], ('noun', 'adj')],
            [[(replsfx, 'о', 'енный'), (addpfx, 'на')], ('noun', 'adj')],

            # 729
            [[(addsfx, 'ный'), (addpfx, 'над')], ('noun', 'adj')],

            # 731
            [[(addsfx, 'ный'), (addpfx, 'около')], ('noun', 'adj')],
            [[(replsfx, 'о', 'ный'), (addpfx, 'около')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ный'), (addpfx, 'около')], ('noun', 'adj')],

            # 732

            [[(addsfx, 'ный'), (addpfx, 'от')], ('noun', 'adj')],
            [[(replsfx, 'я', 'енный'), (addpfx, 'от')], ('noun', 'adj')],

            # 733

            [[(addsfx, 'ный'), (addpfx, 'по')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ный'), (addpfx, 'по')], ('noun', 'adj')],
            [[(replsfx, 'е', 'ский'), (addpfx, 'по')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ский'), (addpfx, 'по')], ('noun', 'adj')],

            # 734

            [[(addsfx, 'ный'), (addpfx, 'под')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ный'), (addpfx, 'под')], ('noun', 'adj')],
            [[(replsfx, 'о', 'ный'), (addpfx, 'под')], ('noun', 'adj')],

            # 735

            [[(addsfx, 'ный'), (addpfx, 'пред')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ный'), (addpfx, 'пред')], ('noun', 'adj')],

            # 736

            [[(addsfx, 'ный'), (addpfx, 'при')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ный'), (addpfx, 'при')], ('noun', 'adj')],
            [[(addsfx, 'ский'), (addpfx, 'при')], ('noun', 'adj')],
            [[(replsfx, 'е', 'ский'), (addpfx, 'при')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ский'), (addpfx, 'при')], ('noun', 'adj')],

            ## ПРИЛАГАТЕЛЬНЫЕ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

            # 743

            [[(replsfx, 'ить', 'ный'), (addpfx, 'без')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'ный'), (addpfx, 'без')], ('verb', 'adj')],
            [[(replsfx, 'ять', 'ный'), (addpfx, 'без')], ('verb', 'adj')],

            [[(replsfx, 'ывать', 'ный'), (addpfx, 'без')], ('verb', 'adj')],

            # 744

            [[(replsfx, 'ить', 'ный'), (addpfx, 'не')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'ный'), (addpfx, 'не')], ('verb', 'adj')],
            [[(replsfx, 'еть', 'ный'), (addpfx, 'не')], ('verb', 'adj')],

            # 745

            [[(replsfx, 'ить', 'имый'), (addpfx, 'не')], ('verb', 'adj')],
            [[(replsfx, 'ать', 'аемый'), (addpfx, 'не')], ('verb', 'adj')],
            [[(replsfx, 'еть', 'емый'), (addpfx, 'не')], ('verb', 'adj')],
            [[(replsfx, 'овать', 'уемый'), (addpfx, 'не')], ('verb', 'adj')],

            ## ПРИЛАГАТЕЛЬНЫЕ МОТИВИРОВАННЫЕ ПРИЛАГАТЕЛЬНЫМИ И НАРЕЧИЯМИ

            # 750

            [[(replsfx, 'ый', 'енный'), (addpfx, 'за')], ('adj', 'adj')],
            [[(replsfx, 'ой', 'енный'), (addpfx, 'за')], ('adj', 'adj')],
            [[(replsfx, 'ой', 'ённый'), (addpfx, 'за')], ('adj', 'adj')],
            [[(replsfx, 'ый', 'ённый'), (addpfx, 'за')], ('adj', 'adj')],

            # ПРЕФИКСАЛЬНЫЕ ПРИЛАГАТЕЛЬНЫЕ С НУЛЕВЫМ СУФФИКСОМ

            # 752

            [[(addsfx, 'ый'), (addpfx, 'без')], ('noun', 'adj')],
            [[(replsfx, 'а', 'ый'), (addpfx, 'без')], ('noun', 'adj')],
            [[(replsfx, 'о', 'ый'), (addpfx, 'без')], ('noun', 'adj')],


            #### ГЛАГОЛЫ

            # 795
            [[(addsfx, 'ить')], ('noun', 'verb')],
            [[(replsfx, 'а', 'ить')], ('noun', 'verb')],
            [[(replsfx, 'о', 'ить')], ('noun', 'verb')],

            # 796
            [[(replsfx, 'ый', 'ить')], ('adj', 'verb')],
            [[(replsfx, 'ий', 'ить')], ('adj', 'verb')],
            [[(replsfx, 'ой', 'ить')], ('adj', 'verb')],

            # 804
            [[(addsfx, 'овать')], ('noun', 'verb')],
            [[(replsfx, 'а', 'овать')], ('noun', 'verb')],
            [[(replsfx, 'е', 'евать')], ('noun', 'verb')],

            [[(addsfx, 'изировать')], ('noun', 'verb')],
            [[(addsfx, 'ировать')], ('noun', 'verb')],
            [[(replsfx, 'а', 'ировать')], ('noun', 'verb')],
            [[(replsfx, 'ия', 'ировать')], ('noun', 'verb')],
            [[(replsfx, 'е', 'ировать')], ('noun', 'verb')],

            # 812
            [[(addsfx, 'ничать')], ('noun', 'verb')],
            [[(replsfx, 'ник', 'ничать')], ('noun', 'verb')],  # убрать

            # 814
            [[(replsfx, 'ный', 'ничать')], ('noun', 'verb')],
            [[(replsfx, 'ной', 'ничать')], ('noun', 'verb')],

            # 818
            [[(addsfx, 'ствовать')], ('noun', 'verb')],
            [[(replsfx, 'а', 'ствовать')], ('noun', 'verb')],
            [[(replsfx, 'о', 'ствовать')], ('noun', 'verb')],

            [[(addsfx, 'ествовать')], ('noun', 'verb')],
            [[(replsfx, 'ие', 'ествовать')], ('noun', 'verb')],

            # 820
            [[(replsfx, 'ый', 'ствовать')], ('adj', 'verb')],

            # 829
            [[(replsfx, 'ый', 'еть')], ('adj', 'verb')],
            [[(replsfx, 'ой', 'еть')], ('adj', 'verb')],
            [[(replsfx, 'ий', 'еть')], ('adj', 'verb')],

            ## ГЛАГОЛЫ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

            # 836
            [[(replsfx, 'ать', 'нуть')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'нуть')], ('verb', 'verb')],
            [[(replsfx, 'овать', 'нуть')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'нуть')], ('verb', 'verb')],
            [[(replsfx, 'оть', 'нуть')], ('verb', 'verb')],
            [[(replsfx, 'аться', 'нуться')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'нуть')], ('verb', 'verb')],
            [[(replsfx, 'ть', 'нуть')], ('verb', 'verb')],

            # 842
            [[(replsfx, 'ать', 'ывать')], ('verb', 'verb')],
            [[(replsfx, 'оть', 'ывать')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'ивать')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'евать')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'евать')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ивать')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'ивать')], ('verb', 'verb')],
            [[(replsfx, 'еться', 'ываться')], ('verb', 'verb')],
            [[(replsfx, 'иться', 'иваться')], ('verb', 'verb')],

            [[(replsfx, 'еть', 'ать')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'ать')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'ять')], ('verb', 'verb')],
            [[(replsfx, 'ть', 'вать')], ('verb', 'verb')],
            [[(replsfx, 'нуть', 'ывать')], ('verb', 'verb')],
            [[(replsfx, 'нуть', 'ать')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ывать')], ('verb', 'verb')],
            [[(replsfx, 'нуться', 'ать')], ('verb', 'verb')],
            [[(replsfx, 'нуться', 'аться')], ('verb', 'verb')],

            ## ПРЕФИКСАЛЬНЫЕ ГЛАГОЛЫ

            # 854
            [[(addpfx, 'в')], ('verb', 'verb')],
            [[(addpfx, 'во')], ('verb', 'verb')],

            # 855
            [[(addpfx, 'вз')], ('verb', 'verb')],
            [[(addpfx, 'взо')], ('verb', 'verb')],

            # 856
            [[(addpfx, 'воз')], ('verb', 'verb')],
            [[(addpfx, 'возо')], ('verb', 'verb')],

            # 857
            [[(addpfx, 'вы')], ('verb', 'verb')],

            # 858
            [[(addpfx, 'де')], ('verb', 'verb')],
            [[(addpfx, 'дез')], ('verb', 'verb')],

            # 859
            [[(addpfx, 'дис')], ('verb', 'verb')],

            # 860
            [[(addpfx, 'до')], ('verb', 'verb')],

            # 861
            [[(addpfx, 'за')], ('verb', 'verb')],

            # 862
            [[(addpfx, 'из')], ('verb', 'verb')],
            [[(addpfx, 'изо')], ('verb', 'verb')],

            # 863
            [[(addpfx, 'на')], ('verb', 'verb')],

            # 864
            [[(addpfx, 'над')], ('verb', 'verb')],
            [[(addpfx, 'надо')], ('verb', 'verb')],

            # 865
            [[(addpfx, 'недо')], ('verb', 'verb')],

            # 867
            [[(addpfx, 'о')], ('verb', 'verb')],

            # 868
            [[(addpfx, 'об')], ('verb', 'verb')],
            [[(addpfx, 'обо')], ('verb', 'verb')],

            # 869
            [[(addpfx, 'от')], ('verb', 'verb')],
            [[(addpfx, 'ото')], ('verb', 'verb')],

            # 870
            [[(addpfx, 'пере')], ('verb', 'verb')],

            # 871
            [[(addpfx, 'по')], ('verb', 'verb')],

            # 872
            [[(addpfx, 'под')], ('verb', 'verb')],
            [[(addpfx, 'подо')], ('verb', 'verb')],

            # 873
            [[(addpfx, 'пре')], ('verb', 'verb')],

            # 874
            [[(addpfx, 'пред')], ('verb', 'verb')],
            [[(addpfx, 'предо')], ('verb', 'verb')],

            # 875
            [[(addpfx, 'при')], ('verb', 'verb')],

            # 876
            [[(addpfx, 'про')], ('verb', 'verb')],

            # 877
            [[(addpfx, 'раз')], ('verb', 'verb')],
            [[(addpfx, 'разо')], ('verb', 'verb')],

            # 878
            [[(addpfx, 'ре')], ('verb', 'verb')],

            # 879
            [[(addpfx, 'с')], ('verb', 'verb')],
            [[(addpfx, 'со')], ('verb', 'verb')],

            # 880
            [[(addpfx, 'со')], ('verb', 'verb')],

            # 881
            [[(addpfx, 'у')], ('verb', 'verb')],

            ## ПРЕФИКСАЛЬНО-СУФФИКСАЛЬНЫЕ ГЛАГОЛЫ

            ### ГЛАГОЛЫ, МОТИВИРОВАННЫЕ ИМЕНАМИ

            # 888
            [[(addsfx, 'ить'), (addpfx, 'за')], ('noun', 'verb')],

            [[(replsfx, 'а', 'ить'), (addpfx, 'за')], ('noun', 'verb')],
            [[(replsfx, 'о', 'ить'), (addpfx, 'за')], ('noun', 'verb')],

            [[(replsfx, 'ый', 'ить'), (addpfx, 'за')], ('adj', 'verb')],

            # 889
            [[(addsfx, 'ить'), (addpfx, 'из')], ('noun', 'verb')],
            [[(addsfx, 'ить'), (addpfx, 'ис')], ('noun', 'verb')],

            [[(replsfx, 'ый', 'ить'), (addpfx, 'из')], ('adj', 'verb')],
            [[(replsfx, 'ый', 'ить'), (addpfx, 'ис')], ('adj', 'verb')],

            # 891
            [[(addsfx, 'ить'), (addpfx, 'о')], ('noun', 'verb')],
            [[(replsfx, 'а', 'ить'), (addpfx, 'о')], ('noun', 'verb')],
            [[(replsfx, 'е', 'ить'), (addpfx, 'о')], ('noun', 'verb')],

            [[(replsfx, 'ый', 'ить'), (addpfx, 'о')], ('adj', 'verb')],
            [[(replsfx, 'ий', 'ить'), (addpfx, 'о')], ('adj', 'verb')],

            # 892
            [[(addsfx, 'ить'), (addpfx, 'об')], ('noun', 'verb')],
            [[(replsfx, 'а', 'ить'), (addpfx, 'об')], ('noun', 'verb')],

            [[(replsfx, 'ий', 'ить'), (addpfx, 'об')], ('adj', 'verb')],
            [[(replsfx, 'ый', 'ить'), (addpfx, 'об')], ('adj', 'verb')],

            # 893
            [[(addsfx, 'ить'), (addpfx, 'обез')], ('noun', 'verb')],
            [[(replsfx, 'а', 'ить'), (addpfx, 'обез')], ('noun', 'verb')],
            [[(replsfx, 'о', 'ить'), (addpfx, 'обез')], ('noun', 'verb')],

            [[(addsfx, 'ить'), (addpfx, 'обес')], ('noun', 'verb')],
            [[(replsfx, 'а', 'ить'), (addpfx, 'обес')], ('noun', 'verb')],
            [[(replsfx, 'о', 'ить'), (addpfx, 'обес')], ('noun', 'verb')],

            # 895
            [[(replsfx, 'ый', 'ить'), (addpfx, 'пере')], ('adj', 'verb')],

            # 897
            [[(addsfx, 'ить'), (addpfx, 'под')], ('noun', 'verb')],

            [[(replsfx, 'ий', 'ить'), (addpfx, 'под')], ('adj', 'verb')],
            [[(replsfx, 'ый', 'ить'), (addpfx, 'под')], ('adj', 'verb')],

            # 898
            [[(addsfx, 'ить'), (addpfx, 'при')], ('noun', 'verb')],
            [[(replsfx, 'а', 'ить'), (addpfx, 'при')], ('noun', 'verb')],
            [[(replsfx, 'я', 'ить'), (addpfx, 'при')], ('noun', 'verb')],

            # 899
            [[(addsfx, 'ить'), (addpfx, 'про')], ('noun', 'verb')],
            [[(replsfx, 'а', 'ить'), (addpfx, 'про')], ('noun', 'verb')],

            [[(replsfx, 'ый', 'ить'), (addpfx, 'про')], ('adj', 'verb')],

            # 900
            [[(addsfx, 'ить'), (addpfx, 'рас')], ('noun', 'verb')],
            [[(replsfx, 'о', 'ить'), (addpfx, 'рас')], ('noun', 'verb')],
            [[(replsfx, 'а', 'ить'), (addpfx, 'рас')], ('noun', 'verb')],

            [[(addsfx, 'ить'), (addpfx, 'раз')], ('noun', 'verb')],
            [[(replsfx, 'а', 'ить'), (addpfx, 'раз')], ('noun', 'verb')],
            [[(replsfx, 'о', 'ить'), (addpfx, 'раз')], ('noun', 'verb')],

            [[(replsfx, 'ый', 'ить'), (addpfx, 'раз')], ('adj', 'verb')],
            [[(replsfx, 'ый', 'ить'), (addpfx, 'рас')], ('adj', 'verb')],
            [[(replsfx, 'ий', 'ить'), (addpfx, 'раз')], ('adj', 'verb')],
            [[(replsfx, 'ий', 'ить'), (addpfx, 'рас')], ('adj', 'verb')],

            # 901
            [[(replsfx, 'а', 'ить'), (addpfx, 'с')], ('noun', 'verb')],

            [[(replsfx, 'ий', 'ить'), (addpfx, 'с')], ('adj', 'verb')],

            # 902
            [[(replsfx, 'ый', 'ить'), (addpfx, 'у')], ('adj', 'verb')],
            [[(replsfx, 'ий', 'ить'), (addpfx, 'у')], ('adj', 'verb')],

            # 904
            [[(addsfx, 'еть'), (addpfx, 'за')], ('noun', 'verb')],
            [[(replsfx, 'а', 'еть'), (addpfx, 'за')], ('noun', 'verb')],

            [[(replsfx, 'ый', 'еть'), (addpfx, 'за')], ('adj', 'verb')],
            [[(replsfx, 'ий', 'еть'), (addpfx, 'за')], ('adj', 'verb')],
            [[(replsfx, 'ой', 'еть'), (addpfx, 'за')], ('adj', 'verb')],

            # 905
            [[(replsfx, 'ый', 'еть'), (addpfx, 'о')], ('adj', 'verb')],
            [[(replsfx, 'ий', 'еть'), (addpfx, 'о')], ('adj', 'verb')],
            [[(replsfx, 'ой', 'еть'), (addpfx, 'о')], ('adj', 'verb')],

            # 906
            [[(addsfx, 'еть'), (addpfx, 'обо')], ('noun', 'verb')],
            [[(replsfx, 'а', 'еть'), (addpfx, 'обо')], ('noun', 'verb')],
            [[(addsfx, 'еть'), (addpfx, 'об')], ('noun', 'verb')],
            [[(replsfx, 'а', 'еть'), (addpfx, 'об')], ('noun', 'verb')],

            # 907
            [[(addsfx, 'еть'), (addpfx, 'обес')], ('noun', 'verb')],
            [[(addsfx, 'еть'), (addpfx, 'обез')], ('noun', 'verb')],
            [[(replsfx, 'а', 'еть'), (addpfx, 'обез')], ('noun', 'verb')],
            [[(replsfx, 'а', 'еть'), (addpfx, 'обес')], ('noun', 'verb')],
            [[(replsfx, 'о', 'еть'), (addpfx, 'обез')], ('noun', 'verb')],
            [[(replsfx, 'о', 'еть'), (addpfx, 'обес')], ('noun', 'verb')],

            # 908
            [[(replsfx, 'ый', 'еть'), (addpfx, 'по')], ('adj', 'verb')],
            [[(replsfx, 'ий', 'еть'), (addpfx, 'по')], ('adj', 'verb')],
            [[(replsfx, 'ой', 'еть'), (addpfx, 'по')], ('adj', 'verb')],

            ### ГЛАГОЛЫ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

            # 914
            [[(replsfx, 'ать', 'ывать'), (addpfx, 'вы')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'ывать'), (addpfx, 'вы')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'ивать'), (addpfx, 'вы')], ('verb', 'verb')],

            # 915
            [[(replsfx, 'ать', 'ывать'), (addpfx, 'на')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'ывать'), (addpfx, 'на')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'ивать'), (addpfx, 'на')], ('verb', 'verb')],

            # 916
            [[(replsfx, 'ать', 'ывать'), (addpfx, 'от')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'ывать'), (addpfx, 'от')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'ивать'), (addpfx, 'от')], ('verb', 'verb')],

            # 917
            [[(replsfx, 'ать', 'ывать'), (addpfx, 'пере')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'ывать'), (addpfx, 'пере')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'ивать'), (addpfx, 'пере')], ('verb', 'verb')],

            # 918
            [[(replsfx, 'ать', 'ывать'), (addpfx, 'по')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'ывать'), (addpfx, 'по')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'ивать'), (addpfx, 'по')], ('verb', 'verb')],

            # 919
            [[(replsfx, 'ать', 'ывать'), (addpfx, 'под')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'ывать'), (addpfx, 'под')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'ивать'), (addpfx, 'под')], ('verb', 'verb')],
            [[(replsfx, 'уть', 'увать'), (addpfx, 'под')], ('verb', 'verb')],

            # 920
            [[(replsfx, 'ать', 'ывать'), (addpfx, 'при')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'ывать'), (addpfx, 'при')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'ивать'), (addpfx, 'при')], ('verb', 'verb')],

            # 921
            [[(replsfx, 'ать', 'ывать'), (addpfx, 'раз')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'ывать'), (addpfx, 'раз')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'ивать'), (addpfx, 'раз')], ('verb', 'verb')],

            # 923
            [[(replsfx, 'ать', 'нуть'), (addpfx, 'вз')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'нуть'), (addpfx, 'вз')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'нуть'), (addpfx, 'вз')], ('verb', 'verb')],

            [[(replsfx, 'ать', 'нуть'), (addpfx, 'вс')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'нуть'), (addpfx, 'вс')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'нуть'), (addpfx, 'вс')], ('verb', 'verb')],

            # 924
            [[(replsfx, 'ать', 'нуть'), (addpfx, 'при')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'нуть'), (addpfx, 'при')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'нуть'), (addpfx, 'при')], ('verb', 'verb')],

            # 925
            [[(replsfx, 'ать', 'нуть'), (addpfx, 'с')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'нуть'), (addpfx, 'с')], ('verb', 'verb')],
            [[(replsfx, 'ить', 'нуть'), (addpfx, 'с')], ('verb', 'verb')],

            # 927
            [[(replsfx, 'ать', 'ить'), (addpfx, 'в')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'вы')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'вз')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'до')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'недо')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'об')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'от')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'пере')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'при')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'про')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'раз')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'за')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'на')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'по')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'под')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'с')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'ить'), (addpfx, 'у')], ('verb', 'verb')],

            [[(replsfx, 'ять', 'ить'), (addpfx, 'в')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'вы')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'вз')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'до')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'недо')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'об')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'от')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'пере')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'при')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'про')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'раз')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'за')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'на')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'по')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'под')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'с')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'ить'), (addpfx, 'у')], ('verb', 'verb')],

            ## ПОСТФИКСАЛЬНЫЕ ГЛАГОЛЫ

            # 930
            [[(addsfx, 'ся')], ('verb', 'verb')],
            [[(addsfx, 'сь')], ('verb', 'verb')],

            ## СУФФИКСАЛЬНО-ПОСТФИКСАЛЬНЫЕ ГЛАГОЛЫ

            # 932
            [[(addsfx, 'иться')], ('noun', 'verb')],
            [[(replsfx, 'а', 'иться')], ('noun', 'verb')],

            # 933
            [[(replsfx, 'ый', 'иться')], ('adj', 'verb')],
            [[(replsfx, 'ий', 'иться')], ('adj', 'verb')],
            [[(replsfx, 'ой', 'иться')], ('adj', 'verb')],

            ## ПРЕФИКСАЛЬНО-ПОСТФИКСАЛЬНЫЕ ГЛАГОЛЫ

            # 937
            [[(addsfx, 'ся'), (addpfx, 'в')], ('verb', 'verb')],

            # 938
            [[(addsfx, 'ся'), (addpfx, 'вз')], ('verb', 'verb')],
            [[(addsfx, 'ся'), (addpfx, 'взо')], ('verb', 'verb')],

            # 939
            [[(addsfx, 'ся'), (addpfx, 'вы')], ('verb', 'verb')],

            # 940
            [[(addsfx, 'ся'), (addpfx, 'до')], ('verb', 'verb')],

            # 941
            [[(addsfx, 'ся'), (addpfx, 'за')], ('verb', 'verb')],

            # 942
            [[(addsfx, 'ся'), (addpfx, 'из')], ('verb', 'verb')],
            [[(addsfx, 'ся'), (addpfx, 'ис')], ('verb', 'verb')],

            # 943
            [[(addsfx, 'ся'), (addpfx, 'на')], ('verb', 'verb')],

            # 944
            [[(addsfx, 'ся'), (addpfx, 'о')], ('verb', 'verb')],

            # 945
            [[(addsfx, 'ся'), (addpfx, 'об')], ('verb', 'verb')],
            [[(addsfx, 'ся'), (addpfx, 'обо')], ('verb', 'verb')],

            # 946
            [[(addsfx, 'ся'), (addpfx, 'от')], ('verb', 'verb')],
            [[(addsfx, 'ся'), (addpfx, 'ото')], ('verb', 'verb')],

            # 947
            [[(addsfx, 'ся'), (addpfx, 'под')], ('verb', 'verb')],
            [[(addsfx, 'ся'), (addpfx, 'подо')], ('verb', 'verb')],

            # 948
            [[(addsfx, 'ся'), (addpfx, 'при')], ('verb', 'verb')],

            # 949
            [[(addsfx, 'ся'), (addpfx, 'про')], ('verb', 'verb')],

            # 950
            [[(addsfx, 'ся'), (addpfx, 'раз')], ('verb', 'verb')],
            [[(addsfx, 'ся'), (addpfx, 'разо')], ('verb', 'verb')],

            # 951
            [[(addsfx, 'ся'), (addpfx, 'с')], ('verb', 'verb')],
            [[(addsfx, 'ся'), (addpfx, 'со')], ('verb', 'verb')],

            # 952
            [[(addsfx, 'ся'), (addpfx, 'у')], ('verb', 'verb')],

            ## ПРЕФИКСАЛЬНО-СУФФИКСАЛЬНО-ПОСТФИКСАЛЬНЫЕ ГЛАГОЛЫ

            # 955
            [[(addsfx, 'иться'), (addpfx, 'о')], ('noun', 'verb')],
            [[(replsfx, 'а', 'иться'), (addpfx, 'о')], ('noun', 'verb')],

            # 956
            [[(addsfx, 'иться'), (addpfx, 'об')], ('noun', 'verb')],
            [[(replsfx, 'а', 'иться'), (addpfx, 'об')], ('noun', 'verb')],

            # 959
            [[(replsfx, 'ить', 'иваться'), (addpfx, 'пере')], ('verb', 'verb')],
            [[(replsfx, 'ать', 'иваться'), (addpfx, 'пере')], ('verb', 'verb')],
            [[(replsfx, 'ять', 'иваться'), (addpfx, 'пере')], ('verb', 'verb')],
            [[(replsfx, 'еть', 'ываться'), (addpfx, 'пере')], ('verb', 'verb')],


        ]

        all_vars = []

        for r in rules:
            prev_lvl_words = [self.word]
            if self.pos != r[1][0]:
                continue
            for changes in r[0]:
                cur_lvl_words = []
                for w in prev_lvl_words:
                    f, args = changes[0], list(changes[1:])
                    list_new = f(w, args)
                    for new_w in list_new:
                        cur_lvl_words.append(new_w)
                prev_lvl_words = deepcopy(cur_lvl_words)
            for w in prev_lvl_words:
                all_vars.append(w)

        return all_vars


test = DerivationADV('сильный', 'adj')
ans = test.generate()
for x in ans:
    print(x, end=', ')
