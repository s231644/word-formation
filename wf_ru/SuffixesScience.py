class Suffix:
    def __init__(self, pos: str, pos_f: str, gr: str, alt_t: str, alts: list(),
                 prod: bool, on_b: list(), ex_b: list()):
        self.graphics = gr
        self.pos = pos
        self.pos_from = pos_f
        self.alternation_type = alt_t
        self.alternations = alts
        self.productive = prod
        self.only_before = on_b
        self.except_before = ex_b


class Prefix:
    def __init__(self, pos: str, pos_f: str, gr: str, alt_t: str, alts: list(),
                 prod: bool, on_a: list(), ex_a: list()):
        self.graphics = gr
        self.pos = pos
        self.pos_from = pos_f
        self.alternation_type = alt_t
        self.alternations = alts
        self.productive = prod
        self.only_after = on_a
        self.except_after = ex_a


class POS:
    def __init__(self, gr: str, pos: str, history=[]):
        self.pos = pos
        self.graphics = gr
        self.history = history


class Derivation:
    @staticmethod
    def apply_pattern(word: POS, rules):
        pos_b, pos_a = rules[1]
        cur_vars = [word]
        for rule in rules[0]:
            f, args = rule[0], rule[1:]
            cur_vars_new = []
            for cur_var in cur_vars:
                cur_vars_new += f(cur_var, args, pos_b, pos_a)
            cur_vars = cur_vars_new
        return cur_vars


paired_hard = ['Б', 'П', 'М', 'В', 'Ф', 'Д', 'Т', 'З', 'С', 'Н', 'Л', 'Р']
paired_soft = ['б', 'п', 'м', 'в', 'ф', 'д', 'т', 'з', 'с', 'н', 'л', 'р']
paired = paired_hard + paired_soft
velar_hard = ['Г', 'К', 'Х']
velar_soft = ['г', 'к', 'х']
velar = velar_hard + velar_soft
sh_hard = ['Ш', 'Ж']
sh_soft = ['щ', 'ч']
sh = sh_hard + sh_soft
vocal_hard = ['а', 'о', 'у', 'ы', 'э']
vocal_soft = ['и', 'я', 'ю', 'е', 'ё']
consonants = paired + velar + sh + ['ъ', 'ь']
vocals = vocal_hard + vocal_soft


def replsfx(word: POS, args, pos_b, pos_a):
    sfx_b, sfx_a = args
    try_sub = []
    possible_words = list()
    sfx_a_ = 0
    for elem in adv_suffixes:
        if isinstance(elem, Suffix) and elem.graphics == sfx_a and elem.pos_from == pos_b and elem.pos == pos_a:
            sfx_a_ = elem
            alt_t = int(float(elem.alternation_type))
            try_sub = alternation_types[alt_t]
    if not isinstance(sfx_a_, Suffix):
        return []

    if len(word.graphics) > len(sfx_b) and word.graphics[-len(sfx_b):] == sfx_b:
        w_a = word.graphics[:-len(sfx_b)]
        # print(sfx_a, sfx_a_.except_before)
        for letters_not_before in sfx_a_.except_before:
            # print(letters_not_before)
            if len(w_a) >= len(letters_not_before) and w_a[-len(letters_not_before):].lower() == letters_not_before:
                # print(w_a, letters_not_before)
                return []

        if sfx_a_.only_before:
            for letters_before in sfx_a_.only_before:
                # print(w_a, letters_before)
                if len(w_a) >= len(letters_before) and w_a[-len(letters_before):].lower() == letters_before:
                    for sub in try_sub:
                        if len(w_a) >= len(sub[0]) and w_a[-len(sub[0]):] == sub[0]:
                            new_word = w_a[:-len(sub[0])] + sub[1] + sfx_a
                            possible_words.append(new_word)
        else:
            for sub in try_sub:
                if len(w_a) >= len(sub[0]) and w_a[-len(sub[0]):] == sub[0]:
                    new_word = w_a[:-len(sub[0])] + sub[1] + sfx_a
                    possible_words.append(new_word)

    possible_results = list()
    for possible_word in possible_words:
        new_word = POS(possible_word, pos_b, word.history + [(replsfx, sfx_b, sfx_a, pos_b, pos_a)])
        possible_results.append(new_word)

    return possible_results


def addsfx(word: POS, args, pos_b, pos_a):
    sfx_a = args[0]
    try_sub = []
    possible_words = list()
    sfx_a_ = 0
    for elem in adv_suffixes:
        if isinstance(elem, Suffix) and elem.graphics == sfx_a and elem.pos_from == pos_b and elem.pos == pos_a:
            sfx_a_ = elem
            alt_t = int(float(elem.alternation_type))
            try_sub = alternation_types[alt_t]
    if not isinstance(sfx_a_, Suffix):
        return []

    w_a = word.graphics
    for letters_not_before in sfx_a_.except_before:
        if len(w_a) >= len(letters_not_before) and w_a[-len(letters_not_before):].lower() == letters_not_before:
            return []

    if sfx_a_.only_before:
        for letters_before in sfx_a_.only_before:
            if len(w_a) >= len(letters_before) and w_a[-len(letters_before):].lower() == letters_before:
                for sub in try_sub:
                    if len(w_a) >= len(sub[0]) and w_a[-len(sub[0]):] == sub[0]:
                        new_word = w_a[:-len(sub[0])] + sub[1] + sfx_a
                        possible_words.append(new_word)
    else:
        for sub in try_sub:
            if len(w_a) >= len(sub[0]) and w_a[-len(sub[0]):] == sub[0]:
                new_word = w_a[:-len(sub[0])] + sub[1] + sfx_a
                possible_words.append(new_word)

    possible_results = list()
    for possible_word in possible_words:
        new_word = POS(possible_word, pos_b, word.history + [(addsfx, sfx_a, pos_b, pos_a)])
        possible_results.append(new_word)

    return possible_results


def addpfx(word: POS, args, pos_b, pos_a):
    pfx_a = args[0]
    try_sub = []
    possible_words = list()
    pfx_a_ = 0
    for elem in adv_suffixes:
        if isinstance(elem, Prefix) and elem.graphics == pfx_a and elem.pos_from == pos_b and elem.pos == pos_a:
            pfx_a_ = elem
    if not isinstance(pfx_a_, Prefix):
        return []

    w_a = word.graphics
    for letters_not_after in pfx_a_.except_after:
        if len(w_a) >= len(letters_not_after) and w_a[:len(letters_not_after)].lower() == letters_not_after:
            return []

    if pfx_a_.only_after:
        for letters_after in pfx_a_.only_after:
            if len(w_a) >= len(letters_after) and w_a[:len(letters_after)].lower() == letters_after:
                new_word = pfx_a + w_a
                possible_words.append(new_word)
    else:
        new_word = pfx_a + w_a
        possible_words.append(new_word)

    possible_results = list()
    for possible_word in possible_words:
        new_word = POS(possible_word, pos_b, word.history + [(addpfx, pfx_a, pos_b, pos_a)])
        possible_results.append(new_word)

    return possible_results


alternation_types = [
    # Альтернационные типы. БОЛЬШИЕ буквы --- твердые, маленькие --- мягкие.

    [
        # Особое место занимают словообразовательные морфы, не вызывающие никаких чередований и, в частности, ни
        # смягчения, ни отвердения предшествующих согласных. Такие морфы в линейной структуре мотивированного слова
        # выступают как после парно-твердых согласных, так и после парно-мягких, а также после шипящих и |ц|.

        # губно-губные
        ('Б', 'Б'), ('П', 'П'), ('М', 'М'),
        ('б', 'б'), ('п', 'п'), ('м', 'м'),
        # губно-зубные
        ('В', 'В'), ('Ф', 'Ф'),
        ('в', 'в'), ('ф', 'ф'),
        # язычно-зубные
        ('Д', 'Д'), ('Т', 'Т'), ('З', 'З'), ('С', 'С'), ('Н', 'Н'), ('Л', 'Л'),
        ('д', 'д'), ('т', 'т'), ('з', 'з'), ('с', 'с'), ('н', 'н'), ('л', 'л'),
        ('Ц', 'Ц'),
        # язычно-альвеолярные
        ('Р', 'Р'),
        ('р', 'р'),
        # язычно-передненёбные
        ('Ж', 'Ж'), ('Ш', 'Ш'),
        ('щ', 'щ'), ('ч', 'ч'),
        # язычно-средне- и задненёбные
        ('Г', 'Г'), ('К', 'К'), ('Х', 'Х'),
        ('г', 'г'), ('к', 'к'), ('х', 'х'),

        ('ь', 'ь'),

     ],
    [
        # К I альтернационному типу принадлежат морфы, перед которыми парно­твердые согласные, включая заднеязычные,
        # чередуются с соответствующими мягкими (1­я разновидность чередований, см. § 1088), а парно­мягкие остаются
        # неизменными. В линейной структуре мотивированных слов такие морфы выступают после парно­мягких согласных,
        # в том числе заднеязычных, а иногда и после шипящих.

        # губно-губные
        ('Б', 'б'), ('П', 'п'), ('М', 'м'),
        ('б', 'б'), ('п', 'п'), ('м', 'м'),
        # губно-зубные
        ('В', 'в'), ('Ф', 'ф'),
        ('в', 'в'), ('ф', 'ф'),
        # язычно-зубные
        ('Д', 'д'), ('Т', 'т'), ('З', 'з'), ('С', 'с'), ('Н', 'н'), ('Л', 'л'),
        ('д', 'д'), ('т', 'т'), ('з', 'з'), ('с', 'с'), ('н', 'н'), ('л', 'л'),
        ('Ц', 'Ц'),
        # язычно-альвеолярные
        ('Р', 'р'),
        ('р', 'р'),
        # язычно-передненёбные
        ('Ж', 'Ж'), ('Ш', 'Ш'),
        ('щ', 'щ'), ('ч', 'ч'),
        # язычно-средне- и задненёбные
        ('Г', 'г'), ('К', 'к'), ('Х', 'х'),
        ('г', 'г'), ('к', 'к'), ('х', 'х'),

('ь', ''),

    ],
    [
        # Ко II альтернационному типу принадлежат морфы, перед которыми парно­твердые согласные, кроме заднеязычных,
        # чередуются с соответствующими мягкими (1­я разновидность чередований), а заднеязычные – с шипящими:
        # |К – ч|, |Г – Ж|, |Х – Ш|, реже |ск – щ| (3­я разновидность чередований, см. § 1088); парно­мягкие согласные
        # (кроме заднеязычных) остаются перед такими морфами без изменений. В линейной структуре мотивированных слов
        # эти морфы выступают после парно­мягких согласных, кроме заднеязычных, и после шипящих.

        # губно-губные
        ('Б', 'б'), ('П', 'п'), ('М', 'м'),
        ('б', 'б'), ('п', 'п'), ('м', 'м'),
        # губно-зубные
        ('В', 'в'), ('Ф', 'ф'),
        ('в', 'в'), ('ф', 'ф'),
        # язычно-зубные
        ('Д', 'д'), ('Т', 'т'), ('З', 'з'), ('С', 'с'), ('Н', 'н'), ('Л', 'л'),
        ('д', 'д'), ('т', 'т'), ('з', 'з'), ('с', 'с'), ('н', 'н'), ('л', 'л'),
        ('Ц', 'Ц'),
        # язычно-альвеолярные
        ('Р', 'р'),
        ('р', 'р'),
        # язычно-передненёбные
        ('Ж', 'Ж'), ('Ш', 'Ш'),
        ('щ', 'щ'), ('ч', 'ч'),
        # язычно-средне- и задненёбные
        ('Г', 'Ж'), ('К', 'ч'), ('Х', 'Ш'),
        ('г', 'Ж'), ('к', 'ч'), ('х', 'Ш'),

        ('сК', 'щ'), ('еЦ', 'ч'), ('яЦ', 'йч'), ('иЦ', 'ич'), ('яЦ', 'яч'),
        ('аЦ', 'ач'), ('Ц', 'ч'),
('ь', ''),
    ],
    [
        # К III альтернационному типу принадлежат морфы, перед которыми парно­твердые согласные, кроме заднеязычных,
        # чередуются с соответствующими мягкими (1­я разновидность чередований, см. § 1088), заднеязычные – с шипящими
        # (3­я разновидность), а парно­мягкие шумные зубные и губные – с шипящими и (соответственно) сочетаниями
        # "губная + |л'|" (5­я разновидность чередований, см. § 1088). В линейной структуре мотивированных слов
        # эти морфы выступают после парно­мягких согласных, кроме заднеязычных, и после шипящих.

        # губно-губные
        ('Б', 'б'), ('П', 'п'), ('М', 'м'),
        ('б', 'Бл'), ('п', 'Пл'), ('м', 'Мл'),
        # губно-зубные
        ('В', 'в'), ('Ф', 'ф'),
        ('в', 'Вл'), ('ф', 'Фл'),
        # язычно-зубные
        ('Д', 'д'), ('Т', 'т'), ('З', 'з'), ('С', 'с'), ('Н', 'н'), ('Л', 'л'),
        ('д', 'Ж'), ('т', 'ч'), ('з', 'Ж'), ('с', 'Ш'), ('н', 'н'), ('л', 'л'),
        ('Ц', 'Ц'),
        # язычно-альвеолярные
        ('Р', 'р'),
        ('р', 'р'),
        # язычно-передненёбные
        ('Ж', 'Ж'), ('Ш', 'Ш'),
        ('щ', 'щ'), ('ч', 'ч'),
        # язычно-средне- и задненёбные
        ('Г', 'Ж'), ('К', 'ч'), ('Х', 'Ш'),
        ('г', 'Ж'), ('к', 'ч'), ('х', 'Ш'),

        ('ст', 'щ'), ('с', 'т'), ('оч', 'ч'), ('З', 'Ж'), ('Ц', 'ч'), ('Т', 'ч'),
        ('з', 'Ж'),
('ь', ''),
    ],
    [
        # К IV альтернационному типу принадлежат морфы, перед которыми парно­твердые согласные, кроме заднеязычных,
        # чередуются с соответствующими мягкими, а твердые заднеязычные и парно­мягкие согласные остаются неизменными.

        # губно-губные
        ('Б', 'б'), ('П', 'п'), ('М', 'м'),
        ('б', 'б'), ('п', 'п'), ('м', 'м'),
        # губно-зубные
        ('В', 'в'), ('Ф', 'ф'),
        ('в', 'в'), ('ф', 'ф'),
        # язычно-зубные
        ('Д', 'д'), ('Т', 'т'), ('З', 'з'), ('С', 'с'), ('Н', 'н'), ('Л', 'л'),
        ('д', 'д'), ('т', 'т'), ('з', 'з'), ('с', 'с'), ('н', 'н'), ('л', 'л'),
        ('Ц', 'Ц'),
        # язычно-альвеолярные
        ('Р', 'р'),
        ('р', 'р'),
        # язычно-передненёбные
        ('Ж', 'Ж'), ('Ш', 'Ш'),
        ('щ', 'щ'), ('ч', 'ч'),
        # язычно-средне- и задненёбные
        ('Г', 'Г'), ('К', 'К'), ('Х', 'Х'),
        ('г', 'г'), ('к', 'к'), ('х', 'х'),
('ь', 'ь'),
    ],
    [
        # K V альтернационному типу принадлежат суф. морфы, перед которыми парно-мягкие согласные (без ограничений)
        # чередуются с соответствующими твердыми (2-я разновидность чередований, см. § 1088), а парно-твердые согласные
        # остаются неизменными.

        # губно-губные
        ('Б', 'Б'), ('П', 'П'), ('М', 'М'),
        ('б', 'Б'), ('п', 'П'), ('м', 'М'),
        # губно-зубные
        ('В', 'В'), ('Ф', 'Ф'),
        ('в', 'В'), ('ф', 'Ф'),
        # язычно-зубные
        ('Д', 'Д'), ('Т', 'Т'), ('З', 'З'), ('С', 'С'), ('Н', 'Н'), ('Л', 'Л'),
        ('д', 'Т'), ('т', 'Т'), ('з', 'З'), ('с', 'С'), ('н', 'Н'), ('л', 'Л'),
        ('Ц', 'Ц'),
        # язычно-альвеолярные
        ('Р', 'Р'),
        ('р', 'Р'),
        # язычно-передненёбные
        ('Ж', 'Ж'), ('Ш', 'Ш'),
        ('щ', 'щ'), ('ч', 'ч'),
        # язычно-средне- и задненёбные
        ('Г', 'Г'), ('К', 'К'), ('Х', 'Х'),
        ('г', 'Г'), ('к', 'К'), ('х', 'Х'),
('ь', 'ь'),
    ],
    [
        # К VI альтернационному типу принадлежат суф. морфы, перед которыми все парно-мягкие согласные, кроме |л'|,
        # чередуются с соответствующими твердыми (2-я разновидность чередований, см. § 1088), а парно-твердые и |л'|
        # остаются неизменными, причем при наличии в основе мотивирующего слова согласной |л| выступает и чередование
        # |л – л'| (относящееся к 1-й разновидности, см. § 1088). В линейной структуре мотивированных слов такие морфы
        # выступают после парно-твердых согласных, кроме |л|, в том числе заднеязычных, и согласной |л'|.

        # губно-губные
        ('Б', 'Б'), ('П', 'П'), ('М', 'М'),
        ('б', 'Б'), ('п', 'П'), ('м', 'М'),
        # губно-зубные
        ('В', 'В'), ('Ф', 'Ф'),
        ('в', 'В'), ('ф', 'Ф'),
        # язычно-зубные
        ('Д', 'Д'), ('Т', 'Т'), ('З', 'З'), ('С', 'С'), ('Н', 'Н'), ('Л', 'л'),
        ('д', 'Д'), ('т', 'Т'), ('з', 'З'), ('с', 'С'), ('н', 'Н'), ('л', 'л'),
        ('Ц', 'Ц'),
        # язычно-альвеолярные
        ('Р', 'Р'),
        ('р', 'Р'),
        # язычно-передненёбные
        ('Ж', 'Ж'), ('Ш', 'Ш'),
        ('щ', 'щ'), ('ч', 'ч'),
        # язычно-средне- и задненёбные
        ('Г', 'Г'), ('К', 'К'), ('Х', 'Х'),
        ('г', 'Г'), ('к', 'К'), ('х', 'Х'),
('ь', 'ь'),
    ],
    [
        # К VII альтернационному типу принадлежат суф. морфы, перед которыми парно-твердые согласные, кроме
        # заднеязычных, остаются неизменными, парно-мягкие, кроме заднеязычных, чередуются с соответствующими твердыми
        # (2-я разновидность чередований), а твердые заднеязычные – с соответствующими мягкими (1-я разновидность,
        # см. § 1088).

        # губно-губные
        ('Б', 'Б'), ('П', 'П'), ('М', 'М'),
        ('б', 'Б'), ('п', 'П'), ('м', 'М'),
        # губно-зубные
        ('В', 'В'), ('Ф', 'Ф'),
        ('в', 'В'), ('ф', 'Ф'),
        # язычно-зубные
        ('Д', 'Д'), ('Т', 'Т'), ('З', 'З'), ('С', 'С'), ('Н', 'Н'), ('Л', 'Л'),
        ('д', 'Д'), ('т', 'Т'), ('з', 'З'), ('с', 'С'), ('н', 'Н'), ('л', 'Л'),
        ('Ц', 'Ц'),
        # язычно-альвеолярные
        ('Р', 'Р'),
        ('р', 'Р'),
        # язычно-передненёбные
        ('Ж', 'Ж'), ('Ш', 'Ш'),
        ('щ', 'щ'), ('ч', 'ч'),
        # язычно-средне- и задненёбные
        ('Г', 'г'), ('К', 'к'), ('Х', 'х'),
        ('г', 'Г'), ('к', 'К'), ('х', 'Х'),
('ь', 'ь'),
    ],
    [
        # К VIII альтернационному типу принадлежат морфы, перед которыми чередуются парно-мягкие согласные, кроме
        # заднеязычных, с соответствующими твердыми (2-я разновидность чередований, см. § 1088), парно-твердые, кроме
        # заднеязычных, остаются неизменными, а заднеязычные чередуются с шипящими (3-я разновидность).
        #
        # При мотивации существительными с основой на сочетания
        # "беглая гласная + |ц|", |ец|, |иц| перед морфами, относящимися к VIII альтернац. типу, чередуются |ц – ч|:
        # кольцо – колечко, птица – птичка (но цаца – цацка с сохранением финали |ац|).

        # губно-губные
        ('Б', 'Б'), ('П', 'П'), ('М', 'М'),
        ('б', 'Б'), ('п', 'П'), ('м', 'М'),
        # губно-зубные
        ('В', 'В'), ('Ф', 'Ф'),
        ('в', 'В'), ('ф', 'Ф'),
        # язычно-зубные
        ('Д', 'Д'), ('Т', 'Т'), ('З', 'З'), ('С', 'С'), ('Н', 'Н'), ('Л', 'Л'),
        ('д', 'Д'), ('т', 'Т'), ('з', 'З'), ('с', 'С'), ('н', 'Н'), ('л', 'Л'),
        ('Ц', 'Ц'),
        # язычно-альвеолярные
        ('Р', 'Р'),
        ('р', 'Р'),
        # язычно-передненёбные
        ('Ж', 'Ж'), ('Ш', 'Ш'),
        ('щ', 'щ'), ('ч', 'ч'),
        # язычно-средне- и задненёбные
        ('Г', 'Г'), ('К', 'К'), ('Х', 'Х'),
        ('г', 'Ж'), ('к', 'ч'), ('х', 'Ш'),

        ('ьК', 'еч'), ('ьЦ', 'еч'), ('иЦ', 'ич'),
('ь', 'ь'),
    ],

]


adv_suffixes = [

    ### СУФФИКСАЛЬНЫЕ НАРЕЧИЯ
    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ ПРИЛАГАТЕЛЬНЫМИ

    # 977
    Suffix(pos='adv', pos_f='adj', gr='о', alt_t='5.0', alts=[], prod=True, on_b=[], ex_b=['ск', 'цк']),
    Suffix(pos='adv', pos_f='adj', gr='е', alt_t='2.0', alts=[], prod=True, on_b=[], ex_b=['ск', 'цк']),

    # 979
    Suffix(pos='adv', pos_f='adj', gr='и', alt_t='1', alts=[], prod=True, on_b=['ск', 'цк'], ex_b=[]),

    # 980
    Suffix(pos='adv', pos_f='adj', gr='ком', alt_t='8', alts=[], prod=False, on_b=[], ex_b=['ск', 'цк']),
    Suffix(pos='adv', pos_f='adj', gr='иком', alt_t='2', alts=[], prod=False, on_b=[], ex_b=['ск', 'цк']),

    # 981


    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ СУЩЕСТВИТЕЛЬНЫМИ

    # 982
    Suffix(pos='adv', pos_f='noun', gr='ом', alt_t='5.0', alts=[], prod=True, on_b=consonants, ex_b=[]),
    Suffix(pos='adv', pos_f='noun', gr='ём', alt_t='1.0', alts=[], prod=True, on_b=consonants, ex_b=[]),
    Suffix(pos='adv', pos_f='noun', gr='ем', alt_t='1.0', alts=[], prod=True, on_b=consonants, ex_b=[]),
    Suffix(pos='adv', pos_f='noun', gr='ой', alt_t='5.0', alts=[], prod=True, on_b=consonants, ex_b=[]),
    Suffix(pos='adv', pos_f='noun', gr='ою', alt_t='5.0', alts=[], prod=True, on_b=consonants, ex_b=[]),
    Suffix(pos='adv', pos_f='noun', gr='ю', alt_t='1.0', alts=[], prod=True, on_b=consonants, ex_b=[]),
    Suffix(pos='adv', pos_f='noun', gr='ами', alt_t='5.0', alts=[], prod=True, on_b=consonants, ex_b=[]),
    Suffix(pos='adv', pos_f='noun', gr='ями', alt_t='1.0', alts=[], prod=True, on_b=consonants, ex_b=[]),


    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ ЧИСЛИТЕЛЬНЫМИ

    # 983
    Suffix(pos='adv', pos_f='num', gr='ю', alt_t='0.0', alts=[], prod=False, on_b=['ь'], ex_b=[]),

    # 984


    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

    # 985
    Suffix(pos='adv', pos_f='verb', gr='мя', alt_t='0.0', alts=[], prod=True, on_b=[], ex_b=[]),

    # 986
    Suffix(pos='adv', pos_f='verb', gr='ом', alt_t='5.0', alts=[], prod=False, on_b=paired + velar, ex_b=[]),
    Suffix(pos='adv', pos_f='verb', gr='ком', alt_t='8', alts=[], prod=False, on_b=paired + sh, ex_b=[]),

    # 987


    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ НАРЕЧИЯМИ

    # 988
    Suffix(pos='adv', pos_f='adv', gr='овато', alt_t='5.0', alts=[], prod=True, on_b=[], ex_b=[]),

    # 989
    Suffix(pos='adv', pos_f='adv', gr='енько', alt_t='2', alts=[], prod=True, on_b=[], ex_b=velar),
    Suffix(pos='adv', pos_f='adv', gr='онько', alt_t='6.0', alts=[], prod=True, on_b=velar, ex_b=[]),

    # 990
    Suffix(pos='adv', pos_f='adv', gr='охонько', alt_t='4', alts=[], prod=True, on_b=[], ex_b=[]),
    Suffix(pos='adv', pos_f='adv', gr='ёхонько', alt_t='4', alts=[], prod=True, on_b=[], ex_b=[]),
    Suffix(pos='adv', pos_f='adv', gr='ошенько', alt_t='4', alts=[], prod=True, on_b=[], ex_b=[]),
    Suffix(pos='adv', pos_f='adv', gr='ёшенько', alt_t='4', alts=[], prod=True, on_b=[], ex_b=[]),

    # 991
    Suffix(pos='adv', pos_f='adv', gr='ком', alt_t='2.0', alts=[], prod=True, on_b=consonants, ex_b=[]),
    Suffix(pos='adv', pos_f='adv', gr='кой', alt_t='2.0', alts=[], prod=True, on_b=consonants, ex_b=[]),
    Suffix(pos='adv', pos_f='adv', gr='ку', alt_t='2.0', alts=[], prod=True, on_b=consonants, ex_b=[]),
    Suffix(pos='adv', pos_f='adv', gr='ко', alt_t='2.0', alts=[], prod=True, on_b=consonants, ex_b=[]),

    ### ПРЕФИКСАЛЬНЫЕ НАРЕЧИЯ

    # 992
    Prefix(pos='adv', pos_f='adv', gr='не', alt_t='', alts=[], prod=True, on_a=[], ex_a=[]),

    # 993
    Prefix(pos='adv', pos_f='adv', gr='во', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Prefix(pos='adv', pos_f='adv', gr='за', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Prefix(pos='adv', pos_f='adv', gr='до', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Prefix(pos='adv', pos_f='adv', gr='на', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Prefix(pos='adv', pos_f='adv', gr='по', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Prefix(pos='adv', pos_f='adv', gr='от', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Prefix(pos='adv', pos_f='adv', gr='ото', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),

    ### ПРЕФИКСАЛЬНО-СУФФИКСАЛЬНЫЕ НАРЕЧИЯ

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ ПРИЛАГАТЕЛЬНЫМИ

    # 994
    Prefix(pos='adv', pos_f='adj', gr='по-', alt_t='', alts=[], prod=True, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='adj', gr='ому', alt_t='0', alts=[], prod=True, on_b=[], ex_b=[]),
    Suffix(pos='adv', pos_f='adj', gr='ему', alt_t='1.0', alts=[], prod=True, on_b=[], ex_b=['ск', 'цк']),

    # 995
    Prefix(pos='adv', pos_f='adj', gr='по-', alt_t='', alts=[], prod=True, on_a=[], ex_a=[]),
    # Suffix(pos='adv', pos_f='adj', gr='и', alt_t='1', alts=[], prod=True, on_b=['ск', 'цк'], ex_b=[]),

    # 996

    # 997

    # 998
    Prefix(pos='adv', pos_f='adj', gr='по-', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='adj', gr='ой', alt_t='0.0', alts=[], prod=False, on_b=[], ex_b=['ск', 'цк']),

    # 999
    Prefix(pos='adv', pos_f='adj', gr='в', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='adj', gr='ую', alt_t='5', alts=[], prod=False, on_b=[], ex_b=['ск', 'цк']),

    # 1000
    Prefix(pos='adv', pos_f='adj', gr='в', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Prefix(pos='adv', pos_f='adj', gr='во', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='adj', gr='е', alt_t='1', alts=[], prod=False, on_b=[], ex_b=['ск', 'цк']),
    # Suffix(pos='adv', pos_f='adj', gr='и', alt_t='1.0', alts=[('зк', 'з')], prod=False, on_b=[], ex_b=['ск', 'цк']),
    # Suffix(pos='adv', pos_f='adj', gr='ых', alt_t='5.0', alts=[], prod=False, on_b=[], ex_b=['ск', 'цк']),
    # Suffix(pos='adv', pos_f='adj', gr='их', alt_t='1.0', alts=[], prod=False, on_b=[], ex_b=['ск', 'цк']),

    # 1001
    Prefix(pos='adv', pos_f='adj', gr='до', alt_t='', alts=[], prod=True, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='adj', gr='а', alt_t='1.0', alts=[], prod=True, on_b=[], ex_b=['ск', 'цк']),
    Suffix(pos='adv', pos_f='adj', gr='я', alt_t='5.0', alts=[], prod=True, on_b=[], ex_b=['ск', 'цк']),

    # 1002

    # 1003
    Prefix(pos='adv', pos_f='adj', gr='из', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='adj', gr='а', alt_t='1.0', alts=[], prod=False, on_b=[], ex_b=[]),
    Prefix(pos='adv', pos_f='adj', gr='ис', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='adj', gr='я', alt_t='5.0', alts=[], prod=False, on_b=[], ex_b=['ск', 'цк']),

    # 1004
    Prefix(pos='adv', pos_f='adj', gr='на', alt_t='', alts=[], prod=True, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='adj', gr='о', alt_t='1.0', alts=[], prod=True, on_b=[], ex_b=['ск', 'цк']),

    # 1005
    Prefix(pos='adv', pos_f='adj', gr='на', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='adj', gr='е', alt_t='1', alts=[], prod=False, on_b=[], ex_b=['ск', 'цк']),

    # 1006
    Prefix(pos='adv', pos_f='adj', gr='с', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Prefix(pos='adv', pos_f='adj', gr='со', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='adj', gr='а', alt_t='1.0', alts=[], prod=False, on_b=[], ex_b=['ск', 'цк']),

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ СУЩЕСТВИТЕЛЬНЫМИ

    # 1009
    Prefix(pos='adv', pos_f='noun', gr='в', alt_t='', alts=[], prod=True, on_a=[], ex_a=[]),
    Prefix(pos='adv', pos_f='noun', gr='во', alt_t='', alts=[], prod=True, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='noun', gr='у', alt_t='5', alts=[], prod=True, on_b=[], ex_b=[]),
    Suffix(pos='adv', pos_f='noun', gr='', alt_t='0', alts=[], prod=True, on_b=[], ex_b=[]),

    # 1010
    Prefix(pos='adv', pos_f='noun', gr='на', alt_t='', alts=[], prod=True, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='noun', gr='у', alt_t='5', alts=[], prod=True, on_b=[], ex_b=[]),
    Suffix(pos='adv', pos_f='noun', gr='', alt_t='0', alts=[], prod=True, on_b=[], ex_b=[]),

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ ЧИСЛИТЕЛЬНЫМИ

    # 1017
    Prefix(pos='adv', pos_f='num', gr='в', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='num', gr='ом', alt_t='5', alts=[], prod=False, on_b=[], ex_b=[]),
    Suffix(pos='adv', pos_f='num', gr='ём', alt_t='1.0', alts=[], prod=False, on_b=[], ex_b=[]),
    Suffix(pos='adv', pos_f='num', gr='о', alt_t='1.0', alts=[], prod=False, on_b=[], ex_b=[]),

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

    # 1020
    Prefix(pos='adv', pos_f='verb', gr='в', alt_t='', alts=[], prod=True, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='verb', gr='ку', alt_t='8', alts=[], prod=True, on_b=[], ex_b=[]),

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ НАРЕЧИЯМИ

    # 1022
    Prefix(pos='adv', pos_f='adv', gr='по', alt_t='', alts=[], prod=False, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='adv', gr='у', alt_t='5.0', alts=[], prod=False, on_b=[], ex_b=[]),

    # ПРЕФИКСАЛЬНЫЕ НАРЕЧИЯ С НУЛЕВЫМ СУФФИКСОМ

    # 1024
    Prefix(pos='adv', pos_f='verb', gr='в', alt_t='', alts=[], prod=True, on_a=[], ex_a=[]),
    Prefix(pos='adv', pos_f='verb', gr='на', alt_t='', alts=[], prod=True, on_a=[], ex_a=[]),
    Suffix(pos='adv', pos_f='verb', gr='', alt_t='0', alts=[], prod=True, on_b=[], ex_b=[]),



    Suffix(pos='adv', pos_f='verb', gr='аючи', alt_t='0.0', alts=[], prod=False, on_b=[], ex_b=[]),

]


rules_adv = [

    ### СУФФИКСАЛЬНЫЕ НАРЕЧИЯ
    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ ПРИЛАГАТЕЛЬНЫМИ

    # 977
    # ('быстрый', 'быстро')
    [[(replsfx, 'ый', 'о')], ('adj', 'adv')],
    # ('тихий', 'тихо')
    [[(replsfx, 'ий', 'о')], ('adj', 'adv')],
    # ('живой', 'живо')
    [[(replsfx, 'ой', 'о')], ('adj', 'adv')],
    # ('певучий', 'певуче')
    [[(replsfx, 'ий', 'е')], ('adj', 'adv')],

    # 979
    # ('воровской', 'воровски')
    [[(replsfx, 'ой', 'и')], ('adj', 'adv')],
    # ('всяческий', 'всячески')
    [[(replsfx, 'ий', 'и')], ('adj', 'adv')],

    # 980
    # ('пеший', 'пешком'), ('тихий', 'тишком'),
    [[(replsfx, 'ий', 'ком')], ('adj', 'adv')],
    # ('тайный', 'тайком')
    [[(replsfx, 'ый', 'ком')], ('adj', 'adv')],
    # ('лихой', 'лишком') ?
    [[(replsfx, 'ой', 'ком')], ('adj', 'adv')],
    # ('босой', 'босиком')
    [[(replsfx, 'ой', 'иком')], ('adj', 'adv')],
    # ('целый', 'целиком')
    [[(replsfx, 'ый', 'иком')], ('adj', 'adv')],

    # 981

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ СУЩЕСТВИТЕЛЬНЫМИ

    # 982
    [[(addsfx, 'ом')], ('noun', 'adv')],
    [[(replsfx, 'о', 'ом')], ('noun', 'adv')],
    [[(replsfx, 'а', 'ом')], ('noun', 'adv')],

    [[(addsfx, 'ём')], ('noun', 'adv')],

    [[(replsfx, 'о', 'ём')], ('noun', 'adv')],
    [[(replsfx, 'а', 'ём')], ('noun', 'adv')],

    [[(addsfx, 'ем')], ('noun', 'adv')],

    [[(replsfx, 'а', 'ой')], ('noun', 'adv')],
    [[(replsfx, 'а', 'ою')], ('noun', 'adv')],

    [[(addsfx, 'ю')], ('noun', 'adv')],

    [[(replsfx, 'а', 'ами')], ('noun', 'adv')],
    [[(addsfx, 'ами')], ('noun', 'adv')],
    [[(addsfx, 'ями')], ('noun', 'adv')],

    # 983
    [[(addsfx, 'ю')], ('num', 'adv')],

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

    # 985
    # ('торчать', 'торчмя')
    [[(replsfx, 'ать', 'мя')], ('verb', 'adv')],
    # ('сидеть', 'сидмя')
    [[(replsfx, 'еть', 'мя')], ('verb', 'adv')],
    # ('стоять', 'стоймя')
    [[(replsfx, 'ять', 'мя')], ('verb', 'adv')],

    # 986
    # ('мелькать', 'мельком')
    [[(replsfx, 'ать', 'ом')], ('verb', 'adv')],
    #
    [[(replsfx, 'ить', 'ом')], ('verb', 'adv')],
    #
    [[(replsfx, 'аться', 'ом')], ('verb', 'adv')],
    #
    [[(replsfx, 'иться', 'ом')], ('verb', 'adv')],
    #
    [[(replsfx, 'ать', 'ком')], ('verb', 'adv')],
    #
    [[(replsfx, 'ить', 'ком')], ('verb', 'adv')],
    #
    [[(replsfx, 'ять', 'ком')], ('verb', 'adv')],


    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ НАРЕЧИЯМИ

    # 988
    # ('рано', 'рановато')
    [[(replsfx, 'о', 'овато')], ('adv', 'adv')],

    # 989
    # ('трудно', 'трудненько')
    [[(replsfx, 'о', 'енько')], ('adv', 'adv')],
    # ('тихо', 'тихонько')
    [[(replsfx, 'о', 'онько')], ('adv', 'adv')],

    # 990
    # ('близко', 'близёхонько')
    [[(replsfx, 'о', 'ёхонько')], ('adv', 'adv')],
    # ('тихо', 'тихохонько')
    [[(replsfx, 'о', 'охонько')], ('adv', 'adv')],
    # ('рано', 'ранёшенько')
    [[(replsfx, 'о', 'ёшенько')], ('adv', 'adv')],

    # 991
    # ('босиком', 'босичком')
    [[(replsfx, 'ом', 'ком')], ('adv', 'adv')],
    # ('стороной', 'сторонкой')
    [[(replsfx, 'ой', 'кой')], ('adv', 'adv')],
    # ('вразвалку', 'вразвалочку')
    [[(replsfx, 'у', 'ку')], ('adv', 'adv')],
    # ('хорошенько', 'хорошенечко')
    [[(replsfx, 'о', 'ко')], ('adv', 'adv')],

    ### ПРЕФИКСАЛЬНЫЕ НАРЕЧИЯ

    # 992

    [[(addpfx, 'не')], ('adv', 'adv')],

    #[[(addpfx, 'во')], ('adv', 'adv')],
    #[[(addpfx, 'за')], ('adv', 'adv')],
    #[[(addpfx, 'до')], ('adv', 'adv')],
    #[[(addpfx, 'на')], ('adv', 'adv')],
    #[[(addpfx, 'по')], ('adv', 'adv')],
    #[[(addpfx, 'от')], ('adv', 'adv')],
    #[[(addpfx, 'ото')], ('adv', 'adv')],
    #[[(addpfx, 'не')], ('adv', 'adv')],


    ### ПРЕФИКСАЛЬНО-СУФФИКСАЛЬНЫЕ НАРЕЧИЯ

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ ПРИЛАГАТЕЛЬНЫМИ

    # 994
    [[(replsfx, 'ой', 'ому'), (addpfx, 'по-')], ('adj', 'adv')],
    [[(replsfx, 'ый', 'ому'), (addpfx, 'по-')], ('adj', 'adv')],
    [[(replsfx, 'ий', 'ому'), (addpfx, 'по-')], ('adj', 'adv')],

    [[(replsfx, 'ий', 'ему'), (addpfx, 'по-')], ('adj', 'adv')],


    # 995
    [[(replsfx, 'ой', 'и'), (addpfx, 'по-')], ('adj', 'adv')],
    [[(replsfx, 'ий', 'и'), (addpfx, 'по-')], ('adj', 'adv')],

    # 996

    # 997

    # 998
    [[(replsfx, 'ый', 'ой'), (addpfx, 'по-')], ('adj', 'adv')],
    [[(replsfx, 'ий', 'ой'), (addpfx, 'по-')], ('adj', 'adv')],

    # 999
    [[(replsfx, 'ый', 'ую'), (addpfx, 'в')], ('adj', 'adv')],
    [[(replsfx, 'ий', 'ую'), (addpfx, 'в')], ('adj', 'adv')],
    [[(replsfx, 'ой', 'ую'), (addpfx, 'в')], ('adj', 'adv')],

    # 1000

    # 1001
    [[(replsfx, 'ый', 'а'), (addpfx, 'до')], ('adj', 'adv')],
    [[(replsfx, 'ий', 'а'), (addpfx, 'до')], ('adj', 'adv')],
    [[(replsfx, 'ой', 'а'), (addpfx, 'до')], ('adj', 'adv')],
    [[(replsfx, 'ий', 'я'), (addpfx, 'до')], ('adj', 'adv')],

    # 1002

    # 1003
    [[(replsfx, 'ый', 'а'), (addpfx, 'до')], ('adj', 'adv')],
    [[(replsfx, 'ий', 'а'), (addpfx, 'до')], ('adj', 'adv')],
    [[(replsfx, 'ой', 'а'), (addpfx, 'до')], ('adj', 'adv')],
    [[(replsfx, 'ий', 'я'), (addpfx, 'до')], ('adj', 'adv')],

    # 1004
    [[(replsfx, 'ый', 'о'), (addpfx, 'на')], ('adj', 'adv')],
    [[(replsfx, 'ой', 'о'), (addpfx, 'на')], ('adj', 'adv')],


    # 1005
    [[(replsfx, 'ый', 'е'), (addpfx, 'на')], ('adj', 'adv')],
    [[(replsfx, 'ий', 'е'), (addpfx, 'на')], ('adj', 'adv')],

    # 1006
    [[(replsfx, 'ый', 'а'), (addpfx, 'с')], ('adj', 'adv')],
    [[(replsfx, 'ий', 'а'), (addpfx, 'с')], ('adj', 'adv')],
    [[(replsfx, 'ой', 'а'), (addpfx, 'с')], ('adj', 'adv')],

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ СУЩЕСТВИТЕЛЬНЫМИ

    # 1009
    [[(addsfx, ''), (addpfx, 'в')], ('noun', 'adv')],
    [[(addsfx, ''), (addpfx, 'во')], ('noun', 'adv')],
    [[(replsfx, 'а', 'у'), (addpfx, 'в')], ('noun', 'adv')],
    [[(replsfx, 'а', 'у'), (addpfx, 'во')], ('noun', 'adv')],

    # 1010
    [[(addsfx, ''), (addpfx, 'на')], ('noun', 'adv')],
    [[(replsfx, 'а', 'у'), (addpfx, 'на')], ('noun', 'adv')],

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ ЧИСЛИТЕЛЬНЫМИ

    # 1017
    # ('двое', 'вдвоём')
    [[(replsfx, 'е', 'ём'), (addpfx, 'в')], ('num', 'adv')],
    # ('четверо', 'вчетвером')
    [[(replsfx, 'о', 'ом'), (addpfx, 'в')], ('num', 'adv')],
    # ('четверо', 'вчетверо')
    [[(replsfx, 'о', 'о'), (addpfx, 'в')], ('num', 'adv')],

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

    # 1020
    # ('переменять', 'напеременку')
    [[(replsfx, 'ять', 'ку'), (addpfx, 'в')], ('verb', 'adv')],
    [[(replsfx, 'ать', 'ку'), (addpfx, 'в')], ('verb', 'adv')],
    [[(replsfx, 'ить', 'ку'), (addpfx, 'в')], ('verb', 'adv')],
    [[(replsfx, 'ять', 'ку'), (addpfx, 'на')], ('verb', 'adv')],
    [[(replsfx, 'ать', 'ку'), (addpfx, 'на')], ('verb', 'adv')],
    [[(replsfx, 'ить', 'ку'), (addpfx, 'на')], ('verb', 'adv')],

    ## НАРЕЧИЯ, МОТИВИРОВАННЫЕ НАРЕЧИЯМИ

    # 1022
    # ('долго', 'подолгу')
    [[(replsfx, 'о', 'у'), (addpfx, 'по')], ('adv', 'adv')],

    # ПРЕФИКСАЛЬНЫЕ НАРЕЧИЯ С НУЛЕВЫМ СУФФИКСОМ

    # 1024
    [[(replsfx, 'ять', ''), (addpfx, 'в')], ('verb', 'adv')],
    [[(replsfx, 'ать', ''), (addpfx, 'в')], ('verb', 'adv')],
    [[(replsfx, 'ить', ''), (addpfx, 'в')], ('verb', 'adv')],
    [[(replsfx, 'ять', ''), (addpfx, 'на')], ('verb', 'adv')],
    [[(replsfx, 'ать', ''), (addpfx, 'на')], ('verb', 'adv')],
    [[(replsfx, 'ить', ''), (addpfx, 'на')], ('verb', 'adv')],



    [[(replsfx, 'ать', 'аючи')], ('verb', 'adv')],
]


adj_suffixes = [



]

rules_adj = [

    # 611
    [[(addsfx, 'ов')], ('noun', 'adj')],
    [[(addsfx, 'ев')], ('noun', 'adj')],
# 612
[[(replsfx, 'а', 'ин')], ('noun', 'adj')],
[[(replsfx, 'я', 'ин')], ('noun', 'adj')],

[[(replsfx, 'а', 'ын')], ('noun', 'adj')],

[[(addsfx, 'ий')], ('noun', 'adj')],
[[(replsfx, 'а', 'ий')], ('noun', 'adj')],


# 616

[[(replsfx, 'я', 'иный')], ('noun', 'adj')],
[[(addsfx, 'иный')], ('noun', 'adj')],


[[(replsfx, 'а', 'ный')], ('noun', 'adj')],
[[(replsfx, 'о', 'ный')], ('noun', 'adj')],
[[(replsfx, 'я', 'оный')], ('noun', 'adj')],
[[(addsfx, 'ный')], ('noun', 'adj')],

[[(replsfx, 'а', 'енный')], ('noun', 'adj')],
[[(replsfx, 'о', 'енный')], ('noun', 'adj')],
[[(addsfx, 'енный')], ('noun', 'adj')],

[[(replsfx, 'а', 'альный')], ('noun', 'adj')],
[[(addsfx, 'арный')], ('noun', 'adj')],

[[(replsfx, 'а', 'арный')], ('noun', 'adj')],
[[(replsfx, 'о', 'енный')], ('noun', 'adj')],
[[(addsfx, 'альный')], ('noun', 'adj')],

[[(replsfx, 'а', 'озный')], ('noun', 'adj')],
[[(replsfx, 'я', 'озный')], ('noun', 'adj')],
[[(addsfx, 'озный')], ('noun', 'adj')],


[[(replsfx, 'ия', 'орный')], ('noun', 'adj')],
[[(addsfx, 'орный')], ('noun', 'adj')],
[[(addsfx, 'уальный')], ('noun', 'adj')],
[[(addsfx, 'ивный')], ('noun', 'adj')],

[[(replsfx, 'ия', 'ичный')], ('noun', 'adj')],
[[(addsfx, 'ичный')], ('noun', 'adj')],
[[(addsfx, 'иальный')], ('noun', 'adj')],


[[(replsfx, 'я', 'онный')], ('noun', 'adj')],
[[(replsfx, 'я', 'ональный')], ('noun', 'adj')],

[[(replsfx, 'ние', 'тельный')], ('noun', 'adj')],
[[(replsfx, 'ение', 'ительный')], ('noun', 'adj')],

# 626
[[(addsfx, 'абельный')], ('noun', 'adj')],
[[(replsfx, 'а', 'абельный')], ('noun', 'adj')],
[[(replsfx, 'ация', 'абельный')], ('noun', 'adj')], # добавить -аци-

# 628
[[(addsfx, 'овой')], ('noun', 'adj')],
[[(addsfx, 'овый')], ('noun', 'adj')],
[[(addsfx, 'евый')], ('noun', 'adj')],
[[(replsfx, 'а', 'овый')], ('noun', 'adj')],
[[(replsfx, 'я', 'ёвый')], ('noun', 'adj')],

# 630
[[(addsfx, 'ский')], ('noun', 'adj')],
[[(replsfx, 'е', 'ской')], ('noun', 'adj')],
[[(replsfx, 'а', 'ский')], ('noun', 'adj')],
[[(addsfx, 'цкий')], ('noun', 'adj')],

[[(addsfx, 'еский')], ('noun', 'adj')],
[[(replsfx, 'а', 'еский')], ('noun', 'adj')],

[[(addsfx, 'овской')], ('noun', 'adj')],
[[(addsfx, 'овский')], ('noun', 'adj')],
[[(replsfx, 'ой', 'овский')], ('noun', 'adj')],
[[(replsfx, 'ий', 'овский')], ('noun', 'adj')],
[[(replsfx, 'а', 'овский')], ('noun', 'adj')],
[[(replsfx, 'ы', 'овский')], ('noun', 'adj')],

# 632
[[(replsfx, 'а', 'анский')], ('pnoun', 'adj')],
[[(replsfx, 'я', 'анский')], ('pnoun', 'adj')],

[[(replsfx, 'а', 'инский')], ('pnoun', 'adj')],
[[(replsfx, 'ы', 'инский')], ('pnoun', 'adj')],
[[(replsfx, 'ый', 'инский')], ('pnoun', 'adj')],
[[(replsfx, 'и', 'инский')], ('pnoun', 'adj')],
[[(replsfx, 'е', 'енский')], ('pnoun', 'adj')],
[[(replsfx, 'о', 'енский')], ('pnoun', 'adj')],
[[(replsfx, 'ий', 'енский')], ('pnoun', 'adj')],

[[(replsfx, 'и', 'ийский')], ('pnoun', 'adj')],
[[(replsfx, 'ы', 'ийский')], ('pnoun', 'adj')],
[[(replsfx, 'а', 'ийский')], ('pnoun', 'adj')],

[[(addsfx, 'ический')], ('noun', 'adj')],
[[(replsfx, 'а', 'ический')], ('pnoun', 'adj')],

[[(addsfx, 'ческий')], ('noun', 'adj')],
[[(replsfx, 'ие', 'ческий')], ('noun', 'adj')],

# 636

[[(addsfx, 'ианский')], ('pnoun', 'adj')],
[[(addsfx, 'анский')], ('pnoun', 'adj')],
[[(addsfx, 'янский')], ('pnoun', 'adj')],

# 637
[[(addsfx, 'астый')], ('noun', 'adj')],
[[(replsfx, 'а', 'астый')], ('noun', 'adj')],
[[(replsfx, 'о', 'астый')], ('noun', 'adj')],
[[(replsfx, 'ы', 'астый')], ('noun', 'adj')],
[[(replsfx, 'и', 'астый')], ('noun', 'adj')],

# 638

[[(addsfx, 'атый')], ('noun', 'adj')],
[[(replsfx, 'а', 'атый')], ('noun', 'adj')],
[[(replsfx, 'о', 'атый')], ('noun', 'adj')],
[[(replsfx, 'ы', 'атый')], ('noun', 'adj')],
[[(replsfx, 'и', 'атый')], ('noun', 'adj')],

# 639
[[(addsfx, 'чатый')], ('noun', 'adj')],
[[(replsfx, 'а', 'чатый')], ('noun', 'adj')],
[[(replsfx, 'о', 'чатый')], ('noun', 'adj')],
    [[(replsfx, 'и', 'чатый')], ('noun', 'adj')],

# 640
[[(addsfx, 'еватый')], ('noun', 'adj')],
[[(addsfx, 'оватый')], ('noun', 'adj')],
[[(replsfx, 'а', 'оватый')], ('noun', 'adj')],
[[(replsfx, 'о', 'оватый')], ('noun', 'adj')],
[[(replsfx, 'е', 'еватый')], ('noun', 'adj')],

# 641
[[(addsfx, 'овитый')], ('noun', 'adj')],
[[(addsfx, 'евитый')], ('noun', 'adj')],
[[(replsfx, 'а', 'овитый')], ('noun', 'adj')],

# 642
[[(addsfx, 'истый')], ('noun', 'adj')],
[[(replsfx, 'о', 'истый')], ('noun', 'adj')],
[[(replsfx, 'а', 'истый')], ('noun', 'adj')],
[[(replsfx, 'я', 'истый')], ('noun', 'adj')],

# 643
[[(addsfx, 'ливый')], ('noun', 'adj')],
[[(replsfx, 'а', 'ливый')], ('noun', 'adj')],
[[(replsfx, 'я', 'ливый')], ('noun', 'adj')],
[[(replsfx, 'а', 'ивый')], ('noun', 'adj')],

##    ПРИЛАГАТЕЛЬНЫЕ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

# 646


[[(replsfx, 'ать', 'ной')], ('verb', 'adj')],
[[(replsfx, 'еть', 'ной')], ('verb', 'adj')],
[[(replsfx, 'ить', 'ный')], ('verb', 'adj')],
[[(replsfx, 'ать', 'ный')], ('verb', 'adj')],
[[(replsfx, 'ять', 'ный')], ('verb', 'adj')],
[[(replsfx, 'еть', 'ный')], ('verb', 'adj')],
[[(replsfx, 'ить', 'ной')], ('verb', 'adj')],

[[(replsfx, 'овать', 'ный')], ('verb', 'adj')],
[[(replsfx, 'ывать', 'ной')], ('verb', 'adj')],
[[(replsfx, 'ивать', 'ной')], ('verb', 'adj')],
[[(replsfx, 'ивать', 'ный')], ('verb', 'adj')],

[[(replsfx, 'лять', 'ной')], ('verb', 'adj')],


[[(replsfx, 'иться', 'ный')], ('verb', 'adj')],
[[(replsfx, 'яться', 'ный')], ('verb', 'adj')],

# 651

[[(replsfx, 'ать', 'льный')], ('verb', 'adj')],
[[(replsfx, 'ить', 'льный')], ('verb', 'adj')],

[[(replsfx, 'аться', 'льный')], ('verb', 'adj')],
[[(replsfx, 'иться', 'льный')], ('verb', 'adj')],

# 652

[[(replsfx, 'ить', 'ительный')], ('verb', 'adj')],
[[(replsfx, 'ать', 'ательный')], ('verb', 'adj')],
[[(replsfx, 'еть', 'ительный')], ('verb', 'adj')],

[[(replsfx, 'аться', 'ительный')], ('verb', 'adj')],

# 653

[[(replsfx, 'ировать', 'абельный')], ('verb', 'adj')],

# 655

[[(replsfx, 'ать', 'ской')], ('verb', 'adj')],
[[(replsfx, 'аться', 'ательский')], ('verb', 'adj')],
[[(replsfx, 'ать', 'еский')], ('verb', 'adj')],

# 656

[[(replsfx, 'оть', 'кий')], ('verb', 'adj')],
[[(replsfx, 'ать', 'кий')], ('verb', 'adj')],
[[(replsfx, 'ить', 'кий')], ('verb', 'adj')],
[[(replsfx, 'еть', 'кий')], ('verb', 'adj')],

# 657

[[(replsfx, 'ять', 'яемый')], ('verb', 'adj')],
[[(replsfx, 'ать', 'аемый')], ('verb', 'adj')],
[[(replsfx, 'ить', 'имый')], ('verb', 'adj')],
[[(replsfx, 'еть', 'имый')], ('verb', 'adj')],

[[(replsfx, 'ать', 'уемый')], ('verb', 'adj')],
[[(replsfx, 'овать', 'уемый')], ('verb', 'adj')],

[[(replsfx, 'нуть', 'имый')], ('verb', 'adj')],

# 658

[[(replsfx, 'ать', 'истый')], ('verb', 'adj')],
[[(replsfx, 'ить', 'истый')], ('verb', 'adj')],
[[(replsfx, 'ываться', 'истый')], ('verb', 'adj')],
[[(replsfx, 'иваться', 'истый')], ('verb', 'adj')],
[[(replsfx, 'нуться', 'истый')], ('verb', 'adj')],

# 659

[[(replsfx, 'ать', 'чатый')], ('verb', 'adj')],
[[(replsfx, 'ить', 'истый')], ('verb', 'adj')],
[[(replsfx, 'ываться', 'истый')], ('verb', 'adj')],
[[(replsfx, 'иваться', 'истый')], ('verb', 'adj')],
[[(replsfx, 'нуться', 'истый')], ('verb', 'adj')],

# 660
[[(replsfx, 'ать', 'учий')], ('verb', 'adj')],
[[(replsfx, 'еть', 'учий')], ('verb', 'adj')],
[[(replsfx, 'ять', 'учий')], ('verb', 'adj')],
[[(replsfx, 'ить', 'ячий')], ('verb', 'adj')],
[[(replsfx, 'еть', 'ячий')], ('verb', 'adj')],
[[(replsfx, 'ать', 'ачий')], ('verb', 'adj')],
[[(replsfx, 'ять', 'ячий')], ('verb', 'adj')],

# 661
[[(replsfx, 'ить', 'ливый')], ('verb', 'adj')],
[[(replsfx, 'ать', 'ливый')], ('verb', 'adj')],
[[(replsfx, 'ить', 'чивый')], ('verb', 'adj')],
[[(replsfx, 'еть', 'чивый')], ('verb', 'adj')],
[[(replsfx, 'ить', 'ивый')], ('verb', 'adj')],

# 662
[[(replsfx, 'ть', 'лый')], ('verb', 'adj')],
[[(replsfx, 'ть', 'лой')], ('verb', 'adj')],

# 663
[[(replsfx, 'ить', 'ёный')], ('verb', 'adj')],
[[(replsfx, 'ить', 'еный')], ('verb', 'adj')],
[[(replsfx, 'ать', 'аный')], ('verb', 'adj')],
[[(replsfx, 'ать', 'анный')], ('verb', 'adj')],
[[(replsfx, 'уть', 'утый')], ('verb', 'adj')],
[[(replsfx, 'ить', 'итый')], ('verb', 'adj')],
[[(replsfx, 'оть', 'отый')], ('verb', 'adj')],
[[(replsfx, 'еть', 'тый')], ('verb', 'adj')],
[[(replsfx, 'ять', 'ятый')], ('verb', 'adj')],

# 664

[[(replsfx, 'аться', 'анный')], ('verb', 'adj')],
[[(replsfx, 'иться', 'енный')], ('verb', 'adj')],
[[(replsfx, 'иться', 'ённый')], ('verb', 'adj')],
[[(replsfx, 'уться', 'ённый')], ('verb', 'adj')],
[[(replsfx, 'яться', 'янный')], ('verb', 'adj')],

## ПРИЛАГАТЕЛЬНЫЕ, МОТИВИРОВАННЫЕ ПРИЛАГАТЕЛЬНЫМИ

# 666

[[(replsfx, 'ый', 'оватый')], ('adj', 'adj')],
[[(replsfx, 'овый', 'оватый')], ('adj', 'adj')],  # включить в предыдущий
[[(replsfx, 'евый', 'еватый')], ('adj', 'adj')],  # включить в предыдущий


# 667

[[(replsfx, 'ый', 'енький')], ('adj', 'adj')],
[[(replsfx, 'ой', 'енький')], ('adj', 'adj')],
[[(replsfx, 'ий', 'енький')], ('adj', 'adj')],
[[(replsfx, 'ий', 'онький')], ('adj', 'adj')],


# 669

[[(replsfx, 'ый', 'ёхонький')], ('adj', 'adj')],
[[(replsfx, 'ый', 'ёшенький')], ('adj', 'adj')],
[[(replsfx, 'ой', 'ёшенький')], ('adj', 'adj')],

# 671

[[(replsfx, 'ой', 'ущий')], ('adj', 'adj')],
[[(replsfx, 'ой', 'ющий')], ('adj', 'adj')],
[[(replsfx, 'ый', 'ущий')], ('adj', 'adj')],
[[(replsfx, 'ый', 'ющий')], ('adj', 'adj')],

# 673

[[(replsfx, 'ый', 'истый')], ('adj', 'adj')],
[[(replsfx, 'ой', 'истый')], ('adj', 'adj')],

### ПРЕФИКСАЛЬНЫЕ ПРИЛАГАТЕЛЬНЫЕ

# 683

[[(addpfx, 'а')], ('adj', 'adj')],

# 684

[[(addpfx, 'анти')], ('adj', 'adj')],

# 685

[[(addpfx, 'архи')], ('adj', 'adj')],

# 686

[[(addpfx, 'без')], ('adj', 'adj')],
[[(addpfx, 'бес')], ('adj', 'adj')],

# 687

[[(addpfx, 'вне')], ('adj', 'adj')],

# 688

[[(addpfx, 'внутри')], ('adj', 'adj')],

# 689

[[(addpfx, 'гипер')], ('adj', 'adj')],

# 690

[[(addpfx, 'до')], ('adj', 'adj')],

# 691

[[(addpfx, 'за')], ('adj', 'adj')],

# 693

[[(addpfx, 'интер')], ('adj', 'adj')],

# 694

[[(addpfx, 'меж')], ('adj', 'adj')],
[[(addpfx, 'между')], ('adj', 'adj')],

# 695

[[(addpfx, 'над')], ('adj', 'adj')],

# 696

[[(addpfx, 'наи')], ('adj', 'adj')],

# 697

[[(addpfx, 'не')], ('adj', 'adj')],

# 698

[[(addpfx, 'небез')], ('adj', 'adj')],
[[(addpfx, 'небес')], ('adj', 'adj')],

# 699

[[(addpfx, 'около')], ('adj', 'adj')],

# 700

[[(addpfx, 'пере')], ('adj', 'adj')],

# 701

[[(addpfx, 'по')], ('adj', 'adj')],

# 703

[[(addpfx, 'под')], ('adj', 'adj')],

# 704

[[(addpfx, 'после')], ('adj', 'adj')],

# 705

[[(addpfx, 'пост')], ('adj', 'adj')],

# 706

[[(addpfx, 'пре')], ('adj', 'adj')],

# 707

[[(addpfx, 'пред')], ('adj', 'adj')],

# 708

[[(addpfx, 'при')], ('adj', 'adj')],

# 709

[[(addpfx, 'про')], ('adj', 'adj')],

# 710

[[(addpfx, 'противо')], ('adj', 'adj')],

# 711

[[(addpfx, 'раз')], ('adj', 'adj')],
[[(addpfx, 'рас')], ('adj', 'adj')],

# 712

[[(addpfx, 'сверх')], ('adj', 'adj')],

# 714

[[(addpfx, 'суб')], ('adj', 'adj')],

# 715

[[(addpfx, 'супер')], ('adj', 'adj')],

# 716

[[(addpfx, 'транс')], ('adj', 'adj')],

# 717

[[(addpfx, 'ультра')], ('adj', 'adj')],

# 718

[[(addpfx, 'экстра')], ('adj', 'adj')],

###  ПРЕФИКСАЛЬНО-СУФФИКСАЛЬНЫЕ ПРИЛАГАТЕЛЬНЫЕ

## ПРИЛАГАТЕЛЬНЫЕ, МОТИВИРОВАННЫЕ СУЩЕСТВИТЕЛЬНЫМИ

# 723

[[(replsfx, 'а', 'ный'), (addpfx, 'без')], ('noun', 'adj')],
[[(replsfx, 'я', 'ный'), (addpfx, 'без')], ('noun', 'adj')],
[[(replsfx, 'и', 'ный'), (addpfx, 'без')], ('noun', 'adj')],
[[(addsfx, 'ный'), (addpfx, 'без')], ('noun', 'adj')],
[[(addsfx, 'ный'), (addpfx, 'бес')], ('noun', 'adj')],
[[(addsfx, 'ний'), (addpfx, 'без')], ('noun', 'adj')],
[[(addsfx, 'ний'), (addpfx, 'бес')], ('noun', 'adj')],

# 724

[[(replsfx, 'о', 'ный'), (addpfx, 'вне')], ('noun', 'adj')],
[[(replsfx, 'о', 'ный'), (addpfx, 'внутри')], ('noun', 'adj')],
[[(addsfx, 'ный'), (addpfx, 'вне')], ('noun', 'adj')],
[[(addsfx, 'ный'), (addpfx, 'внутри')], ('noun', 'adj')],
[[(replsfx, 'а', 'ный'), (addpfx, 'вне')], ('noun', 'adj')],
[[(replsfx, 'а', 'ный'), (addpfx, 'внутри')], ('noun', 'adj')],

# 725

[[(replsfx, 'е', 'ный'), (addpfx, 'за')], ('noun', 'adj')],
[[(replsfx, 'а', 'ный'), (addpfx, 'за')], ('noun', 'adj')],
[[(replsfx, 'о', 'ный'), (addpfx, 'за')], ('noun', 'adj')],
[[(addsfx, 'ный'), (addpfx, 'за')], ('noun', 'adj')],

# 726
[[(addsfx, 'ённый'), (addpfx, 'за')], ('noun', 'adj')],
[[(addsfx, 'енный'), (addpfx, 'за')], ('noun', 'adj')],
[[(addsfx, 'ованный'), (addpfx, 'за')], ('noun', 'adj')],

# 727
[[(addsfx, 'ный'), (addpfx, 'меж')], ('noun', 'adj')],
[[(addsfx, 'ный'), (addpfx, 'между')], ('noun', 'adj')],
[[(addsfx, 'ский'), (addpfx, 'меж')], ('noun', 'adj')],
[[(addsfx, 'ской'), (addpfx, 'меж')], ('noun', 'adj')],

[[(replsfx, 'а', 'ный'), (addpfx, 'меж')], ('noun', 'adj')],

# 728
[[(replsfx, 'а', 'ный'), (addpfx, 'на')], ('noun', 'adj')],
[[(replsfx, 'о', 'ный'), (addpfx, 'на')], ('noun', 'adj')],
[[(addsfx, 'ный'), (addpfx, 'на')], ('noun', 'adj')],
[[(replsfx, 'а', 'енный'), (addpfx, 'на')], ('noun', 'adj')],
[[(replsfx, 'о', 'енный'), (addpfx, 'на')], ('noun', 'adj')],

# 729
[[(addsfx, 'ный'), (addpfx, 'над')], ('noun', 'adj')],

# 731
[[(addsfx, 'ный'), (addpfx, 'около')], ('noun', 'adj')],
[[(replsfx, 'о', 'ный'), (addpfx, 'около')], ('noun', 'adj')],
[[(replsfx, 'а', 'ный'), (addpfx, 'около')], ('noun', 'adj')],

# 732

[[(addsfx, 'ный'), (addpfx, 'от')], ('noun', 'adj')],
[[(replsfx, 'я', 'енный'), (addpfx, 'от')], ('noun', 'adj')],

# 733

[[(addsfx, 'ный'), (addpfx, 'по')], ('noun', 'adj')],
[[(replsfx, 'а', 'ный'), (addpfx, 'по')], ('noun', 'adj')],
[[(replsfx, 'е', 'ский'), (addpfx, 'по')], ('noun', 'adj')],
[[(replsfx, 'а', 'ский'), (addpfx, 'по')], ('noun', 'adj')],

# 734

[[(addsfx, 'ный'), (addpfx, 'под')], ('noun', 'adj')],
[[(replsfx, 'а', 'ный'), (addpfx, 'под')], ('noun', 'adj')],
[[(replsfx, 'о', 'ный'), (addpfx, 'под')], ('noun', 'adj')],

# 735

[[(addsfx, 'ный'), (addpfx, 'пред')], ('noun', 'adj')],
[[(replsfx, 'а', 'ный'), (addpfx, 'пред')], ('noun', 'adj')],

# 736

[[(addsfx, 'ный'), (addpfx, 'при')], ('noun', 'adj')],
[[(replsfx, 'а', 'ный'), (addpfx, 'при')], ('noun', 'adj')],
[[(addsfx, 'ский'), (addpfx, 'при')], ('noun', 'adj')],
[[(replsfx, 'е', 'ский'), (addpfx, 'при')], ('noun', 'adj')],
[[(replsfx, 'а', 'ский'), (addpfx, 'при')], ('noun', 'adj')],

## ПРИЛАГАТЕЛЬНЫЕ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

# 743

[[(replsfx, 'ить', 'ный'), (addpfx, 'без')], ('verb', 'adj')],
[[(replsfx, 'ать', 'ный'), (addpfx, 'без')], ('verb', 'adj')],
[[(replsfx, 'ять', 'ный'), (addpfx, 'без')], ('verb', 'adj')],

[[(replsfx, 'ывать', 'ный'), (addpfx, 'без')], ('verb', 'adj')],

# 744

[[(replsfx, 'ить', 'ный'), (addpfx, 'не')], ('verb', 'adj')],
[[(replsfx, 'ать', 'ный'), (addpfx, 'не')], ('verb', 'adj')],
[[(replsfx, 'еть', 'ный'), (addpfx, 'не')], ('verb', 'adj')],

# 745

[[(replsfx, 'ить', 'имый'), (addpfx, 'не')], ('verb', 'adj')],
[[(replsfx, 'ать', 'аемый'), (addpfx, 'не')], ('verb', 'adj')],
[[(replsfx, 'еть', 'емый'), (addpfx, 'не')], ('verb', 'adj')],
[[(replsfx, 'овать', 'уемый'), (addpfx, 'не')], ('verb', 'adj')],

## ПРИЛАГАТЕЛЬНЫЕ МОТИВИРОВАННЫЕ ПРИЛАГАТЕЛЬНЫМИ И НАРЕЧИЯМИ

# 750

[[(replsfx, 'ый', 'енный'), (addpfx, 'за')], ('adj', 'adj')],
[[(replsfx, 'ой', 'енный'), (addpfx, 'за')], ('adj', 'adj')],
[[(replsfx, 'ой', 'ённый'), (addpfx, 'за')], ('adj', 'adj')],
[[(replsfx, 'ый', 'ённый'), (addpfx, 'за')], ('adj', 'adj')],

# ПРЕФИКСАЛЬНЫЕ ПРИЛАГАТЕЛЬНЫЕ С НУЛЕВЫМ СУФФИКСОМ

# 752

[[(addsfx, 'ый'), (addpfx, 'без')], ('noun', 'adj')],
[[(replsfx, 'а', 'ый'), (addpfx, 'без')], ('noun', 'adj')],
[[(replsfx, 'о', 'ый'), (addpfx, 'без')], ('noun', 'adj')],

]



rules_verb = [
    #795
    [[(addsfx, 'ить')], ('noun', 'verb')],
[[(replsfx, 'а', 'ить')], ('noun', 'verb')],
[[(replsfx, 'о', 'ить')], ('noun', 'verb')],

    # 796
[[(replsfx, 'ый', 'ить')], ('adj', 'verb')],
[[(replsfx, 'ий', 'ить')], ('adj', 'verb')],
[[(replsfx, 'ой', 'ить')], ('adj', 'verb')],

# 804
    [[(addsfx, 'овать')], ('noun', 'verb')],
[[(replsfx, 'а', 'овать')], ('noun', 'verb')],
[[(replsfx, 'е', 'евать')], ('noun', 'verb')],

    [[(addsfx, 'изировать')], ('noun', 'verb')],
[[(addsfx, 'ировать')], ('noun', 'verb')],
[[(replsfx, 'а', 'ировать')], ('noun', 'verb')],
[[(replsfx, 'ия', 'ировать')], ('noun', 'verb')],
[[(replsfx, 'е', 'ировать')], ('noun', 'verb')],

# 812
    [[(addsfx, 'ничать')], ('noun', 'verb')],
[[(replsfx, 'ник', 'ничать')], ('noun', 'verb')],  # убрать

# 814
[[(replsfx, 'ный', 'ничать')], ('noun', 'verb')],
[[(replsfx, 'ной', 'ничать')], ('noun', 'verb')],

# 818
[[(addsfx, 'ствовать')], ('noun', 'verb')],
[[(replsfx, 'а', 'ствовать')], ('noun', 'verb')],
[[(replsfx, 'о', 'ствовать')], ('noun', 'verb')],


[[(addsfx, 'ествовать')], ('noun', 'verb')],
[[(replsfx, 'ие', 'ествовать')], ('noun', 'verb')],

# 820
[[(replsfx, 'ый', 'ствовать')], ('adj', 'verb')],

# 829
[[(replsfx, 'ый', 'еть')], ('adj', 'verb')],
[[(replsfx, 'ой', 'еть')], ('adj', 'verb')],
[[(replsfx, 'ий', 'еть')], ('adj', 'verb')],

## ГЛАГОЛЫ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

# 836
[[(replsfx, 'ать', 'нуть')], ('verb', 'verb')],
[[(replsfx, 'ить', 'нуть')], ('verb', 'verb')],
[[(replsfx, 'овать', 'нуть')], ('verb', 'verb')],
[[(replsfx, 'ять', 'нуть')], ('verb', 'verb')],
[[(replsfx, 'оть', 'нуть')], ('verb', 'verb')],
[[(replsfx, 'аться', 'нуться')], ('verb', 'verb')],
[[(replsfx, 'еть', 'нуть')], ('verb', 'verb')],
[[(replsfx, 'ть', 'нуть')], ('verb', 'verb')],

# 842
[[(replsfx, 'ать', 'ывать')], ('verb', 'verb')],
[[(replsfx, 'оть', 'ывать')], ('verb', 'verb')],
[[(replsfx, 'ить', 'ивать')], ('verb', 'verb')],
[[(replsfx, 'ить', 'евать')], ('verb', 'verb')],
[[(replsfx, 'ять', 'евать')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ивать')], ('verb', 'verb')],
[[(replsfx, 'еть', 'ивать')], ('verb', 'verb')],
[[(replsfx, 'еться', 'ываться')], ('verb', 'verb')],
[[(replsfx, 'иться', 'иваться')], ('verb', 'verb')],


[[(replsfx, 'еть', 'ать')], ('verb', 'verb')],
[[(replsfx, 'ить', 'ать')], ('verb', 'verb')],
[[(replsfx, 'ить', 'ять')], ('verb', 'verb')],
[[(replsfx, 'ть', 'вать')], ('verb', 'verb')],
[[(replsfx, 'нуть', 'ывать')], ('verb', 'verb')],
[[(replsfx, 'нуть', 'ать')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ывать')], ('verb', 'verb')],
[[(replsfx, 'нуться', 'ать')], ('verb', 'verb')],
[[(replsfx, 'нуться', 'аться')], ('verb', 'verb')],

## ПРЕФИКСАЛЬНЫЕ ГЛАГОЛЫ

# 854
[[(addpfx, 'в')], ('verb', 'verb')],
[[(addpfx, 'во')], ('verb', 'verb')],

# 855
[[(addpfx, 'вз')], ('verb', 'verb')],
[[(addpfx, 'взо')], ('verb', 'verb')],

# 856
[[(addpfx, 'воз')], ('verb', 'verb')],
[[(addpfx, 'возо')], ('verb', 'verb')],

# 857
[[(addpfx, 'вы')], ('verb', 'verb')],

# 858
[[(addpfx, 'де')], ('verb', 'verb')],
[[(addpfx, 'дез')], ('verb', 'verb')],

# 859
[[(addpfx, 'дис')], ('verb', 'verb')],

# 860
[[(addpfx, 'до')], ('verb', 'verb')],

# 861
[[(addpfx, 'за')], ('verb', 'verb')],

# 862
[[(addpfx, 'из')], ('verb', 'verb')],
[[(addpfx, 'изо')], ('verb', 'verb')],

# 863
[[(addpfx, 'на')], ('verb', 'verb')],

# 864
[[(addpfx, 'над')], ('verb', 'verb')],
[[(addpfx, 'надо')], ('verb', 'verb')],

# 865
[[(addpfx, 'недо')], ('verb', 'verb')],

# 867
[[(addpfx, 'о')], ('verb', 'verb')],

# 868
[[(addpfx, 'об')], ('verb', 'verb')],
[[(addpfx, 'обо')], ('verb', 'verb')],

# 869
[[(addpfx, 'от')], ('verb', 'verb')],
[[(addpfx, 'ото')], ('verb', 'verb')],

# 870
[[(addpfx, 'пере')], ('verb', 'verb')],

# 871
[[(addpfx, 'по')], ('verb', 'verb')],

# 872
[[(addpfx, 'под')], ('verb', 'verb')],
[[(addpfx, 'подо')], ('verb', 'verb')],

# 873
[[(addpfx, 'пре')], ('verb', 'verb')],

# 874
[[(addpfx, 'пред')], ('verb', 'verb')],
[[(addpfx, 'предо')], ('verb', 'verb')],

# 875
[[(addpfx, 'при')], ('verb', 'verb')],

# 876
[[(addpfx, 'про')], ('verb', 'verb')],

# 877
[[(addpfx, 'раз')], ('verb', 'verb')],
[[(addpfx, 'разо')], ('verb', 'verb')],

# 878
[[(addpfx, 'ре')], ('verb', 'verb')],

# 879
[[(addpfx, 'с')], ('verb', 'verb')],
[[(addpfx, 'со')], ('verb', 'verb')],

# 880
[[(addpfx, 'со')], ('verb', 'verb')],

# 881
[[(addpfx, 'у')], ('verb', 'verb')],

## ПРЕФИКСАЛЬНО-СУФФИКСАЛЬНЫЕ ГЛАГОЛЫ

### ГЛАГОЛЫ, МОТИВИРОВАННЫЕ ИМЕНАМИ

# 888
[[(addsfx, 'ить'), (addpfx, 'за')], ('noun', 'verb')],

[[(replsfx, 'а', 'ить'), (addpfx, 'за')], ('noun', 'verb')],
[[(replsfx, 'о', 'ить'), (addpfx, 'за')], ('noun', 'verb')],

[[(replsfx, 'ый', 'ить'), (addpfx, 'за')], ('adj', 'verb')],

# 889
[[(addsfx, 'ить'), (addpfx, 'из')], ('noun', 'verb')],
[[(addsfx, 'ить'), (addpfx, 'ис')], ('noun', 'verb')],

[[(replsfx, 'ый', 'ить'), (addpfx, 'из')], ('adj', 'verb')],
[[(replsfx, 'ый', 'ить'), (addpfx, 'ис')], ('adj', 'verb')],

# 891
[[(addsfx, 'ить'), (addpfx, 'о')], ('noun', 'verb')],
[[(replsfx, 'а', 'ить'), (addpfx, 'о')], ('noun', 'verb')],
[[(replsfx, 'е', 'ить'), (addpfx, 'о')], ('noun', 'verb')],


[[(replsfx, 'ый', 'ить'), (addpfx, 'о')], ('adj', 'verb')],
[[(replsfx, 'ий', 'ить'), (addpfx, 'о')], ('adj', 'verb')],

# 892
[[(addsfx, 'ить'), (addpfx, 'об')], ('noun', 'verb')],
[[(replsfx, 'а', 'ить'), (addpfx, 'об')], ('noun', 'verb')],

[[(replsfx, 'ий', 'ить'), (addpfx, 'об')], ('adj', 'verb')],
[[(replsfx, 'ый', 'ить'), (addpfx, 'об')], ('adj', 'verb')],

# 893
[[(addsfx, 'ить'), (addpfx, 'обез')], ('noun', 'verb')],
[[(replsfx, 'а', 'ить'), (addpfx, 'обез')], ('noun', 'verb')],
[[(replsfx, 'о', 'ить'), (addpfx, 'обез')], ('noun', 'verb')],

[[(addsfx, 'ить'), (addpfx, 'обес')], ('noun', 'verb')],
[[(replsfx, 'а', 'ить'), (addpfx, 'обес')], ('noun', 'verb')],
[[(replsfx, 'о', 'ить'), (addpfx, 'обес')], ('noun', 'verb')],

# 895
[[(replsfx, 'ый', 'ить'), (addpfx, 'пере')], ('adj', 'verb')],

# 897
[[(addsfx, 'ить'), (addpfx, 'под')], ('noun', 'verb')],

[[(replsfx, 'ий', 'ить'), (addpfx, 'под')], ('adj', 'verb')],
[[(replsfx, 'ый', 'ить'), (addpfx, 'под')], ('adj', 'verb')],

# 898
[[(addsfx, 'ить'), (addpfx, 'при')], ('noun', 'verb')],
[[(replsfx, 'а', 'ить'), (addpfx, 'при')], ('noun', 'verb')],
[[(replsfx, 'я', 'ить'), (addpfx, 'при')], ('noun', 'verb')],

# 899
[[(addsfx, 'ить'), (addpfx, 'про')], ('noun', 'verb')],
[[(replsfx, 'а', 'ить'), (addpfx, 'про')], ('noun', 'verb')],

[[(replsfx, 'ый', 'ить'), (addpfx, 'про')], ('adj', 'verb')],

# 900
[[(addsfx, 'ить'), (addpfx, 'рас')], ('noun', 'verb')],
[[(replsfx, 'о', 'ить'), (addpfx, 'рас')], ('noun', 'verb')],
[[(replsfx, 'а', 'ить'), (addpfx, 'рас')], ('noun', 'verb')],

[[(addsfx, 'ить'), (addpfx, 'раз')], ('noun', 'verb')],
[[(replsfx, 'а', 'ить'), (addpfx, 'раз')], ('noun', 'verb')],
[[(replsfx, 'о', 'ить'), (addpfx, 'раз')], ('noun', 'verb')],


[[(replsfx, 'ый', 'ить'), (addpfx, 'раз')], ('adj', 'verb')],
[[(replsfx, 'ый', 'ить'), (addpfx, 'рас')], ('adj', 'verb')],
[[(replsfx, 'ий', 'ить'), (addpfx, 'раз')], ('adj', 'verb')],
[[(replsfx, 'ий', 'ить'), (addpfx, 'рас')], ('adj', 'verb')],

# 901
[[(replsfx, 'а', 'ить'), (addpfx, 'с')], ('noun', 'verb')],

[[(replsfx, 'ий', 'ить'), (addpfx, 'с')], ('adj', 'verb')],

# 902
[[(replsfx, 'ый', 'ить'), (addpfx, 'у')], ('adj', 'verb')],
[[(replsfx, 'ий', 'ить'), (addpfx, 'у')], ('adj', 'verb')],

# 904
[[(addsfx, 'еть'), (addpfx, 'за')], ('noun', 'verb')],
[[(replsfx, 'а', 'еть'), (addpfx, 'за')], ('noun', 'verb')],


[[(replsfx, 'ый', 'еть'), (addpfx, 'за')], ('adj', 'verb')],
[[(replsfx, 'ий', 'еть'), (addpfx, 'за')], ('adj', 'verb')],
[[(replsfx, 'ой', 'еть'), (addpfx, 'за')], ('adj', 'verb')],

# 905
[[(replsfx, 'ый', 'еть'), (addpfx, 'о')], ('adj', 'verb')],
[[(replsfx, 'ий', 'еть'), (addpfx, 'о')], ('adj', 'verb')],
[[(replsfx, 'ой', 'еть'), (addpfx, 'о')], ('adj', 'verb')],

# 906
[[(addsfx, 'еть'), (addpfx, 'обо')], ('noun', 'verb')],
[[(replsfx, 'а', 'еть'), (addpfx, 'обо')], ('noun', 'verb')],
[[(addsfx, 'еть'), (addpfx, 'об')], ('noun', 'verb')],
[[(replsfx, 'а', 'еть'), (addpfx, 'об')], ('noun', 'verb')],

# 907
[[(addsfx, 'еть'), (addpfx, 'обес')], ('noun', 'verb')],
[[(addsfx, 'еть'), (addpfx, 'обез')], ('noun', 'verb')],
[[(replsfx, 'а', 'еть'), (addpfx, 'обез')], ('noun', 'verb')],
[[(replsfx, 'а', 'еть'), (addpfx, 'обес')], ('noun', 'verb')],
[[(replsfx, 'о', 'еть'), (addpfx, 'обез')], ('noun', 'verb')],
[[(replsfx, 'о', 'еть'), (addpfx, 'обес')], ('noun', 'verb')],


# 908
[[(replsfx, 'ый', 'еть'), (addpfx, 'по')], ('adj', 'verb')],
[[(replsfx, 'ий', 'еть'), (addpfx, 'по')], ('adj', 'verb')],
[[(replsfx, 'ой', 'еть'), (addpfx, 'по')], ('adj', 'verb')],

### ГЛАГОЛЫ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

# 914
[[(replsfx, 'ать', 'ывать'), (addpfx, 'вы')], ('verb', 'verb')],
[[(replsfx, 'еть', 'ывать'), (addpfx, 'вы')], ('verb', 'verb')],
[[(replsfx, 'ить', 'ивать'), (addpfx, 'вы')], ('verb', 'verb')],

# 915
[[(replsfx, 'ать', 'ывать'), (addpfx, 'на')], ('verb', 'verb')],
[[(replsfx, 'еть', 'ывать'), (addpfx, 'на')], ('verb', 'verb')],
[[(replsfx, 'ить', 'ивать'), (addpfx, 'на')], ('verb', 'verb')],

# 916
[[(replsfx, 'ать', 'ывать'), (addpfx, 'от')], ('verb', 'verb')],
[[(replsfx, 'еть', 'ывать'), (addpfx, 'от')], ('verb', 'verb')],
[[(replsfx, 'ить', 'ивать'), (addpfx, 'от')], ('verb', 'verb')],

# 917
[[(replsfx, 'ать', 'ывать'), (addpfx, 'пере')], ('verb', 'verb')],
[[(replsfx, 'еть', 'ывать'), (addpfx, 'пере')], ('verb', 'verb')],
[[(replsfx, 'ить', 'ивать'), (addpfx, 'пере')], ('verb', 'verb')],

# 918
[[(replsfx, 'ать', 'ывать'), (addpfx, 'по')], ('verb', 'verb')],
[[(replsfx, 'еть', 'ывать'), (addpfx, 'по')], ('verb', 'verb')],
[[(replsfx, 'ить', 'ивать'), (addpfx, 'по')], ('verb', 'verb')],

# 919
[[(replsfx, 'ать', 'ывать'), (addpfx, 'под')], ('verb', 'verb')],
[[(replsfx, 'еть', 'ывать'), (addpfx, 'под')], ('verb', 'verb')],
[[(replsfx, 'ить', 'ивать'), (addpfx, 'под')], ('verb', 'verb')],
[[(replsfx, 'уть', 'увать'), (addpfx, 'под')], ('verb', 'verb')],

# 920
[[(replsfx, 'ать', 'ывать'), (addpfx, 'при')], ('verb', 'verb')],
[[(replsfx, 'еть', 'ывать'), (addpfx, 'при')], ('verb', 'verb')],
[[(replsfx, 'ить', 'ивать'), (addpfx, 'при')], ('verb', 'verb')],

# 921
[[(replsfx, 'ать', 'ывать'), (addpfx, 'раз')], ('verb', 'verb')],
[[(replsfx, 'еть', 'ывать'), (addpfx, 'раз')], ('verb', 'verb')],
[[(replsfx, 'ить', 'ивать'), (addpfx, 'раз')], ('verb', 'verb')],

# 923
[[(replsfx, 'ать', 'нуть'), (addpfx, 'вз')], ('verb', 'verb')],
[[(replsfx, 'еть', 'нуть'), (addpfx, 'вз')], ('verb', 'verb')],
[[(replsfx, 'ить', 'нуть'), (addpfx, 'вз')], ('verb', 'verb')],

[[(replsfx, 'ать', 'нуть'), (addpfx, 'вс')], ('verb', 'verb')],
[[(replsfx, 'еть', 'нуть'), (addpfx, 'вс')], ('verb', 'verb')],
[[(replsfx, 'ить', 'нуть'), (addpfx, 'вс')], ('verb', 'verb')],

# 924
[[(replsfx, 'ать', 'нуть'), (addpfx, 'при')], ('verb', 'verb')],
[[(replsfx, 'еть', 'нуть'), (addpfx, 'при')], ('verb', 'verb')],
[[(replsfx, 'ить', 'нуть'), (addpfx, 'при')], ('verb', 'verb')],

# 925
[[(replsfx, 'ать', 'нуть'), (addpfx, 'с')], ('verb', 'verb')],
[[(replsfx, 'еть', 'нуть'), (addpfx, 'с')], ('verb', 'verb')],
[[(replsfx, 'ить', 'нуть'), (addpfx, 'с')], ('verb', 'verb')],

# 927
[[(replsfx, 'ать', 'ить'), (addpfx, 'в')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'вы')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'вз')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'до')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'недо')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'об')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'от')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'пере')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'при')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'про')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'раз')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'за')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'на')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'по')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'под')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'с')], ('verb', 'verb')],
[[(replsfx, 'ать', 'ить'), (addpfx, 'у')], ('verb', 'verb')],

[[(replsfx, 'ять', 'ить'), (addpfx, 'в')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'вы')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'вз')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'до')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'недо')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'об')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'от')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'пере')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'при')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'про')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'раз')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'за')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'на')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'по')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'под')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'с')], ('verb', 'verb')],
[[(replsfx, 'ять', 'ить'), (addpfx, 'у')], ('verb', 'verb')],


## ПОСТФИКСАЛЬНЫЕ ГЛАГОЛЫ

# 930
[[(addsfx, 'ся')], ('verb', 'verb')],
[[(addsfx, 'сь')], ('verb', 'verb')],

## СУФФИКСАЛЬНО-ПОСТФИКСАЛЬНЫЕ ГЛАГОЛЫ

# 932
[[(addsfx, 'иться')], ('noun', 'verb')],
[[(replsfx, 'а', 'иться')], ('noun', 'verb')],

# 933
[[(replsfx, 'ый', 'иться')], ('adj', 'verb')],
[[(replsfx, 'ий', 'иться')], ('adj', 'verb')],
[[(replsfx, 'ой', 'иться')], ('adj', 'verb')],

## ПРЕФИКСАЛЬНО-ПОСТФИКСАЛЬНЫЕ ГЛАГОЛЫ

# 937
[[(addsfx, 'ся'), (addpfx, 'в')], ('verb', 'verb')],

# 938
[[(addsfx, 'ся'), (addpfx, 'вз')], ('verb', 'verb')],
[[(addsfx, 'ся'), (addpfx, 'взо')], ('verb', 'verb')],

# 939
[[(addsfx, 'ся'), (addpfx, 'вы')], ('verb', 'verb')],

# 940
[[(addsfx, 'ся'), (addpfx, 'до')], ('verb', 'verb')],

# 941
[[(addsfx, 'ся'), (addpfx, 'за')], ('verb', 'verb')],

# 942
[[(addsfx, 'ся'), (addpfx, 'из')], ('verb', 'verb')],
[[(addsfx, 'ся'), (addpfx, 'ис')], ('verb', 'verb')],

# 943
[[(addsfx, 'ся'), (addpfx, 'на')], ('verb', 'verb')],

# 944
[[(addsfx, 'ся'), (addpfx, 'о')], ('verb', 'verb')],

# 945
[[(addsfx, 'ся'), (addpfx, 'об')], ('verb', 'verb')],
[[(addsfx, 'ся'), (addpfx, 'обо')], ('verb', 'verb')],

# 946
[[(addsfx, 'ся'), (addpfx, 'от')], ('verb', 'verb')],
[[(addsfx, 'ся'), (addpfx, 'ото')], ('verb', 'verb')],


# 947
[[(addsfx, 'ся'), (addpfx, 'под')], ('verb', 'verb')],
[[(addsfx, 'ся'), (addpfx, 'подо')], ('verb', 'verb')],

# 948
[[(addsfx, 'ся'), (addpfx, 'при')], ('verb', 'verb')],

# 949
[[(addsfx, 'ся'), (addpfx, 'про')], ('verb', 'verb')],

# 950
[[(addsfx, 'ся'), (addpfx, 'раз')], ('verb', 'verb')],
[[(addsfx, 'ся'), (addpfx, 'разо')], ('verb', 'verb')],

# 951
[[(addsfx, 'ся'), (addpfx, 'с')], ('verb', 'verb')],
[[(addsfx, 'ся'), (addpfx, 'со')], ('verb', 'verb')],

# 952
[[(addsfx, 'ся'), (addpfx, 'у')], ('verb', 'verb')],


## ПРЕФИКСАЛЬНО-СУФФИКСАЛЬНО-ПОСТФИКСАЛЬНЫЕ ГЛАГОЛЫ

# 955
[[(addsfx, 'иться'), (addpfx, 'о')], ('noun', 'verb')],
[[(replsfx, 'а', 'иться'), (addpfx, 'о')], ('noun', 'verb')],

# 956
[[(addsfx, 'иться'), (addpfx, 'об')], ('noun', 'verb')],
[[(replsfx, 'а', 'иться'), (addpfx, 'об')], ('noun', 'verb')],

# 959
[[(replsfx, 'ить', 'иваться'), (addpfx, 'пере')], ('verb', 'verb')],
[[(replsfx, 'ать', 'иваться'), (addpfx, 'пере')], ('verb', 'verb')],
[[(replsfx, 'ять', 'иваться'), (addpfx, 'пере')], ('verb', 'verb')],
[[(replsfx, 'еть', 'ываться'), (addpfx, 'пере')], ('verb', 'verb')],


]





rules_noun = [

### СУФФИКСАЛЬНЫЕ СУЩЕСТВИТЕЛЬНЫЕ

## СУЩЕСТВИТЕЛЬНЫЕ, МОТИВИРОВАННЫЕ ГЛАГОЛАМИ

# 211
[[(replsfx, 'ть', 'тель')], ('verb', 'noun')],
[[(replsfx, 'еть', 'итель')], ('verb', 'noun')],

# 212
[[(replsfx, 'ать', 'ник')], ('verb', 'noun')],
[[(replsfx, 'аться', 'ник')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ник')], ('verb', 'noun')],

# 213
[[(replsfx, 'ать', 'чик')], ('verb', 'noun')],
[[(replsfx, 'ить', 'чик')], ('verb', 'noun')],
[[(replsfx, 'ять', 'щик')], ('verb', 'noun')],
[[(replsfx, 'ить', 'чик')], ('verb', 'noun')],
[[(replsfx, 'ать', 'щик')], ('verb', 'noun')],
[[(replsfx, 'еть', 'чик')], ('verb', 'noun')],
[[(replsfx, 'уть', 'щик')], ('verb', 'noun')],
[[(replsfx, 'ывать', 'чик')], ('verb', 'noun')],
[[(replsfx, 'ывать', 'щик')], ('verb', 'noun')],

# 214
[[(replsfx, 'ть', 'льщик')], ('verb', 'noun')],

# 215
[[(replsfx, 'ть', 'льник')], ('verb', 'noun')],

# 217
[[(replsfx, 'ть', 'лец')], ('verb', 'noun')],

# 219
[[(replsfx, 'ать', 'ач')], ('verb', 'noun')],
[[(replsfx, 'нуть', 'ач')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ач')], ('verb', 'noun')],

# 220
[[(replsfx, 'ать', 'ун')], ('verb', 'noun')],
[[(replsfx, 'еть', 'ун')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ун')], ('verb', 'noun')],

# 221
[[(replsfx, 'ать', 'ок')], ('verb', 'noun')],
[[(replsfx, 'еть', 'ок')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ок')], ('verb', 'noun')],
[[(replsfx, 'ять', 'ок')], ('verb', 'noun')],

# 224
[[(replsfx, 'ировать', 'ятор')], ('verb', 'noun')],
[[(replsfx, 'ировать', 'атор')], ('verb', 'noun')],
[[(replsfx, 'ировать', 'итор')], ('verb', 'noun')],
[[(replsfx, 'овать', 'атор')], ('verb', 'noun')],

# 225
[[(replsfx, 'ировать', 'ёр')], ('verb', 'noun')],
[[(replsfx, 'ировать', 'ор')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ёр')], ('verb', 'noun')],
[[(replsfx, 'ивать', 'ёр')], ('verb', 'noun')],
[[(replsfx, 'овать', 'ёр')], ('verb', 'noun')],
[[(replsfx, 'евать', 'ор')], ('verb', 'noun')],

# 226
[[(replsfx, 'ировать', 'янт')], ('verb', 'noun')],
[[(replsfx, 'ировать', 'ант')], ('verb', 'noun')],
[[(replsfx, 'ировать', 'ент')], ('verb', 'noun')],

# 227
[[(replsfx, 'ировать', 'ят')], ('verb', 'noun')],
[[(replsfx, 'ировать', 'ат')], ('verb', 'noun')],
[[(replsfx, 'овать', 'ат')], ('verb', 'noun')],

# 228
[[(replsfx, 'ть', 'лка')], ('verb', 'noun')],

# 229
[[(replsfx, 'ть', 'ловка')], ('verb', 'noun')],

# 230
[[(replsfx, 'ть', 'ла')], ('verb', 'noun')],
[[(replsfx, 'ть', 'ло')], ('verb', 'noun')],

# 231
[[(replsfx, 'еть', 'ка')], ('verb', 'noun')],
[[(replsfx, 'оть', 'ка')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ка')], ('verb', 'noun')],
[[(replsfx, 'ать', 'ка')], ('verb', 'noun')],
[[(replsfx, 'ять', 'ка')], ('verb', 'noun')],

[[(replsfx, 'ивать', 'ушка')], ('verb', 'noun')],
[[(replsfx, 'ать', 'ушка')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ушка')], ('verb', 'noun')],
[[(replsfx, 'ть', 'шка')], ('verb', 'noun')],

# 232
[[(replsfx, 'ить', 'яга')], ('verb', 'noun')],
[[(replsfx, 'ать', 'яга')], ('verb', 'noun')],

# 233
[[(replsfx, 'ять', 'яка')], ('verb', 'noun')],
[[(replsfx, 'ать', 'ака')], ('verb', 'noun')],

# 234
[[(replsfx, 'ать', 'уха')], ('verb', 'noun')],
[[(replsfx, 'ить', 'иха')], ('verb', 'noun')],

# 236
[[(replsfx, 'ать', 'уша')], ('verb', 'noun')],
[[(replsfx, 'ть', 'ша')], ('verb', 'noun')],

# 237
[[(replsfx, 'ать', 'уля')], ('verb', 'noun')],
[[(replsfx, 'ить', 'юля')], ('verb', 'noun')],

# 241
[[(replsfx, 'ть', 'льня')], ('verb', 'noun')],

# 247
[[(replsfx, 'ать', 'ыш')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ыш')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ыш')], ('verb', 'noun')],
[[(replsfx, 'ывать', 'ыш')], ('verb', 'noun')],

# 248
[[(replsfx, 'ать', 'ина')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ина')], ('verb', 'noun')],

# 249
[[(replsfx, 'оть', 'ево')], ('verb', 'noun')],
[[(replsfx, 'ить', 'иво')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ево')], ('verb', 'noun')],

# 250
[[(replsfx, 'ать', 'ки')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ки')], ('verb', 'noun')],

# 252
[[(replsfx, 'ить', 'имость')], ('verb', 'noun')],
[[(replsfx, 'ать', 'емость')], ('verb', 'noun')],
[[(replsfx, 'еть', 'имость')], ('verb', 'noun')],
[[(replsfx, 'ть', 'емость')], ('verb', 'noun')],

# 253
[[(replsfx, 'иться', 'ённость')], ('verb', 'noun')],
[[(replsfx, 'ться', 'тость')], ('verb', 'noun')],
[[(replsfx, 'ться', 'нность')], ('verb', 'noun')],

## Существительные со значением отвлеченного процессуального признака

# 256
[[(replsfx, 'ать', 'ание')], ('verb', 'noun')],
[[(replsfx, 'ать', 'ание')], ('verb', 'noun')],
[[(replsfx, 'аться', 'ание')], ('verb', 'noun')],
[[(replsfx, 'еть', 'ение')], ('verb', 'noun')],
[[(replsfx, 'ять', 'ение')], ('verb', 'noun')],
[[(replsfx, 'яться', 'ение')], ('verb', 'noun')],
[[(replsfx, 'ять', 'ение')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ение')], ('verb', 'noun')],
[[(replsfx, 'нуть', 'новение')], ('verb', 'noun')],
[[(replsfx, 'нуться', 'новение')], ('verb', 'noun')],
[[(replsfx, 'нуть', 'ение')], ('verb', 'noun')],
[[(replsfx, 'нуться', 'ение')], ('verb', 'noun')],
[[(replsfx, 'ать', 'ение')], ('verb', 'noun')],
[[(replsfx, 'аться', 'ение')], ('verb', 'noun')],
[[(replsfx, 'ыть', 'овение')], ('verb', 'noun')],
[[(replsfx, 'ыть', 'вение')], ('verb', 'noun')],
[[(replsfx, 'оться', 'ение')], ('verb', 'noun')],
[[(replsfx, 'иться', 'иение')], ('verb', 'noun')],
[[(replsfx, 'ить', 'иение')], ('verb', 'noun')],

# 259
[[(replsfx, 'уть', 'утие')], ('verb', 'noun')],
[[(replsfx, 'ить', 'итие')], ('verb', 'noun')],
[[(replsfx, 'ить', 'итьё')], ('verb', 'noun')],
[[(replsfx, 'ыть', 'ытие')], ('verb', 'noun')],
[[(replsfx, 'ать', 'атие')], ('verb', 'noun')],
[[(replsfx, 'ять', 'ятие')], ('verb', 'noun')],
[[(replsfx, 'ять', 'яние')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ание')], ('verb', 'noun')],

[[(replsfx, 'иться', 'ье')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ье')], ('verb', 'noun')],
[[(replsfx, 'ать', 'ье')], ('verb', 'noun')],
[[(replsfx, 'овать', 'ие')], ('verb', 'noun')],

# 262
[[(replsfx, 'ить', 'ка')], ('verb', 'noun')],
[[(replsfx, 'ать', 'ка')], ('verb', 'noun')],
[[(replsfx, 'еть', 'ка')], ('verb', 'noun')],
[[(replsfx, 'ить', 'овка')], ('verb', 'noun')],
[[(replsfx, 'ать', 'овка')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ёжка')], ('verb', 'noun')],

# 263
[[(replsfx, 'ировать', 'ация')], ('verb', 'noun')],
[[(replsfx, 'ировать', 'яция')], ('verb', 'noun')],
[[(replsfx, 'ировать', 'ция')], ('verb', 'noun')],
[[(replsfx, 'ировать', 'иция')], ('verb', 'noun')],
[[(replsfx, 'ировать', 'енция')], ('verb', 'noun')],
[[(replsfx, 'ировать', 'ия')], ('verb', 'noun')],

[[(replsfx, 'изовать', 'ация')], ('verb', 'noun')],
[[(replsfx, 'изовать', 'яция')], ('verb', 'noun')],
[[(replsfx, 'изовать', 'ция')], ('verb', 'noun')],
[[(replsfx, 'изовать', 'иция')], ('verb', 'noun')],
[[(replsfx, 'изовать', 'енция')], ('verb', 'noun')],
[[(replsfx, 'изовать', 'ия')], ('verb', 'noun')],

[[(replsfx, 'овать', 'ация')], ('verb', 'noun')],
[[(replsfx, 'овать', 'яция')], ('verb', 'noun')],
[[(replsfx, 'овать', 'ция')], ('verb', 'noun')],
[[(replsfx, 'овать', 'иция')], ('verb', 'noun')],
[[(replsfx, 'овать', 'енция')], ('verb', 'noun')],
[[(replsfx, 'овать', 'ия')], ('verb', 'noun')],

[[(replsfx, 'ить', 'ия')], ('verb', 'noun')],

# 264
[[(replsfx, 'ать', 'ество')], ('verb', 'noun')],
[[(replsfx, 'яться', 'ство')], ('verb', 'noun')],
[[(replsfx, 'ать', 'ство')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ство')], ('verb', 'noun')],
[[(replsfx, 'иться', 'ство')], ('verb', 'noun')],
[[(replsfx, 'аться', 'ательство')], ('verb', 'noun')],
[[(replsfx, 'ать', 'ательство')], ('verb', 'noun')],
[[(replsfx, 'иться', 'ительство')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ительство')], ('verb', 'noun')],

# 267
[[(replsfx, 'ать', 'ня')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ня')], ('verb', 'noun')],
[[(replsfx, 'ать', 'отня')], ('verb', 'noun')],

# 268
[[(replsfx, 'ировать', 'аж')], ('verb', 'noun')],

# 269
[[(replsfx, 'ить', 'ёж')], ('verb', 'noun')],
[[(replsfx, 'ать', 'ёж')], ('verb', 'noun')],
[[(replsfx, 'еть', 'ёж')], ('verb', 'noun')],

# 270
[[(replsfx, 'ать', 'ок')], ('verb', 'noun')],
[[(replsfx, 'еть', 'ок')], ('verb', 'noun')],
[[(replsfx, 'ить', 'ок')], ('verb', 'noun')],
[[(replsfx, 'нуть', 'ок')], ('verb', 'noun')],

# 272
[[(replsfx, 'ять', 'изм')], ('verb', 'noun')],
[[(replsfx, 'ать', 'изм')], ('verb', 'noun')],

# 278
[[(replsfx, 'ить', 'от')], ('verb', 'noun')],
[[(replsfx, 'ать', 'от')], ('verb', 'noun')],

# 279
[[(replsfx, 'ить', 'уха')], ('verb', 'noun')],
[[(replsfx, 'ать', 'уха')], ('verb', 'noun')],
[[(replsfx, 'иться', 'уха')], ('verb', 'noun')],

## СУЩЕСТВИТЕЛЬНЫЕ, МОТИВИРОВАННЫЕ ПРИЛАГАТЕЛЬНЫМИ

# 285
[[(replsfx, 'ый', 'ик')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ик')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ик')], ('adj', 'noun')],

[[(replsfx, 'ый', 'ник')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ник')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ник')], ('adj', 'noun')],

# 286
[[(replsfx, 'ый', 'чик')], ('adj', 'noun')],
[[(replsfx, 'ой', 'чик')], ('adj', 'noun')],
[[(replsfx, 'ий', 'чик')], ('adj', 'noun')],

[[(replsfx, 'ый', 'щик')], ('adj', 'noun')],
[[(replsfx, 'ой', 'щик')], ('adj', 'noun')],
[[(replsfx, 'ий', 'щик')], ('adj', 'noun')],

# 287
[[(replsfx, 'ый', 'ец')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ец')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ец')], ('adj', 'noun')],

[[(replsfx, 'ый', 'овец')], ('adj', 'noun')],
[[(replsfx, 'ой', 'овец')], ('adj', 'noun')],
[[(replsfx, 'ий', 'овец')], ('adj', 'noun')],

[[(replsfx, 'ый', 'авец')], ('adj', 'noun')],
[[(replsfx, 'ой', 'авец')], ('adj', 'noun')],
[[(replsfx, 'ий', 'авец')], ('adj', 'noun')],

# 289
[[(replsfx, 'ый', 'ист')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ист')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ист')], ('adj', 'noun')],

# 290
[[(replsfx, 'ый', 'изм')], ('adj', 'noun')],
[[(replsfx, 'ой', 'изм')], ('adj', 'noun')],
[[(replsfx, 'ий', 'изм')], ('adj', 'noun')],

# 291
[[(replsfx, 'ый', 'ак')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ак')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ак')], ('adj', 'noun')],

[[(replsfx, 'ый', 'як')], ('adj', 'noun')],
[[(replsfx, 'ой', 'як')], ('adj', 'noun')],
[[(replsfx, 'ий', 'як')], ('adj', 'noun')],

[[(replsfx, 'ый', 'чяк')], ('adj', 'noun')],
[[(replsfx, 'ой', 'чяк')], ('adj', 'noun')],
[[(replsfx, 'ий', 'чяк')], ('adj', 'noun')],

# 292
[[(replsfx, 'ый', 'ач')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ач')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ач')], ('adj', 'noun')],

# 293
[[(replsfx, 'ый', 'ка')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ка')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ка')], ('adj', 'noun')],

[[(replsfx, 'ый', 'ушка')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ушка')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ушка')], ('adj', 'noun')],

[[(replsfx, 'ый', 'ашка')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ашка')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ашка')], ('adj', 'noun')],

[[(replsfx, 'ый', 'яшка')], ('adj', 'noun')],
[[(replsfx, 'ой', 'яшка')], ('adj', 'noun')],
[[(replsfx, 'ий', 'яшка')], ('adj', 'noun')],

[[(replsfx, 'ый', 'анка')], ('adj', 'noun')],
[[(replsfx, 'ой', 'анка')], ('adj', 'noun')],
[[(replsfx, 'ий', 'анка')], ('adj', 'noun')],

[[(replsfx, 'ый', 'янка')], ('adj', 'noun')],
[[(replsfx, 'ой', 'янка')], ('adj', 'noun')],
[[(replsfx, 'ий', 'янка')], ('adj', 'noun')],

[[(replsfx, 'ый', 'инка')], ('adj', 'noun')],
[[(replsfx, 'ой', 'инка')], ('adj', 'noun')],
[[(replsfx, 'ий', 'инка')], ('adj', 'noun')],

[[(replsfx, 'ый', 'овка')], ('adj', 'noun')],
[[(replsfx, 'ой', 'овка')], ('adj', 'noun')],
[[(replsfx, 'ий', 'овка')], ('adj', 'noun')],

# 294
[[(replsfx, 'ый', 'ина')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ина')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ина')], ('adj', 'noun')],

[[(replsfx, 'ый', 'овина')], ('adj', 'noun')],
[[(replsfx, 'ой', 'овина')], ('adj', 'noun')],
[[(replsfx, 'ий', 'овина')], ('adj', 'noun')],

# 295
[[(replsfx, 'ый', 'атина')], ('adj', 'noun')],
[[(replsfx, 'ой', 'атина')], ('adj', 'noun')],
[[(replsfx, 'ий', 'атина')], ('adj', 'noun')],

[[(replsfx, 'ый', 'ятина')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ятина')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ятина')], ('adj', 'noun')],

# 297
[[(replsfx, 'ый', 'уха')], ('adj', 'noun')],
[[(replsfx, 'ой', 'уха')], ('adj', 'noun')],
[[(replsfx, 'ий', 'уха')], ('adj', 'noun')],

[[(replsfx, 'ый', 'юха')], ('adj', 'noun')],
[[(replsfx, 'ой', 'юха')], ('adj', 'noun')],
[[(replsfx, 'ий', 'юха')], ('adj', 'noun')],

# 298
[[(replsfx, 'ый', 'уша')], ('adj', 'noun')],
[[(replsfx, 'ой', 'уша')], ('adj', 'noun')],
[[(replsfx, 'ий', 'уша')], ('adj', 'noun')],

# 299
[[(replsfx, 'ый', 'яга')], ('adj', 'noun')],
[[(replsfx, 'ой', 'яга')], ('adj', 'noun')],
[[(replsfx, 'ий', 'яга')], ('adj', 'noun')],

# 300
[[(replsfx, 'ый', 'юга')], ('adj', 'noun')],
[[(replsfx, 'ой', 'юга')], ('adj', 'noun')],
[[(replsfx, 'ий', 'юга')], ('adj', 'noun')],

# 301
[[(replsfx, 'ый', 'уля')], ('adj', 'noun')],
[[(replsfx, 'ой', 'уля')], ('adj', 'noun')],
[[(replsfx, 'ий', 'уля')], ('adj', 'noun')],

[[(replsfx, 'ый', 'юля')], ('adj', 'noun')],
[[(replsfx, 'ой', 'юля')], ('adj', 'noun')],
[[(replsfx, 'ий', 'юля')], ('adj', 'noun')],

# 302
[[(replsfx, 'ый', 'иш')], ('adj', 'noun')],
[[(replsfx, 'ой', 'иш')], ('adj', 'noun')],
[[(replsfx, 'ий', 'иш')], ('adj', 'noun')],

[[(replsfx, 'ый', 'ыш')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ыш')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ыш')], ('adj', 'noun')],

# 303
[[(replsfx, 'ый', 'ышка')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ышка')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ышка')], ('adj', 'noun')],

# 305
[[(replsfx, 'ый', 'ье')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ье')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ье')], ('adj', 'noun')],

[[(replsfx, 'ый', 'ьё')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ьё')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ьё')], ('adj', 'noun')],

# 306
[[(replsfx, 'ый', 'щина')], ('adj', 'noun')],
[[(replsfx, 'ой', 'щина')], ('adj', 'noun')],
[[(replsfx, 'ий', 'щина')], ('adj', 'noun')],

[[(replsfx, 'ый', 'чина')], ('adj', 'noun')],
[[(replsfx, 'ой', 'чина')], ('adj', 'noun')],
[[(replsfx, 'ий', 'чина')], ('adj', 'noun')],

# 307
[[(replsfx, 'ый', 'ика')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ика')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ика')], ('adj', 'noun')],

# 308
[[(replsfx, 'ый', 'ище')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ище')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ище')], ('adj', 'noun')],

# 311
[[(replsfx, 'ый', 'ко')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ко')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ко')], ('adj', 'noun')],

# 312
[[(replsfx, 'ичный', 'ин')], ('adj', 'noun')],
[[(replsfx, 'ический', 'ин')], ('adj', 'noun')],
[[(replsfx, 'ый', 'ин')], ('adj', 'noun')],

## Существительные со значением отвлеченного признака

# 315
[[(replsfx, 'ый', 'ость')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ость')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ость')], ('adj', 'noun')],

[[(replsfx, 'ий', 'есть')], ('adj', 'noun')],

# 317
[[(replsfx, 'ый', 'ество')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ество')], ('adj', 'noun')],
[[(replsfx, 'ый', 'ство')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ство')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ство')], ('adj', 'noun')],
[[(replsfx, 'ский', 'ство')], ('adj', 'noun')],

# 317
[[(replsfx, 'ый', 'щина')], ('adj', 'noun')],
[[(replsfx, 'ский', 'щина')], ('adj', 'noun')],
[[(replsfx, 'ский', 'чина')], ('adj', 'noun')],
[[(replsfx, 'ой', 'щина')], ('adj', 'noun')],

# 319
[[(replsfx, 'ый', 'изм')], ('adj', 'noun')],
[[(replsfx, 'ический', 'изм')], ('adj', 'noun')],
[[(replsfx, 'ский', 'изм')], ('adj', 'noun')],
[[(replsfx, 'ой', 'изм')], ('adj', 'noun')],

# 320
[[(replsfx, 'ый', 'ие')], ('adj', 'noun')],
[[(replsfx, 'ый', 'ствие')], ('adj', 'noun')],
[[(replsfx, 'ый', 'ье')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ие')], ('adj', 'noun')],

# 321
[[(replsfx, 'ый', 'ица')], ('adj', 'noun')],

# 322
[[(replsfx, 'ый', 'ота')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ота')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ета')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ота')], ('adj', 'noun')],

# 323
[[(replsfx, 'ый', 'изна')], ('adj', 'noun')],
[[(replsfx, 'ой', 'изна')], ('adj', 'noun')],
[[(replsfx, 'ий', 'изна')], ('adj', 'noun')],

# 324
[[(replsfx, 'ый', 'ина')], ('adj', 'noun')],
[[(replsfx, 'ий', 'ина')], ('adj', 'noun')],
[[(replsfx, 'окий', 'ина')], ('adj', 'noun')],
[[(replsfx, 'кий', 'ина')], ('adj', 'noun')],

# 325
[[(replsfx, 'ый', 'инка')], ('adj', 'noun')],
[[(replsfx, 'ий', 'инка')], ('adj', 'noun')],
[[(replsfx, 'ой', 'инка')], ('adj', 'noun')],

# 326
[[(replsfx, 'ый', 'еца')], ('adj', 'noun')],
[[(replsfx, 'ый', 'ца')], ('adj', 'noun')],
[[(replsfx, 'ой', 'ца')], ('adj', 'noun')],

## СУЩЕСТВИТЕЛЬНЫЕ, МОТИВИРОВАННЫЕ СУЩЕСТВИТЕЛЬНЫМИ





]









test = Derivation
ans = []
wrd, ps = 'дрожать', 'verb'
for rule in rules_verb:
    if rule[1][0] == ps:
        ans += test.apply_pattern(POS(wrd, ps), rule)
for elem in ans:
    print(elem.graphics.lower())


"""
class Rule:
    def __init__(self, name: str, pos_b: str, pos_a: str, rules: list(), afxs_b:list(), afxs_a:list()):
        self.name = name
        self.rule = rules
        self.pos_b = pos_b
        self.pos_a = pos_a
        self.afxs_b = afxs_b
        self.afxs_a = afxs_a

AdjRules = [
    Rule('p117_1', 'noun', 'adj', [(replsfx, 'а', 'ин')],
         afxs_b=[Suffix(gr='а', on_b=consonants, ex_b=[])],
         afxs_a=[Suffix(gr='ин', on_b=consonants, ex_b=[])],
    )
]
"""