from copy import deepcopy

"""
def replsfx(sfx_b, sfx_a, try_sub=list()):
    possible_words = list()
    if word[-len(sfx_b):] == sfx_b:
        w_a = word[:-len(sfx_b)]
        for sub in try_sub + [('', '')]:
            if w_a[-len(sub[0]):] == sub[0]:
                new_word = w_a[:-len(sub[0])] + sub[1] + sfx_a
                possible_words.append(new_word)
    return possible_words


def addpfx(pfx_a, try_sub=list()):
    possible_words = list()
    w_a = word
    for sub in try_sub + [('', '')]:
        if w_a[:len(sub[0])] == sub[0]:
            new_word = pfx_a + sub[1] + w_a[len(sub[0]):]
            possible_words.append(new_word)
    return possible_words


def addsfx(sfx_a, try_sub=list()):
    possible_words = list()
    w_a = word
    for sub in try_sub + [('', '')]:
        if w_a[:-len(sub[0])] == sub[0]:
            new_word = sfx_a + sub[1] + w_a[-len(sub[0]):]
            possible_words.append(new_word)
    return possible_words


def delpfx(pfx_b, try_sub=list()):
    possible_words = list()
    if word[:len(pfx_b)] == pfx_b:
        w_a = word[len(pfx_b):]
        for sub in try_sub + [('', '')]:
            if w_a[:len(sub[0])] == sub[0]:
                new_word = w_a[len(sub[0]):] + sub[1]
                possible_words.append(new_word)
    return possible_words


def delsfx(sfx_b, try_sub=list()):
    possible_words = list()
    if word[-len(sfx_b):] == sfx_b:
        w_a = word[:-len(sfx_b)]
        for sub in try_sub + [('', '')]:
            if w_a[-len(sub[0]):] == sub[0]:
                new_word = w_a[:-len(sub[0])] + sub[1]
                possible_words.append(new_word)
    return possible_words
"""


class DerivationADV:
    def __init__(self, word, pos):
        self.word = word
        self.pos = pos
        self.possible_derivatives = list()

    def generate(self):

        def replsfx(word, kwargs):
            try_sub = [] if len(kwargs) == 2 else kwargs[-1]
            sfx_b, sfx_a = kwargs[0], kwargs[1]
            possible_words = list()
            if len(word) > len(sfx_b) and word[-len(sfx_b):] == sfx_b:
                w_a = word[:-len(sfx_b)]
                possible_words.append(w_a + sfx_a)
                for sub in try_sub:
                    if len(w_a) >= len(sub[0]) and w_a[-len(sub[0]):] == sub[0]:
                        new_word = w_a[:-len(sub[0])] + sub[1] + sfx_a
                        possible_words.append(new_word)
            return possible_words

        def addpfx(word, kwargs):
            try_sub = [] if len(kwargs) == 1 else kwargs[-1]
            pfx_a = kwargs[0]
            possible_words = list()
            w_a = word
            possible_words.append(pfx_a + w_a)
            for sub in try_sub:
                if w_a[:len(sub[0])] == sub[0]:
                    new_word = pfx_a + sub[1] + w_a[len(sub[0]):]
                    possible_words.append(new_word)
            return possible_words

        def addsfx(word, kwargs):
            try_sub = [] if len(kwargs) == 1 else kwargs[-1]
            sfx_a = kwargs[0]
            possible_words = list()
            w_a = word
            possible_words.append(w_a + sfx_a)
            for sub in try_sub:
                if w_a[-len(sub[0]):] == sub[0]:
                    new_word = w_a[:-len(sub[0])] + sub[1] + sfx_a
                    possible_words.append(new_word)
            return possible_words

        def delpfx(word, kwargs):
            try_sub = [] if len(kwargs) == 1 else kwargs[-1]
            pfx_b = kwargs[0]
            possible_words = list()
            if word[:len(pfx_b)] == pfx_b:
                w_a = word[len(pfx_b):]
                possible_words.append(w_a)
                for sub in try_sub:
                    if w_a[:len(sub[0])] == sub[0]:
                        new_word = w_a[len(sub[0]):] + sub[1]
                        possible_words.append(new_word)
            return possible_words

        def delsfx(word, kwargs):
            try_sub = [] if len(kwargs) == 1 else kwargs[-1]
            sfx_b = kwargs[0]
            possible_words = list()
            if word[-len(sfx_b):] == sfx_b:
                w_a = word[:-len(sfx_b)]
                possible_words.append(w_a)
                for sub in try_sub:
                    if w_a[-len(sub[0]):] == sub[0]:
                        new_word = w_a[:-len(sub[0])] + sub[1]
                        possible_words.append(new_word)
            return possible_words

        rules = [
            # ('быстрый', 'быстро')
            [[(replsfx, 'ый', 'о')], ('adj', 'adv')],

            # ('всяческий', 'всячески')
            [[(replsfx, 'ий', 'и')], ('adj', 'adv')],

            # ('дружеский', 'по-дружески')
            [[(replsfx, 'ий', 'и'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('певучий', 'певуче')
            [[(replsfx, 'ий', 'е')], ('adj', 'adv')],

            # ('зябкий', 'зябко')
            [[(replsfx, 'ий', 'о')], ('adj', 'adv')],

            # ('вечер', 'вечером')
            [[(addsfx, 'ом')], ('noun', 'adv')],

            # ('видимый', 'по-видимому')
            [[(replsfx, 'ый', 'ому'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('вдалеке', 'невдалеке')
            [[(addpfx, 'не')], ('adv', 'adv')],

            # ('трудно', 'трудненько')
            [[(replsfx, 'о', 'енько')], ('adv', 'adv')],

            # ('прежний', 'по-прежнему')
            [[(replsfx, 'ий', 'ему'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('верх', 'вверх')
            [[(addpfx, 'в')], ('noun', 'adv')],

            # ('деловой', 'по-деловому')
            [[(replsfx, 'ой', 'ому'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('рано', 'ранёхонько')
            [[(replsfx, 'о', 'ёхонько')], ('adv', 'adv')],

            # ('верх', 'наверх')
            [[(addpfx, 'на')], ('noun', 'adv')],

            # ('прямой', 'впрямую')
            [[(replsfx, 'ой', 'ую'), (addpfx, 'в')], ('adj', 'adv')],

            # ('весна', 'весной')
            [[(replsfx, 'а', 'ой')], ('noun', 'adv')],

            # ('пять', 'пятью')
            [[(addsfx, 'ю')], ('num', 'adv')],

            # ('рано', 'ранёшенько')
            [[(replsfx, 'о', 'ёшенько')], ('adv', 'adv')],

            # ('светло', 'засветло')
            [[(addpfx, 'за')], ('adv', 'adv')],

            # ('белый', 'добела')
            [[(replsfx, 'ый', 'а'), (addpfx, 'до')], ('adj', 'adv')],

            # ('долго', 'подолгу')
            [[(replsfx, 'о', 'у'), (addpfx, 'по')], ('adv', 'adv')],
    
            # ('тихо', 'тихонько')
            [[(replsfx, 'о', 'онько'), (addpfx,  'по')], ('adv', 'adv')],
    
            # ('прочь', 'напрочь')
            [[(addpfx,  'на')], ('adv', 'adv')],
    
            # ('полный', 'сполна')
            [[(replsfx, 'ый', 'а'),(addpfx,  'с')], ('adj', 'adv')],
    
            # ('половина', 'вполовину')
            [[(replsfx, 'а', 'у'),(addpfx,'в')], ('noun', 'adv')],
    
            # ('малый', 'вмале')
            [[(replsfx, 'ый', 'е'),(addpfx,'в')], ('adj', 'adv')],
    
            # ('грубый', 'нагрубо')
            [[(replsfx, 'ый', 'о'), (addpfx,'на')], ('adj', 'adv')],

            # ('озорной', 'озорно')
            [[(replsfx, 'ой', 'о')], ('adj', 'adv')],

            # ('ныне', 'поныне')
            [[(addpfx, 'по')], ('adv', 'adv')],

            # ('встреча', 'навстречу')
            [[(replsfx, 'а', 'у'),(addpfx, 'на')], ('noun', 'adv')],
    
            # ('четверо', 'вчетвером')
            [[(replsfx, 'о', 'ом'),(addpfx, 'в')], ('num', 'adv')],
    
            # ('мелькать', 'мельком')
            [[(replsfx, 'ать', 'ом')], ('verb', 'adv')],
    
            # ('веселый', 'навеселе')
            [[(replsfx, 'ый', 'е'),(addpfx, 'на')], ('adj', 'adv')],
    
            # ('пятеро', 'впятеро')
            [[(addpfx, 'в')], ('num', 'adv')],
    
            # ('торчать', 'торчмя')
            [[(replsfx, 'ать', 'мя')], ('verb', 'adv')],
    
            # ('напрасный', 'понапрасну')
            [[(replsfx, 'ый', 'у'),(addpfx, 'по')], ('adj', 'adv')],
    
            # ('редкий', 'изредка')
            [[(replsfx, 'ий', 'а'),(addpfx, 'из')], ('adj', 'adv')],
    
            # ('крепкий', 'накрепко')
            [[(replsfx, 'ий', 'о'),(addpfx, 'на')], ('adj', 'adv')],
    
            # ('средина', 'посредине')
            [[(replsfx, 'а', 'е'),(addpfx, 'по')], ('noun', 'adv')],
    
            # ('ныне', 'доныне')
            [[(addpfx, 'до')], ('adv', 'adv')],
    
            # ('десятый', 'в-десятых')
            [[(replsfx, 'ый', 'ых'),(addpfx, 'в-')], ('num', 'adv')],
    
            # ('мертвый', 'замертво')
            [[(replsfx, 'ый', 'о'),(addpfx, 'за')], ('adj', 'adv')],
    
            # ('бок', 'обок')
            [[(addpfx, 'о')], ('noun', 'adv')],
    
            # ('бок', 'сбоку')
            [[(addsfx, 'у'),(addpfx, 'с')], ('noun', 'adv')],
    
            #  ('два', 'дважды')
            [[(addsfx, 'жды')], ('num', 'adv')],

            # ('белый', 'белым')
            [[(replsfx, 'ый', 'ым')], ('adj', 'adv')],
    
            # ('рано', 'рановато')
            [[(replsfx, 'о', 'овато')], ('adv', 'adv')],
    
            #  ('хорошенько', 'хорошенечко')
            [[(replsfx, 'о', 'енько')], ('adv', 'adv')],
    
            # ('лёгкий', 'слегка')
            [[(replsfx, 'ий', 'а'),(addpfx, 'с')], ('adj', 'adv')],
    
            # ('простой', 'попросту')
            [[(replsfx, 'ой', 'у'),(addpfx, 'по')], ('adj', 'adv')],
    
            # ('косой', 'накосо')
            [[(replsfx, 'ой', 'о'),(addpfx, 'на')], ('adj', 'adv')],
    
            # ('низ', 'донизу')
            [[(addsfx, 'у'),(addpfx, 'до')], ('noun', 'adv')],
    
            # ('весна', 'весною')
            [[(replsfx, 'а', 'ою')], ('noun', 'adv')],
    
            # ('сидеть', 'сидмя')
            [[(replsfx, 'еть', 'мя')], ('verb', 'adv')],
    
            # ('расхватать', 'нарасхват')
            [[(delsfx, 'ать'),(addpfx, 'на')], ('verb', 'adv')],
    
            # ('вразвалку', 'вразвалочку')
            [[(replsfx, 'у', 'ку', [('к', 'оч')]),(addpfx, 'в')], ('adv', 'adv')],
    
            # ('серый', 'иссера')
            [[(replsfx, 'ый', 'а'),(addpfx, 'ис')], ('adj', 'adv')],
    
            # ('живой', 'вживе')
            [[(replsfx, 'ой', 'е'),(addpfx, 'в')], ('adj', 'adv')],
    
            # ('простой', 'спроста')
            [[(replsfx, 'ой', 'а'),(addpfx, 'с')], ('adj', 'adv')],
    
            # ('верх', 'кверху')
            [[(addsfx, 'у'),(addpfx, 'к')], ('noun', 'adv')],
    
            # ('догонять', 'вдогон')
            [[(delsfx, 'ять'),(addpfx, 'в')], ('verb', 'adv')],
    
            # ('перебивать', 'вперебой') ##### ???
            [[(replsfx, 'а', 'у'),(addpfx, 'в')], ('verb', 'adv')],
    
            # ('скользить', 'скользом')
            [[(replsfx, 'ить', 'ом')], ('verb', 'adv')],
    
            # ('коротко', 'коротенько')
            [[(replsfx, 'о', 'енько', [('к', '')]),(addpfx, 'в')], ('adv', 'adv')],
    
            # ('ныне', 'отныне')
            [[(addpfx, 'от')], ('adv', 'adv')],
    
            # ('плотный', 'вплотную')
            [[(replsfx, 'ый', 'ую'),(addpfx, 'в')], ('adj', 'adv')],
    
            # ('малый', 'смалу')
            [[(replsfx, 'ый', 'у'),(addpfx, 'с')], ('adj', 'adv')],
    
            # ('новый', 'сызнова')
            [[(replsfx, 'ый', 'а'),(addpfx, 'сыз')], ('adj', 'adv')],
    
            # ('краткий', 'вкратце')
            [[(replsfx, 'ий', 'е', [('к', 'ц')]),(addpfx, 'в')], ('adj', 'adv')],
    
            # ('короткий', 'накоротке')
            [[(replsfx, 'ий', 'е'),(addpfx, 'на')], ('adj', 'adv')],
    
            # ('косой', 'искоса')
            [[(replsfx, 'ой', 'а'),(addpfx, 'ис')], ('adj', 'adv')],
    
            # ('рядом', 'рядком')
            [[(replsfx, 'ом', 'ком')], ('adj', 'adv')],
    
            # ('займ', 'взаймы')
            [[(addsfx, 'ы'),(addpfx, 'в')], ('noun', 'adv')],
    
            # ('перед', 'спереди')
            [[(addsfx, 'и'),(addpfx, 'с')], ('noun', 'adv')],
    
            # ('ночь', 'ночью')
            [[(addsfx, 'ю')], ('noun', 'adv')],
    
            # ('муж', 'замуж')
            [[(addpfx, 'за')], ('noun', 'adv')],
    
            # ('верх', 'вверху')
            [[(addsfx, 'у'),(addpfx, 'в')], ('noun', 'adv')],
    
            # ('утро', 'утром')
            [[(replsfx, 'о', 'ом')], ('noun', 'adv')],
    
            # ('воля', 'волей')
            [[(replsfx, 'я', 'ей')], ('noun', 'adv')],
    
            # ('двое', 'надвое')
            [[(addpfx, 'на')], ('num', 'adv')],
    
            # ('двое', 'вдвоём')
            [[(replsfx, 'ое', 'оём'),(addpfx, 'в')], ('num', 'adv')],
    
            # ('желтый', 'изжелта')
            [[(replsfx, 'ый', 'а'),(addpfx, 'из')], ('adj', 'adv')],
    
            # ('новый', 'вновь')
            [[(delsfx, 'ый', [('в', 'вь')]),(addpfx, 'в')], ('adj', 'adv')],
    
            # ('тихо', 'тихохонько')
            [[(replsfx, 'о', 'онько')], ('adv', 'adv')],
    
            # ('тихий', 'втихую')
            [[(replsfx, 'ий', 'ую'),(addpfx, 'в')], ('adv', 'adv')],
    
            # ('общий', 'сообща')
            [[(replsfx, 'ий', 'а'),(addpfx, 'со')], ('adv', 'adv')],
    
            # ('седьмой', 'в-седьмых')
            [[(replsfx, 'ой', 'ых'),(addpfx, 'в-')], ('num', 'adv')],
    
            # ('нагой', 'донага')
            [[(replsfx, 'ой', 'а'),(addpfx, 'до')], ('adj', 'adv')],
    
            # ('живой', 'заживо')
            [[(replsfx, 'ой', 'о'),(addpfx, 'за')], ('adj', 'adv')],
    
            # ('прямой', 'напрямую')
            [[(replsfx, 'ой', 'ую'),(addpfx, 'на')], ('adj', 'adv')],

            # ('молодой', 'смолоду')
            [[(replsfx, 'ой', 'у'),(addpfx, 'с')], ('adj', 'adv')],
    
            # ('близость', 'поблизости')
            [[(delsfx, '', [('ь', 'и')]),(addpfx, 'по')], ('noun', 'adv')],
    
            # ('верх', 'поверх')
            [[(addpfx, 'по')], ('noun', 'adv')],
    
            # ('даль', 'вдали')
            [[(addsfx, 'и'),(addpfx, 'в')], ('noun', 'adv')],
    
            # ('один', 'единожды')
            [[(addsfx, 'ожды', [('один', 'един')])], ('num', 'adv')],
    
            # ('догонять', 'вдогонку')
            [[(replsfx, 'ять', 'ку'),(addpfx, 'в')], ('verb', 'adv')],
    
            # ('ползать', 'ползком')
            [[(replsfx, 'ать', 'ком')], ('verb', 'adv')],
    
            # ('перемежать', 'вперемежку')
            [[(replsfx, 'ать', 'ку'),(addpfx, 'в')], ('verb', 'adv')],
    
            # ('внутрь', 'вовнутрь')
            [[(addpfx, 'во')], ('adv', 'adv')],
    
            # ('переваливать', 'вперевалку')
            [[(replsfx, 'ивать', 'ку'),(addpfx, 'в')], ('verb', 'adv')],
    
            # ('новый', 'по-новой')
            [[(replsfx, 'ый', 'ой'),(addpfx, 'по')], ('adj', 'adv')],
    
            # ('правый', 'вправо')
            [[(replsfx, 'ый', 'о'),(addpfx, 'в')], ('adj', 'adv')],
    
            # ('чистый', 'начистую')
            [[(replsfx, 'ый', 'ую'),(addpfx, 'на')], ('adj', 'adv')],
    
            # ('частый', 'зачастую')
            [[(replsfx, 'ый', 'ую'),(addpfx, 'за')], ('adj', 'adv')],
    
            # ('старый', 'исстари')
            [[(replsfx, 'ый', 'и'),(addpfx, 'ис')], ('adj', 'adv')],
    
            # ('чистый', 'подчистую')
            [[(replsfx, 'ый', 'ую'),(addpfx, 'под')], ('adj', 'adv')],
    
            # ('пеший', 'пешком')
            [[(replsfx, 'ий', 'ком')], ('adj', 'adv')],
    
            # ('тихий', 'по-тихому')
            [[(replsfx, 'ий', 'ому'),(addpfx, 'по-')], ('adj', 'adv')],
    
            # ('поздний', 'допоздна')
            [[(replsfx, 'ий', 'а'),(addpfx, 'до')], ('adj', 'adv')],
    
            # ('синий', 'иссиня')
            [[(replsfx, 'ий', 'я'),(addpfx, 'ис')], ('adj', 'adv')],
    
            # ('древлий', 'издревле')
            [[(replsfx, 'ий', 'е'),(addpfx, 'из')], ('adj', 'adv')],
    
            # ('босой', 'босиком')
            [[(replsfx, 'ой', 'иком')], ('adj', 'adv')],
    
            # ('твой', 'по-твоему')
            [[(replsfx, 'ой', 'оему'),(addpfx, 'по-')], ('pron', 'adv')],
    
            # ('слепой', 'сослепу')
            [[(replsfx, 'ой', 'у'),(addpfx, 'со')], ('adj', 'adv')],
    
            # ('косой', 'вкось')
            [[(delsfx, 'ой', [('', 'ь')]),(addpfx, 'в')], ('adj', 'adv')],
    
            # ('век', 'вовек')
            [[(addpfx, 'во')], ('noun', 'adv')],
    
            # ('верх', 'поверху')
            [[(addsfx, 'у'),(addpfx, 'по')], ('noun', 'adv')],
    
            # ('верх', 'наверху')
            [[(addsfx, 'у'),(addpfx, 'на')], ('noun', 'adv')],
    
            # ('утро', 'поутру')
            [[(replsfx, 'о', 'у'),(addpfx, 'по')], ('noun', 'adv')],
    
            # ('начало', 'вначале')
            [[(replsfx, 'о', 'е'),(addpfx, 'в')], ('noun', 'adv')],
    
            # ('стороной', 'сторонкой')
            [[(replsfx, 'ой', 'кой')], ('adv', 'adv')],
    
            # ('дни', 'днями')
            [[(replsfx, 'и', 'ями')], ('noun', 'adv')],
    
            # ('висеть', 'на весу')
            [[(replsfx, 'еть', 'у'),(addpfx, 'на ')], ('verb', 'adv')],

            # ('урывать', 'урывками')
            [[(replsfx, 'ать', 'ками')], ('verb', 'adv')],
    
            # ('ощупать', 'ощупью')
            [[(replsfx, 'ать', 'ю', [('', 'ь')])], ('verb', 'adv')],
    
            # ('насторожиться', 'настороже')
            [[(replsfx, 'иться', 'е')], ('verb', 'adv')],
    
            # ('родиться', 'отроду')
            [[(replsfx, 'иться', 'у'),(addpfx, 'от')], ('verb', 'adv')],
    
            # ('повалить', 'наповал')
            [[(delsfx, 'ить'),(addpfx, 'на')], ('verb', 'adv')],
    
            # ('выше', 'свыше')
            [[(addpfx, 'с')], ('adv', 'adv')],
    
            # ('завтра', 'послезавтра')
            [[(addpfx, 'после')], ('adv', 'adv')],
    
            # ('вчера', 'позавчера')
            [[(addpfx, 'поза')], ('adv', 'adv')],
    
            # ('дурной', 'сдуру')
            [[(replsfx, 'ой', 'у', [('н', '')]),(addpfx, 'с')], ('adj', 'adv')],

            # ('панибратский', 'запанибрата')
            [[(replsfx, 'ский', 'а'), (addpfx, 'за')], ('adj', 'adv')],

            # ('вытягивать', 'навытяжку')
            [[(replsfx, 'ивать', 'ку', [('г', 'ж')]), (addpfx, 'на')], ('verb', 'adv')],

            # ('приглядывать', 'вприглядку')
            [[(replsfx, 'ывать', 'у'), (addpfx, 'а')], ('verb', 'adv')],

            # ('перечесть', 'наперечет')
            [[(replsfx, 'сть', 'т'), (addpfx, 'на')], ('verb', 'adv')],

            # ('целый', 'целиком')
            [[(replsfx, 'ый', 'иком')], ('adj', 'adv')],

            # ('голый', 'гольём')
            [[(replsfx, 'ый', 'ём', [('', 'ь')])], ('adj', 'adv')],

            # ('особый', 'особняком')
            [[(replsfx, 'ый', 'няком')], ('adj', 'adv')],

            # ('полный', 'полностью')
            [[(replsfx, 'ый', 'остью')], ('adj', 'adv')],

            # ('малый', 'помаленьку')
            [[(replsfx, 'ый', 'еньку'), (addpfx, 'по')], ('adj', 'adv')],

            # ('запертый', 'взаперти')
            [[(replsfx, 'ый', 'и'), (addpfx, 'в')], ('adj', 'adv')],

            # ('первый', 'во-первых')
            [[(replsfx, 'ый', 'ых'), (addpfx, 'во-')], ('num', 'adv')],

            # ('первый', 'впервой')
            [[(replsfx, 'ый', 'ой'), (addpfx, 'в')], ('num', 'adv')],

            # ('первый', 'впервые')
            [[(replsfx, 'ый', 'ые'), (addpfx, 'в')], ('num', 'adv')],

            # ('единый', 'воедино')
            [[(replsfx, 'ый', 'о'), (addpfx, 'во')], ('adj', 'adv')],

            #  ('верный', 'наверное')
            [[(replsfx, 'ый', 'ое'), (addpfx, 'на')], ('adj', 'adv')],

            # ('верный', 'наверняка')
            [[(replsfx, 'ый', 'яка'), (addpfx, 'на')], ('adj', 'adv')],

            # ('малый', 'сызмальства')
            [[(replsfx, 'ый', 'ства', [('', 'ь')]), (addpfx, 'сыз')], ('adj', 'adv')],

            # ('синий', 'синё')
            [[(replsfx, 'ий', 'ё'), (addpfx, 'до')], ('adj', 'adv')],

            # ('пеший', 'пёхом')
            [[(replsfx, 'ий', 'ом', [('еш', 'ёх')])], ('adj', 'adv')],

            # ('давний', 'давным')
            [[(replsfx, 'ий', 'ым')], ('adj', 'adv')],

            # ('тихий', 'потихоньку')
            [[(replsfx, 'ий', 'оньку'), (addpfx, 'по')], ('adj', 'adv')],

            # ('тихий', 'по-тихой')
            [[(replsfx, 'ий', 'ой'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('общий', 'вообще')
            [[(replsfx, 'ий', 'е'), (addpfx, 'во')], ('adj', 'adv')],

            # ('третий', 'в-третьих')
            [[(replsfx, 'ий', 'их', [('', 'ь')]), (addpfx, 'в-')], ('num', 'adv')],

            # ('синий', 'досиня')
            [[(replsfx, 'ий', 'я'), (addpfx, 'до')], ('adj', 'adv')],

            # ('давний', 'сыздавна')
            [[(replsfx, 'ий', 'а'), (addpfx, 'сыз')], ('adj', 'adv')],

            # ('горячий', 'вгорячах')
            [[(replsfx, 'ий', 'ах'), (addpfx, 'в')], ('adj', 'adv')],

            # ('далёкий', 'неподалёку')
            [[(replsfx, 'ий', 'у'), (addpfx, 'непо')], ('adj', 'adv')],

            # ('тихий', 'исподтишка')
            [[(replsfx, 'ий', 'ка', [('х', 'ш')]), (addpfx, 'изпод')], ('adj', 'adv')],

            # ('воровской', 'воровски')
            [[(replsfx, 'ой', 'и')], ('adj', 'adv')],

            # ('живой', 'живьём')
            [[(replsfx, 'ой', 'ём', [('', 'ь')])], ('adj', 'adv')],

            # ('нагой', 'нагишом')
            [[(replsfx, 'ой', 'ишом')], ('adj', 'adv')],

            # ('косой', 'наискось')
            [[(delsfx, 'ой', [('', 'ь')]), (addpfx, 'наис')], ('adj', 'adv')],

            # ('мужской', 'по-мужски')
            [[(replsfx, 'ой', 'и'), (addpfx, 'по-')], ('adj', 'adv')],

            # ('второй', 'во-вторых')
            [[(replsfx, 'ой', 'ых'), (addpfx, 'во-')], ('num', 'adv')],

            # ('прямой', 'напрямик')
            [[(replsfx, 'ой', 'ик'), (addpfx, 'на')], ('adj', 'adv')],

            # ('прямой', 'напрямки')
            [[(replsfx, 'ой', 'ки'), (addpfx, 'на')], ('adj', 'adv')],

            # ('косой', 'наискосок')
            [[(replsfx, 'ой', 'ок'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('косой', 'наискоски')
            [[(replsfx, 'ой', 'ки'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('пешком', 'пешочком')
            [[(replsfx, 'ом', 'ком', [('к', 'оч')])], ('adv', 'adv')],

            # ('явный', 'въяве')
            [[(replsfx, 'ный', 'е')], ('adj', 'adv')],

            # ('довольный', 'вдоволь')
            [[(delsfx, 'ный'), (addpfx, 'в')], ('adj', 'adv')],

            # ('тайный', 'тайком')
            [[(replsfx, 'ный', 'ком')], ('adj', 'adv')],

            # ('босиком', 'босичком')
            [[(replsfx, 'ом', 'ком', [('к', 'ч')])], ('adv', 'adv')],

            # ('рядом', 'рядышком')
            [[(replsfx, 'ом', 'ышком')], ('adv', 'adv')],

            # ('гусь', 'гуськом')
            [[(addsfx, 'ком')], ('noun', 'adv')],

            # ('даль', 'издали')
            [[(addsfx, 'и'), (addpfx, 'из')], ('noun', 'adv')],

            # ('часть', 'отчасти')
            [[(addsfx, 'и'), (addpfx, 'от')], ('noun', 'adv')],

            # ('муж', 'замужем')
            [[(addsfx, 'ем'), (addpfx, 'за')], ('noun', 'adv')],

            # ('день', 'днем')
            [[(addsfx, 'ём', [('ень', 'н')])], ('noun', 'adv')],

            # ('дом', 'дома')
            [[(addpfx, 'а')], ('noun', 'adv')],

            # ('терпёж', 'невтерпёж')
            [[(addpfx, 'нев')], ('noun', 'adv')],

            # ('запас', 'про запас')
            [[(addpfx, 'про ')], ('noun', 'adv')],

            # ('детство', 'сыздетства')
            [[(replsfx, 'о', 'а'), (addpfx, 'сыз')], ('noun', 'adv')],

            # ('утро', 'наутро')
            [[(replsfx, 'о', 'о'), (addpfx, 'на')], ('noun', 'adv')],

            # ('сила', 'силком')
            [[(replsfx, 'а', 'ком')], ('noun', 'adv')],

            # ('сила', 'силом')
            [[(replsfx, 'а', 'ом')], ('noun', 'adv')],

            # ('времена', 'временами')
            [[(replsfx, 'а', 'ами')], ('noun_pl', 'adv')],

            # ('истина', 'воистину')
            [[(replsfx, 'а', 'у'), (addpfx, 'во')], ('noun', 'adv')],

            # ('неволя', 'поневоле')
            [[(replsfx, 'я', 'е'), (addpfx, 'по')], ('noun', 'adv')],

            # ('ничья', 'вничью')
            [[(replsfx, 'я', 'ю'), (addpfx, 'в')], ('adj', 'adv')],

            # ('один', 'однажды')
            [[(addsfx, 'ажды', [('ин', 'н')])], ('num', 'adv')],

            # ('много', 'многажды')
            [[(addsfx, 'ажды')], ('adv', 'adv')],

            # ('стоять', 'стоймя')
            [[(replsfx, 'ять', 'мя')], ('verb', 'adv')],

            # ('стоять', 'стойком')
            [[(replsfx, 'ять', 'йком')], ('verb', 'adv')],

            # ('переменять', 'напеременку')
            [[(replsfx, 'ять', 'ку'), (addpfx, 'на')], ('verb', 'adv')],

            # ('перегонять', 'наперегонки')
            [[(replsfx, 'ять', 'ки'), (addpfx, 'на')], ('verb', 'adv')],

            # ('перегонять', 'вперегонки')
            [[(replsfx, 'ять', 'ки'), (addpfx, 'в')], ('verb', 'adv')],

            # ('рвать', 'срыву')
            [[(replsfx, 'ать', 'у', [('рв', 'рыв')]), (addpfx, 'с')], ('verb', 'adv')],

            # ('голодать', 'впроголодь')
            [[(delsfx, 'ать', [('', 'ь')]), (addpfx, 'впро')], ('verb', 'adv')],

            # ('махать', 'наотмашь')
            [[(delsfx, 'ать', [('х', 'шь')]), (addpfx, 'наот')], ('verb', 'adv')],

            # ('плавать', 'на плаву')
            [[(replsfx, 'ать', 'у'), (addpfx, 'на ')], ('verb', 'adv')],

            # ('бегать', 'в бегах')
            [[(replsfx, 'ать', 'ах'), (addpfx, 'в ')], ('verb', 'adv')],

            # ('умолкать', 'без умолку')
            [[(replsfx, 'ать', 'у'), (addpfx, 'без ')], ('verb', 'adv')],

            # ('кувыркаться', 'кувырком')
            [[(replsfx, 'аться', 'ом')], ('verb', 'adv')],

            # ('просыпаться', 'без просыпу')
            [[(replsfx, 'аться', 'у'), (addpfx, 'без ')], ('verb', 'adv')],

            # ('догадаться', 'невдогад')
            [[(delsfx, 'аться'), (addpfx, 'нев')], ('verb', 'adv')],

            # ('волочь', 'волоком')
            [[(addsfx, 'ом', [('чь', 'к')])], ('verb', 'adv')],

            # ('мочь', 'невмочь')
            [[(addpfx, 'нев')], ('verb', 'adv')],

            # ('мочь', 'невмоготу')
            [[(addsfx, 'оту', [('чь', 'г')]), (addpfx, 'нев')], ('verb', 'adv')],

            # ('родиться', 'сроду')
            [[(replsfx, 'иться', 'у'), (addpfx, 'с')], ('verb', 'adv')],

            # ('дыбиться', 'дыбом')
            [[(replsfx, 'иться', 'ом')], ('verb', 'adv')],

            # ('торопиться', 'второпях')
            [[(replsfx, 'иться', 'ях'), (addpfx, 'в')], ('verb', 'adv')],

            # ('отвалиться', 'до отвала')
            [[(replsfx, 'иться', 'а'), (addpfx, 'до ')], ('verb', 'adv')],

            # ('отвалиться', 'до отвалу')
            [[(replsfx, 'иться', 'у'), (addpfx, 'до ')], ('verb', 'adv')],

            # ('перекосить', 'наперекосяк')
            [[(replsfx, 'ить', 'як'), (addpfx, 'на')], ('verb', 'adv')],

            # ('трусить', 'труском')
            [[(replsfx, 'ить', 'ком')], ('verb', 'adv')],

            # ('ходить', 'ходуном')
            [[(replsfx, 'ить', 'уном')], ('verb', 'adv')],

            # ('скользить', 'вскользь')
            [[(addsfx, 'ить', [('', 'ь')]), (addpfx, 'в')], ('verb', 'adv')],

            # ('набить', 'битком')
            [[(addsfx, 'ком', [('ь', '')]), (delpfx, 'на')], ('verb', 'adv')],

            # ('близко', 'близёхонько')
            # [[(replsfx, 'о', 'ёхонько'), [('к', '')]], ('adv', 'adv')],

            # ('всюду', 'отовсюду')
            [[(addpfx, 'ото')], ('adv', 'adv')],

            # ('вне', 'извне')
            [[(addpfx, 'из')], ('adv', 'adv')],

            # ('мамин', 'по-маминому')
            [[(addsfx, 'ому'), (addpfx, 'по-')], ('adj_', 'adv')],

            # ('наш', 'по-нашему')
            [[(addsfx, 'ему'), (addpfx, 'по-')], ('pron', 'adv')],

            # ('близкий', 'вблизи')
            [[(replsfx, 'ий', 'и', [('к', '')]), (addpfx, 'в')], ('adj', 'adv')],

            # ('легкий', 'полегоньку')
            [[(replsfx, 'ий', 'оньку', [('к', '')]), (addpfx, 'по')], ('adj', 'adv')],

            # ('последствие', 'впоследствии')
            [[(replsfx, 'е', 'и'), (addpfx, 'в')], ('noun', 'adv')],

            # ('двое', 'вдвое')
            [[(addpfx, 'в')], ('num', 'adv')],

            # ('перебивать', 'наперебой') # нихачю (
            # [[(replsfx, 'ой', 'ки'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('упасть', 'доупаду')
            [[(replsfx, 'ть', 'у', [('ст', 'д')]), (addpfx, 'до')], ('verb', 'adv')],

            # ('проснуться', 'впросонках')
            [[(replsfx, 'уться', 'ках', [('сн', 'сон')]), (addpfx, 'в')], ('verb', 'adv')],

            # ('проснуться', 'спросонок')
            [[(replsfx, 'усться', 'ок', [('сн', 'сон')]), (addpfx, 'с')], ('verb', 'adv')],

            # ('проснуться', 'спросонья')
            [[(replsfx, 'усться', 'я', [('сн', 'сонь')]), (addpfx, 'с')], ('verb', 'adv')],

            # ('удаться', 'наудалую')

            # [[(replsfx, '', 'ки'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('нарочно', 'понарошку')
            [[(replsfx, 'но', 'ку', [('ч', 'ш')]), (addpfx, 'по')], ('adv', 'adv')],

            # ('провернуть', 'невпроворот')
            # [[(replsfx, 'ой', 'ки'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('вывернуть', 'навыворот')
            # [[(replsfx, 'ой', 'ки'), (addpfx, 'наис')], ('adj', 'adv')],

            # ('земля', 'наземь')
            [[(delsfx, 'ля', [('', 'ь')]), (addpfx, 'на')], ('noun', 'adv')],

            # ('играть', 'играючи')
            [[(replsfx, 'ать', 'аючи')], ('verb', 'adv')],

            # ('шестой', 'вполшестого')
            [[(replsfx, 'ой', 'ого'), (addpfx, 'впол')], ('num', 'adv')],

            # ('оборот', 'вполоборота')
            [[(addsfx, 'а'), (addpfx, 'впол')], ('noun', 'adv')],


        ]

        all_vars = []

        for r in rules:
            prev_lvl_words = [self.word]
            if self.pos != r[1][0]:
                continue
            for changes in r[0]:
                cur_lvl_words = []
                for w in prev_lvl_words:
                    f, args = changes[0], list(changes[1:])
                    list_new = f(w, args)
                    for new_w in list_new:
                        cur_lvl_words.append(new_w)
                prev_lvl_words = deepcopy(cur_lvl_words)
            for w in prev_lvl_words:
                all_vars.append(w)

        print(all_vars)



test = DerivationADV('соксовский', 'adj')
test.generate()