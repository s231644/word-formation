from wf_ru.prod.functions import *

rules_adj = [
    # 611
    Rule(
        pos_b='noun', pos_a='adj',
        rules=[
            (
                [
                    (onlysfx, {'ь', 'й'}),
                    (delsfx, 'opt', {'ь'}),
                    (delvowel, 'opt'),
                    (addsfx, {'ев'})
                ],
                [
                    (onlysfx, consonants),
                    (addsfx, {'ов'})
                ],
            )
        ]
    ),

    # 612
    Rule(
        pos_b='noun', pos_a='adj',
        rules=[
                (onlysfx, {'а', 'я'}),
                (delsfx, {'а', 'я'}),
                (
                    [
                        (onlysfx, {'ц'}),
                        (addsfx, {'ын'})
                    ],
                    [
                        (excsfx, {'ц'}),
                        (addsfx, {'ин'})
                    ],
                )
        ]
    ),


    Rule(
        pos_b='noun', pos_a='adj',
        rules=[
            (
                [
                    (onlysfx, {'а', 'ь'}),
                    (delsfx, {'а', 'ь'}),
                ],
                [
                    (excsfx, {'а', 'ь'}),
                ],
            ),
            (plt, 'try'),
            (addvowel, 'opt'),
            (addsfx, {'ий'})
        ]
    ),

    # 616
    Rule(
        pos_b='noun', pos_a='adj',
        rules=[
            (
                [
                    (onlysfx, {'я', 'а', 'ь'}),
                    (delsfx, {'я', 'а', 'ь'}),
                ],
                [
                    (excsfx, {'я', 'а', 'ь'}),
                    (replsfx, {('ей', 'ь')}),
                ],
            ),
            (plt, 'try'),
            (delvowel, 'opt'),
            (addsfx, {'иный'})
        ]
    ),

    # 617
    Rule(
        pos_b='noun', pos_a='adj',
        rules=[
            (excsfx, {'тель', 'ист', 'ие', 'ние', 'ация', 'ция'}),
            (delsfx, 'try', {'о', 'а', 'ь'}),
            (plt, 'try'),
            (delvowel, 'opt'),
            (addsfx, {'ный', 'ной'})
        ]
    ),

    Rule(
        pos_b='noun', pos_a='adj',
        rules=[
            (excsfx, {'тель', 'ист', 'ие', 'ние', 'ация', 'ция'}),
            (delsfx, {'о', 'а', 'ь'}),
            (plt, 'try'),
            (delvowel, 'opt'),
            (addsfx, {'енный'})
        ]
    ),

    Rule(
        pos_b='noun', pos_a='adj',
        rules=[
            (onlysfx, {'ация', 'яция'}),
            (delsfx, {'а', 'я'}),
            (addsfx, {'онный'})
        ]
    ),

    Rule(
        pos_b='noun', pos_a='adj',
        rules=[
            (excsfx, {'тель', 'ист', 'ие', 'ние', 'ация', 'ция'}),
            (delsfx, 'try', {'о', 'а', 'ь', 'ия', 'я'}),
            (plt, 'try'),
            (delvowel, 'opt'),
            (addsfx, {'ичный', 'озный', 'орный', 'альный', 'арный', 'уальный', 'ивный', 'иальный', 'онный', 'ональный'})
        ]
    ),

    Rule(
        pos_b='noun', pos_a='adj',
        rules=[
            (onlysfx, {'ние'}),
            (delsfx, {'ние'}),
            (
                [
                    (delsfx, {'е'}),
                    (addsfx, {'ительный'}),
                ],
                [
                    (excsfx, {'е'}),
                    (addsfx, {'тельный'}),
                ],
            ),
        ]
    ),
]
