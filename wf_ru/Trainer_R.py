import pandas as pd

import Automaton_R

wf = Automaton_R.WordFormation()

parsed_words = pd.read_csv('parsed_ADV.csv')
not_parsed_words = pd.read_csv('not_parsed_ADV.csv')
raw_words = pd.read_csv('raw_ADV.csv')

for line in raw_words:
    if len(line) < 2 or line[0] == '§':
        print(line)
        continue
    line = line.replace('\n', '')
    len_of_word = len(line)
    line += ' adv'
    w, pos = line.split()
    ww = wf.decompose_pos(pos, w)
    all_variants = list()
    for idx, el in enumerate(ww):
        s = ''
        if 2 * len(el.cur_indices) <= len_of_word + 4:
            print(idx + 1, end=' ')
            for i in range(len(el.cur_indices) - 1):
                st = el.cur_states[i + 1]
                print('(' + st + ')', end='')
                print(w[el.cur_indices[i]:el.cur_indices[i + 1]], end='')
                s += '(' + st + ')' + w[el.cur_indices[i]:el.cur_indices[i + 1]]
            print()
        all_variants.append(s)
    print('Input the number of correct variant (or 0 to pass): ')
    ans = int(input()) - 1
    if ans >= 0:
        parsed_words.write(line + ' ' + all_variants[ans] + '\n')
    else:
        not_parsed_words.write(w + '\n')